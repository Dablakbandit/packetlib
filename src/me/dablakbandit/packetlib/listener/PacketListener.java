package me.dablakbandit.packetlib.listener;

public abstract class PacketListener {

	public abstract void onPlayerPacketSend(PlayerPacketSend packet);
	public abstract void onPlayerPacketReceive(PlayerPacketReceive packet);
	public abstract void onServerPacketSend(ServerPacketSend packet);
	public abstract void onServerPacketReceive(ServerPacketReceive packet);
}
