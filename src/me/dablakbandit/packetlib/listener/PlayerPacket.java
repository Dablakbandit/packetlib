package me.dablakbandit.packetlib.listener;

import me.dablakbandit.packetlib.packets.Packet;

import org.bukkit.entity.Player;

public abstract class PlayerPacket{

	private Player player;
	private Packet packet;
	private PacketCancellable cancellable;
	
	public PlayerPacket(Player player, Packet packet, PacketCancellable cancellable){
		this.player = player;
		this.packet = packet;
		this.cancellable = cancellable;
	}
	
	public Player getPlayer(){
		return player;
	}
	
	public boolean isCancelled(){
		return cancellable.isCancelled();
	}
	
	public void setCancelled(boolean val){
		cancellable.setCancelled(val);
	}
	
	public Packet getPacket(){
		return packet;
	}
	
	public void setPacket(Packet packet){
		this.packet = packet;
	}
	
}
