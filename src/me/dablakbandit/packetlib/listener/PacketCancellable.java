package me.dablakbandit.packetlib.listener;

public class PacketCancellable {

	private boolean	cancelled = false;

	public boolean isCancelled(){
		return cancelled;
	}

	public void setCancelled(boolean val){
		cancelled = val;
	}

}
