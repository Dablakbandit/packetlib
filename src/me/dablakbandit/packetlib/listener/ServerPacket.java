package me.dablakbandit.packetlib.listener;

import me.dablakbandit.packetlib.packets.Packet;

public abstract class ServerPacket{

	private Packet packet;
	private PacketCancellable cancellable;
	
	public ServerPacket(Packet packet, PacketCancellable cancellable){
		this.packet = packet;
		this.cancellable = cancellable;
	}
	
	public boolean isCancelled(){
		return cancellable.isCancelled();
	}
	
	public void setCancelled(boolean val){
		cancellable.setCancelled(val);
	}
	
	public Packet getPacket(){
		return packet;
	}
	
	public void setPacket(Packet packet){
		this.packet = packet;
	}
	
}
