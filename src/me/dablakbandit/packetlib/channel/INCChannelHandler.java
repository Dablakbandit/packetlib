package me.dablakbandit.packetlib.channel;

import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import me.dablakbandit.nmsutils.NMSUtils;
import me.dablakbandit.packetlib.api.PacketListenerAPI;
import me.dablakbandit.packetlib.listener.PacketCancellable;
import me.dablakbandit.packetlib.listener.PlayerPacketReceive;
import me.dablakbandit.packetlib.listener.PlayerPacketSend;
import me.dablakbandit.packetlib.listener.ServerPacketReceive;
import me.dablakbandit.packetlib.listener.ServerPacketSend;
import me.dablakbandit.packetlib.packets.Packet;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class INCChannelHandler implements ChannelHandler, Listener{

	private static Class<?>	entityPlayer = NMSUtils.getNMSClass("EntityPlayer");
	private static Class<?>	playerConnection = NMSUtils.getNMSClass("PlayerConnection");
	private static Class<?>	networkManager = NMSUtils.getNMSClass("NetworkManager");

	private static Field channelField = getChannelField();
	private static Field network = NMSUtils.getField(playerConnection, "networkManager");
	private static Field connection = NMSUtils.getField(entityPlayer, "playerConnection");

	public INCChannelHandler(){
		addServerConnectionChannel();
		for(Player player : Bukkit.getOnlinePlayers()){
			removeChannel(player);
			addChannel(player);
		}
	}
	
	public void disable(){
		for(Player player : Bukkit.getOnlinePlayers()){
			removeChannel(player);
		}
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event){
		addChannel(event.getPlayer());
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event){
		removeChannel(event.getPlayer());
	}
	
	@EventHandler
	public void onPlayerKick(PlayerKickEvent event){
		removeChannel(event.getPlayer());
	}
	
	private static Field getChannelField(){
		Field channelField = null;
		try{
			channelField = NMSUtils.getFirstFieldOfTypeWithException(networkManager, Channel.class);
		}catch(Exception e){
			System.out.print("Channel class not found");
		}
		if(channelField != null){
			channelField.setAccessible(true);
		}
		return channelField;
	}

	@Override
	public void addChannel(final Player player){
		try{
			final Object handle = NMSUtils.getHandle(player);
			final Object connection = INCChannelHandler.connection.get(handle);
			final Channel channel = (Channel) channelField.get(network.get(connection));
			new Thread(new Runnable(){
				@Override
				public void run(){
					try{
						channel.pipeline().remove("packet_listener_server");
						channel.pipeline().addBefore("packet_handler", "packet_listener_player", new PlayerChannelHandler(player));
					}catch(Exception e){}
				}
			}, "PacketLib Player Channel Adder").start();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void removeChannel(final Player player){
		try{
			final Object handle = NMSUtils.getHandle(player);
			final Object connection = INCChannelHandler.connection.get(handle);
			final Channel channel = (Channel) channelField.get(network.get(connection));
			new Thread(new Runnable(){
				@Override
				public void run(){
					try {
						channel.pipeline().remove("packet_listener_player");
					}catch(Exception e){}
				}
			}, "PacketLib Player Channel Remover").start();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void addServerConnectionChannel(){
		try{
			Server server = Bukkit.getServer();
			Object dedicatedserver = NMSUtils.getMethod(server.getClass(), "getServer").invoke(server);
			Class<?> serverconnectionclass = NMSUtils.getNMSClass("ServerConnection");
			Object serverconnection = NMSUtils.getFirstFieldOfType(NMSUtils.getNMSClass("MinecraftServer"), serverconnectionclass).get(dedicatedserver);
			Field f = NMSUtils.getLastFieldOfType(serverconnectionclass, List.class);
			List currentlist = (List<?>) f.get(serverconnection);
			Field f1 = NMSUtils.getField(currentlist.getClass().getSuperclass(), "list");
			Object list = f1.get(currentlist);
			if(list.getClass().equals(ListenerList.class)){
				return;
			}
			List newlist = Collections.synchronizedList(new ListenerList());
			for(Object o : currentlist){
				newlist.add(o);
			}
			f.set(serverconnection, newlist);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@SuppressWarnings("serial")
	class ListenerList<E> extends ArrayList<E>{
		public boolean add(E paramE){
			try{
				final E a = paramE;
				new Thread(new Runnable(){
					@Override
					public void run(){
						try{
							Channel channel = null;
							while(channel==null){
								channel = (Channel) channelField.get(a);
							}
							channel.pipeline().addBefore("packet_handler", "packet_listener_server", new ServerChannelHandler());
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}, "PacketLib Server Channel Adder").start();
			}catch(Exception e){
				e.printStackTrace();
			}
			return super.add(paramE);
		}
	}

	private static Class<?>	packet	= NMSUtils.getNMSClass("Packet");

	private class PlayerChannelHandler extends ChannelDuplexHandler{

		private Player player;

		public PlayerChannelHandler(Player player){
			this.player = player;
		}

		@Override
		public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception{
			PlayerPacketSend pps = new PlayerPacketSend(player, Packet.getPacketClass(msg), new PacketCancellable());
			if(packet.isAssignableFrom(msg.getClass())){
				PacketListenerAPI.getInstance().onPlayerPacketSend(pps);
			}
			if(pps.isCancelled())return;
			super.write(ctx, pps.getPacket().getPacket(), promise);
		}

		@Override
		public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception{
			PlayerPacketReceive ppr = new PlayerPacketReceive(player, Packet.getPacketClass(msg), new PacketCancellable());
			if(packet.isAssignableFrom(msg.getClass())){
				PacketListenerAPI.getInstance().onPlayerPacketReceive(ppr);
			}
			if(ppr.isCancelled())return;
			super.channelRead(ctx, ppr.getPacket().getPacket());
		}
	}
	
	private class ServerChannelHandler extends ChannelDuplexHandler{

		public ServerChannelHandler(){}

		@Override
		public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception{
			ServerPacketSend sps = new ServerPacketSend(Packet.getPacketClass(msg), new PacketCancellable());
			if(packet.isAssignableFrom(msg.getClass())){
				PacketListenerAPI.getInstance().onServerPacketSend(sps);
			}
			if(sps.isCancelled())return;
			super.write(ctx, sps.getPacket().getPacket(), promise);
		}

		@Override
		public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception{
			ServerPacketReceive spr = new ServerPacketReceive(Packet.getPacketClass(msg), new PacketCancellable());
			if(packet.isAssignableFrom(msg.getClass())){
				PacketListenerAPI.getInstance().onServerPacketReceive(spr);
			}
			if(spr.isCancelled())return;
			super.channelRead(ctx, spr.getPacket().getPacket());
		}
	}
}