package me.dablakbandit.packetlib.channel;

import org.bukkit.entity.Player;

public abstract interface ChannelHandler{
	abstract void addChannel(Player player);
	abstract void removeChannel(Player player);
	abstract void addServerConnectionChannel();
	abstract void disable();
}