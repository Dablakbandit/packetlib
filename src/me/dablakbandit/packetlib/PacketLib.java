package me.dablakbandit.packetlib;

import me.dablakbandit.packetlib.channel.ChannelHandler;
import me.dablakbandit.packetlib.channel.INCChannelHandler;
import me.dablakbandit.packetlib.channel.NMUChannelHandler;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class PacketLib extends JavaPlugin{

	private static PacketLib main;
	private ChannelHandler channelhandler;

	public void onLoad(){
		main = this;
	}

	public void onEnable(){
		if(initChannelHandler()){
			System.out.println("[PacketLib] Initialized ChannelHandler.");
		}else{
			Bukkit.getPluginManager().disablePlugin(this);
			System.err.println("[PacketLib] ChannelHandler failed to initialize!");
			return;
		}
	}

	public void onDisable(){
		channelhandler.disable();
	}

	public static PacketLib getInstance(){
		return main;
	}

	private boolean initChannelHandler(){
		try{
			Class.forName("net.minecraft.util.io.netty.channel.Channel");
			NMUChannelHandler ch = new NMUChannelHandler();
			Bukkit.getPluginManager().registerEvents(ch, this);
			this.channelhandler = ch;
			System.out.print("[PacketLib] Using NMUChannelHandler");
			return true;
		}catch(Exception e){}
		try{
			Class.forName("io.netty.channel.Channel");
			INCChannelHandler ch = new INCChannelHandler();
			Bukkit.getPluginManager().registerEvents(ch, this);
			this.channelhandler = ch;
			System.out.print("[PacketLib] Using INCChannelHandler");
			return true;
		}catch(Exception e){}
		return false;
	}
}
