package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutWindowData extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutWindowData
	//a : int
	//b : int
	//c : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutWindowData
	//a : int
	//b : int
	//c : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutWindowData
	//a : int
	//b : int
	//c : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutWindowData
	//a : int
	//b : int
	//c : int

	protected PacketPlayOutWindowData(Object packet){
		super(packet, PacketType.PacketPlayOutWindowData);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutWindowData");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutWindowData.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutWindowData.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutWindowData.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutWindowData.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutWindowData.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutWindowData.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutWindowData.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutWindowData.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutWindowData.c!=null;
	}
}
