package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInSteerVehicle extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInSteerVehicle
	//a : float
	//b : float
	//c : boolean
	//d : boolean

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInSteerVehicle
	//a : float
	//b : float
	//c : boolean
	//d : boolean

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInSteerVehicle
	//a : float
	//b : float
	//c : boolean
	//d : boolean

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInSteerVehicle
	//a : float
	//b : float
	//c : boolean
	//d : boolean

	protected PacketPlayInSteerVehicle(Object packet){
		super(packet, PacketType.PacketPlayInSteerVehicle);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInSteerVehicle");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public float getA(){
		try{
			return (float)PacketPlayInSteerVehicle.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setA(float f){
		try{
			PacketPlayInSteerVehicle.a.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInSteerVehicle.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public float getB(){
		try{
			return (float)PacketPlayInSteerVehicle.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setB(float f){
		try{
			PacketPlayInSteerVehicle.b.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayInSteerVehicle.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public boolean getC(){
		try{
			return (boolean)PacketPlayInSteerVehicle.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setC(boolean b){
		try{
			PacketPlayInSteerVehicle.c.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayInSteerVehicle.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public boolean getD(){
		try{
			return (boolean)PacketPlayInSteerVehicle.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setD(boolean b){
		try{
			PacketPlayInSteerVehicle.d.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayInSteerVehicle.d!=null;
	}
}
