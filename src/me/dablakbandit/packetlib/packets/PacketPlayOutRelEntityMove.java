package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutRelEntityMove extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutRelEntityMove

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutEntity$PacketPlayOutRelEntityMove

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutEntity$PacketPlayOutRelEntityMove

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutRelEntityMove
	//onGround : boolean

	protected PacketPlayOutRelEntityMove(Object packet){
		super(packet, PacketType.PacketPlayOutRelEntityMove);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutRelEntityMove", "PacketPlayOutEntity");

	private static Field onGround = NMSUtils.getFieldSilent(packetclass, "onGround");

	public boolean getOnGround(){
		try{
			return (boolean)PacketPlayOutRelEntityMove.onGround.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setOnGround(boolean b){
		try{
			PacketPlayOutRelEntityMove.onGround.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasOnGround(){
		return PacketPlayOutRelEntityMove.onGround!=null;
	}

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutRelEntityMove.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutRelEntityMove.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutRelEntityMove.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public byte getB(){
		try{
			return (byte)PacketPlayOutRelEntityMove.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(byte b){
		try{
			PacketPlayOutRelEntityMove.b.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutRelEntityMove.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public byte getC(){
		try{
			return (byte)PacketPlayOutRelEntityMove.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(byte b){
		try{
			PacketPlayOutRelEntityMove.c.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutRelEntityMove.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public byte getD(){
		try{
			return (byte)PacketPlayOutRelEntityMove.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(byte b){
		try{
			PacketPlayOutRelEntityMove.d.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutRelEntityMove.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public byte getE(){
		try{
			return (byte)PacketPlayOutRelEntityMove.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(byte b){
		try{
			PacketPlayOutRelEntityMove.e.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutRelEntityMove.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public byte getF(){
		try{
			return (byte)PacketPlayOutRelEntityMove.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setF(byte b){
		try{
			PacketPlayOutRelEntityMove.f.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutRelEntityMove.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public boolean getG(){
		try{
			return (boolean)PacketPlayOutRelEntityMove.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setG(boolean b){
		try{
			PacketPlayOutRelEntityMove.g.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayOutRelEntityMove.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public boolean getH(){
		try{
			return (boolean)PacketPlayOutRelEntityMove.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setH(boolean b){
		try{
			PacketPlayOutRelEntityMove.h.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutRelEntityMove.h!=null;
	}
}
