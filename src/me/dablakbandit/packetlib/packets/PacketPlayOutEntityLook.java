package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutEntityLook extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutEntityLook

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutEntity$PacketPlayOutEntityLook

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutEntity$PacketPlayOutEntityLook

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutEntityLook
	//onGround : boolean

	protected PacketPlayOutEntityLook(Object packet){
		super(packet, PacketType.PacketPlayOutEntityLook);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutEntityLook", "PacketPlayOutEntity");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutEntityLook.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutEntityLook.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutEntityLook.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public byte getB(){
		try{
			return (byte)PacketPlayOutEntityLook.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(byte b){
		try{
			PacketPlayOutEntityLook.b.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutEntityLook.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public byte getC(){
		try{
			return (byte)PacketPlayOutEntityLook.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(byte b){
		try{
			PacketPlayOutEntityLook.c.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutEntityLook.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public byte getD(){
		try{
			return (byte)PacketPlayOutEntityLook.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(byte b){
		try{
			PacketPlayOutEntityLook.d.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutEntityLook.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public byte getE(){
		try{
			return (byte)PacketPlayOutEntityLook.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(byte b){
		try{
			PacketPlayOutEntityLook.e.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutEntityLook.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public byte getF(){
		try{
			return (byte)PacketPlayOutEntityLook.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setF(byte b){
		try{
			PacketPlayOutEntityLook.f.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutEntityLook.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public boolean getG(){
		try{
			return (boolean)PacketPlayOutEntityLook.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setG(boolean b){
		try{
			PacketPlayOutEntityLook.g.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayOutEntityLook.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public boolean getH(){
		try{
			return (boolean)PacketPlayOutEntityLook.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setH(boolean b){
		try{
			PacketPlayOutEntityLook.h.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutEntityLook.h!=null;
	}
}
