package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutEntity extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutEntity
	//a : int
	//b : byte
	//c : byte
	//d : byte
	//e : byte
	//f : byte
	//g : boolean
	//h : boolean

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutEntity
	//a : int
	//b : byte
	//c : byte
	//d : byte
	//e : byte
	//f : byte
	//g : boolean
	//h : boolean

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutEntity
	//a : int
	//b : byte
	//c : byte
	//d : byte
	//e : byte
	//f : byte
	//g : boolean
	//h : boolean

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutEntity
	//a : int
	//b : byte
	//c : byte
	//d : byte
	//e : byte
	//f : byte
	//g : boolean

	protected PacketPlayOutEntity(Object packet){
		super(packet, PacketType.PacketPlayOutEntity);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutEntity");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutEntity.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutEntity.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutEntity.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public byte getB(){
		try{
			return (byte)PacketPlayOutEntity.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(byte b){
		try{
			PacketPlayOutEntity.b.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutEntity.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public byte getC(){
		try{
			return (byte)PacketPlayOutEntity.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(byte b){
		try{
			PacketPlayOutEntity.c.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutEntity.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public byte getD(){
		try{
			return (byte)PacketPlayOutEntity.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(byte b){
		try{
			PacketPlayOutEntity.d.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutEntity.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public byte getE(){
		try{
			return (byte)PacketPlayOutEntity.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(byte b){
		try{
			PacketPlayOutEntity.e.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutEntity.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public byte getF(){
		try{
			return (byte)PacketPlayOutEntity.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setF(byte b){
		try{
			PacketPlayOutEntity.f.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutEntity.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public boolean getG(){
		try{
			return (boolean)PacketPlayOutEntity.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setG(boolean b){
		try{
			PacketPlayOutEntity.g.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayOutEntity.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public boolean getH(){
		try{
			return (boolean)PacketPlayOutEntity.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setH(boolean b){
		try{
			PacketPlayOutEntity.h.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutEntity.h!=null;
	}
}
