package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInTransaction extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInTransaction
	//a : int
	//b : short
	//c : boolean

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInTransaction
	//a : int
	//b : short
	//c : boolean

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInTransaction
	//a : int
	//b : short
	//c : boolean

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInTransaction
	//a : int
	//b : short
	//c : boolean

	protected PacketPlayInTransaction(Object packet){
		super(packet, PacketType.PacketPlayInTransaction);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInTransaction");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayInTransaction.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayInTransaction.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInTransaction.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public short getB(){
		try{
			return (short)PacketPlayInTransaction.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(short s){
		try{
			PacketPlayInTransaction.b.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayInTransaction.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public boolean getC(){
		try{
			return (boolean)PacketPlayInTransaction.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setC(boolean b){
		try{
			PacketPlayInTransaction.c.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayInTransaction.c!=null;
	}
}
