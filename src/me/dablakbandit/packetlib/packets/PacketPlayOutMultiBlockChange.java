package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutMultiBlockChange extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutMultiBlockChange
	//a : net.minecraft.server.v1_8_R1.ChunkCoordIntPair
	//b : [Lnet.minecraft.server.v1_8_R1.MultiBlockChangeInfo;

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutMultiBlockChange
	//a : net.minecraft.server.v1_8_R2.ChunkCoordIntPair
	//b : [Lnet.minecraft.server.v1_8_R2.PacketPlayOutMultiBlockChange$MultiBlockChangeInfo;

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutMultiBlockChange
	//a : net.minecraft.server.v1_8_R3.ChunkCoordIntPair
	//b : [Lnet.minecraft.server.v1_8_R3.PacketPlayOutMultiBlockChange$MultiBlockChangeInfo;

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutMultiBlockChange
	//a : org.apache.logging.log4j.Logger
	//b : net.minecraft.server.v1_7_R4.ChunkCoordIntPair
	//c : [B
	//d : int
	//ashort : [S
	//blocks : [I
	//chunk : net.minecraft.server.v1_7_R4.Chunk

	protected PacketPlayOutMultiBlockChange(Object packet){
		super(packet, PacketType.PacketPlayOutMultiBlockChange);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutMultiBlockChange");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketPlayOutMultiBlockChange.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketPlayOutMultiBlockChange.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutMultiBlockChange.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayOutMultiBlockChange.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayOutMultiBlockChange.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutMultiBlockChange.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public byte[] getC(){
		try{
			return (byte[])PacketPlayOutMultiBlockChange.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setC(byte[] b){
		try{
			PacketPlayOutMultiBlockChange.c.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutMultiBlockChange.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutMultiBlockChange.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutMultiBlockChange.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutMultiBlockChange.d!=null;
	}

	private static Field ashort = NMSUtils.getFieldSilent(packetclass, "ashort");

	public short[] getAshort(){
		try{
			return (short[])PacketPlayOutMultiBlockChange.ashort.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setAshort(short[] s){
		try{
			PacketPlayOutMultiBlockChange.ashort.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAshort(){
		return PacketPlayOutMultiBlockChange.ashort!=null;
	}

	private static Field blocks = NMSUtils.getFieldSilent(packetclass, "blocks");

	public int[] getBlocks(){
		try{
			return (int[])PacketPlayOutMultiBlockChange.blocks.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setBlocks(int[] i){
		try{
			PacketPlayOutMultiBlockChange.blocks.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBlocks(){
		return PacketPlayOutMultiBlockChange.blocks!=null;
	}

	private static Field chunk = NMSUtils.getFieldSilent(packetclass, "chunk");

	public Object getChunk(){
		try{
			return PacketPlayOutMultiBlockChange.chunk.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setChunk(Object o){
		try{
			PacketPlayOutMultiBlockChange.chunk.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasChunk(){
		return PacketPlayOutMultiBlockChange.chunk!=null;
	}
}
