package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutKickDisconnect extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutKickDisconnect
	//a : net.minecraft.server.v1_8_R1.IChatBaseComponent

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutKickDisconnect
	//a : net.minecraft.server.v1_8_R2.IChatBaseComponent

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutKickDisconnect
	//a : net.minecraft.server.v1_8_R3.IChatBaseComponent

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutKickDisconnect
	//a : net.minecraft.server.v1_7_R4.IChatBaseComponent

	protected PacketPlayOutKickDisconnect(Object packet){
		super(packet, PacketType.PacketPlayOutKickDisconnect);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutKickDisconnect");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketPlayOutKickDisconnect.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketPlayOutKickDisconnect.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutKickDisconnect.a!=null;
	}
}
