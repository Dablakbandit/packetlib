package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutRelEntityMoveLook extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutRelEntityMoveLook

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutEntity$PacketPlayOutRelEntityMoveLook

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutEntity$PacketPlayOutRelEntityMoveLook

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutRelEntityMoveLook
	//onGround : boolean

	protected PacketPlayOutRelEntityMoveLook(Object packet){
		super(packet, PacketType.PacketPlayOutRelEntityMoveLook);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutRelEntityMoveLook", "PacketPlayOutEntity");

	private static Field onGround = NMSUtils.getFieldSilent(packetclass, "onGround");

	public boolean getOnGround(){
		try{
			return (boolean)PacketPlayOutRelEntityMoveLook.onGround.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setOnGround(boolean b){
		try{
			PacketPlayOutRelEntityMoveLook.onGround.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasOnGround(){
		return PacketPlayOutRelEntityMoveLook.onGround!=null;
	}

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutRelEntityMoveLook.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutRelEntityMoveLook.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutRelEntityMoveLook.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public byte getB(){
		try{
			return (byte)PacketPlayOutRelEntityMoveLook.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(byte b){
		try{
			PacketPlayOutRelEntityMoveLook.b.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutRelEntityMoveLook.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public byte getC(){
		try{
			return (byte)PacketPlayOutRelEntityMoveLook.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(byte b){
		try{
			PacketPlayOutRelEntityMoveLook.c.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutRelEntityMoveLook.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public byte getD(){
		try{
			return (byte)PacketPlayOutRelEntityMoveLook.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(byte b){
		try{
			PacketPlayOutRelEntityMoveLook.d.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutRelEntityMoveLook.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public byte getE(){
		try{
			return (byte)PacketPlayOutRelEntityMoveLook.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(byte b){
		try{
			PacketPlayOutRelEntityMoveLook.e.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutRelEntityMoveLook.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public byte getF(){
		try{
			return (byte)PacketPlayOutRelEntityMoveLook.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setF(byte b){
		try{
			PacketPlayOutRelEntityMoveLook.f.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutRelEntityMoveLook.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public boolean getG(){
		try{
			return (boolean)PacketPlayOutRelEntityMoveLook.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setG(boolean b){
		try{
			PacketPlayOutRelEntityMoveLook.g.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayOutRelEntityMoveLook.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public boolean getH(){
		try{
			return (boolean)PacketPlayOutRelEntityMoveLook.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setH(boolean b){
		try{
			PacketPlayOutRelEntityMoveLook.h.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutRelEntityMoveLook.h!=null;
	}
}
