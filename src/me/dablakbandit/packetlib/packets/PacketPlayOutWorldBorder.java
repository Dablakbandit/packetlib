package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutWorldBorder extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutWorldBorder
	//a : net.minecraft.server.v1_8_R1.EnumWorldBorderAction
	//b : int
	//c : double
	//d : double
	//e : double
	//f : double
	//g : long
	//h : int
	//i : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutWorldBorder
	//a : net.minecraft.server.v1_8_R2.PacketPlayOutWorldBorder$EnumWorldBorderAction
	//b : int
	//c : double
	//d : double
	//e : double
	//f : double
	//g : long
	//h : int
	//i : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutWorldBorder
	//a : net.minecraft.server.v1_8_R3.PacketPlayOutWorldBorder$EnumWorldBorderAction
	//b : int
	//c : double
	//d : double
	//e : double
	//f : double
	//g : long
	//h : int
	//i : int

	protected PacketPlayOutWorldBorder(Object packet){
		super(packet, PacketType.PacketPlayOutWorldBorder);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutWorldBorder");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketPlayOutWorldBorder.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketPlayOutWorldBorder.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutWorldBorder.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutWorldBorder.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutWorldBorder.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutWorldBorder.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public double getC(){
		try{
			return (double)PacketPlayOutWorldBorder.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}

	public void setC(double d){
		try{
			PacketPlayOutWorldBorder.c.set(packet, d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutWorldBorder.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public double getD(){
		try{
			return (double)PacketPlayOutWorldBorder.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}

	public void setD(double d){
		try{
			PacketPlayOutWorldBorder.d.set(packet, d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutWorldBorder.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public double getE(){
		try{
			return (double)PacketPlayOutWorldBorder.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}

	public void setE(double d){
		try{
			PacketPlayOutWorldBorder.e.set(packet, d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutWorldBorder.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public double getF(){
		try{
			return (double)PacketPlayOutWorldBorder.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}

	public void setF(double d){
		try{
			PacketPlayOutWorldBorder.f.set(packet, d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutWorldBorder.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public long getG(){
		try{
			return (long)PacketPlayOutWorldBorder.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setG(long l){
		try{
			PacketPlayOutWorldBorder.g.set(packet, l);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayOutWorldBorder.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public int getH(){
		try{
			return (int)PacketPlayOutWorldBorder.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setH(int i){
		try{
			PacketPlayOutWorldBorder.h.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutWorldBorder.h!=null;
	}

	private static Field i = NMSUtils.getFieldSilent(packetclass, "i");

	public int getI(){
		try{
			return (int)PacketPlayOutWorldBorder.i.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setI(int i){
		try{
			PacketPlayOutWorldBorder.i.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasI(){
		return PacketPlayOutWorldBorder.i!=null;
	}
}
