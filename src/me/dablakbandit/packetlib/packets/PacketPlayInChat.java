package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInChat extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInChat
	//a : java.lang.String
	//executors : java.util.concurrent.ExecutorService

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInChat
	//a : java.lang.String
	//executors : java.util.concurrent.ExecutorService

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInChat
	//a : java.lang.String
	//executors : java.util.concurrent.ExecutorService

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInChat
	//message : java.lang.String
	//executors : java.util.concurrent.ExecutorService

	protected PacketPlayInChat(Object packet){
		super(packet, PacketType.PacketPlayInChat);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInChat");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String getA(){
		try{
			return (String)PacketPlayInChat.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(String s){
		try{
			PacketPlayInChat.a.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInChat.a!=null;
	}

	private static Field message = NMSUtils.getFieldSilent(packetclass, "message");

	public String getMessage(){
		try{
			return (String)PacketPlayInChat.message.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setMessage(String s){
		try{
			PacketPlayInChat.message.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasMessage(){
		return PacketPlayInChat.message!=null;
	}

	private static Field executors = NMSUtils.getFieldSilent(packetclass, "executors");

	public Object getExecutors(){
		try{
			return PacketPlayInChat.executors.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setExecutors(Object o){
		try{
			PacketPlayInChat.executors.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasExecutors(){
		return PacketPlayInChat.executors!=null;
	}
}
