package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutLogin extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutLogin
	//a : int
	//b : boolean
	//c : net.minecraft.server.v1_8_R1.EnumGamemode
	//d : int
	//e : net.minecraft.server.v1_8_R1.EnumDifficulty
	//f : int
	//g : net.minecraft.server.v1_8_R1.WorldType
	//h : boolean

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutLogin
	//a : int
	//b : boolean
	//c : net.minecraft.server.v1_8_R2.WorldSettings$EnumGamemode
	//d : int
	//e : net.minecraft.server.v1_8_R2.EnumDifficulty
	//f : int
	//g : net.minecraft.server.v1_8_R2.WorldType
	//h : boolean

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutLogin
	//a : int
	//b : boolean
	//c : net.minecraft.server.v1_8_R3.WorldSettings$EnumGamemode
	//d : int
	//e : net.minecraft.server.v1_8_R3.EnumDifficulty
	//f : int
	//g : net.minecraft.server.v1_8_R3.WorldType
	//h : boolean

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutLogin
	//a : int
	//b : boolean
	//c : net.minecraft.server.v1_7_R4.EnumGamemode
	//d : int
	//e : net.minecraft.server.v1_7_R4.EnumDifficulty
	//f : int
	//g : net.minecraft.server.v1_7_R4.WorldType

	protected PacketPlayOutLogin(Object packet){
		super(packet, PacketType.PacketPlayOutLogin);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutLogin");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutLogin.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutLogin.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutLogin.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public boolean getB(){
		try{
			return (boolean)PacketPlayOutLogin.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setB(boolean b){
		try{
			PacketPlayOutLogin.b.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutLogin.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public Object getC(){
		try{
			return PacketPlayOutLogin.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setC(Object o){
		try{
			PacketPlayOutLogin.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutLogin.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutLogin.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutLogin.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutLogin.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public Object getE(){
		try{
			return PacketPlayOutLogin.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setE(Object o){
		try{
			PacketPlayOutLogin.e.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutLogin.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public int getF(){
		try{
			return (int)PacketPlayOutLogin.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setF(int i){
		try{
			PacketPlayOutLogin.f.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutLogin.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public Object getG(){
		try{
			return PacketPlayOutLogin.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setG(Object o){
		try{
			PacketPlayOutLogin.g.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayOutLogin.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public boolean getH(){
		try{
			return (boolean)PacketPlayOutLogin.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setH(boolean b){
		try{
			PacketPlayOutLogin.h.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutLogin.h!=null;
	}
}
