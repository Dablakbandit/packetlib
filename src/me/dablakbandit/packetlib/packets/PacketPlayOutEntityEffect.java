package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutEntityEffect extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutEntityEffect
	//a : int
	//b : byte
	//c : byte
	//d : int
	//e : byte

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutEntityEffect
	//a : int
	//b : byte
	//c : byte
	//d : int
	//e : byte

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutEntityEffect
	//a : int
	//b : byte
	//c : byte
	//d : int
	//e : byte

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutEntityEffect
	//a : int
	//b : byte
	//c : byte
	//d : short

	protected PacketPlayOutEntityEffect(Object packet){
		super(packet, PacketType.PacketPlayOutEntityEffect);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutEntityEffect");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutEntityEffect.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutEntityEffect.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutEntityEffect.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public byte getB(){
		try{
			return (byte)PacketPlayOutEntityEffect.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(byte b){
		try{
			PacketPlayOutEntityEffect.b.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutEntityEffect.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public byte getC(){
		try{
			return (byte)PacketPlayOutEntityEffect.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(byte b){
		try{
			PacketPlayOutEntityEffect.c.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutEntityEffect.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public short getDAsShort(){
		try{
			return (short)PacketPlayOutEntityEffect.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setDAsShort(short o){
		try{
			PacketPlayOutEntityEffect.d.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasDAsShort(){
		return PacketPlayOutEntityEffect.d!=null&&short.class.isAssignableFrom(PacketPlayOutEntityEffect.d.getType());
	}

	public int getDAsInt(){
		try{
			return (int)PacketPlayOutEntityEffect.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setDAsInt(int o){
		try{
			PacketPlayOutEntityEffect.d.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasDAsInt(){
		return PacketPlayOutEntityEffect.d!=null&&int.class.isAssignableFrom(PacketPlayOutEntityEffect.d.getType());
	}
	public boolean hasD(){
		return PacketPlayOutEntityEffect.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public byte getE(){
		try{
			return (byte)PacketPlayOutEntityEffect.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(byte b){
		try{
			PacketPlayOutEntityEffect.e.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutEntityEffect.e!=null;
	}
}
