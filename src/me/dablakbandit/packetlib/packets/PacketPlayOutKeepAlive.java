package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutKeepAlive extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutKeepAlive
	//a : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutKeepAlive
	//a : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutKeepAlive
	//a : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutKeepAlive
	//a : int

	protected PacketPlayOutKeepAlive(Object packet){
		super(packet, PacketType.PacketPlayOutKeepAlive);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutKeepAlive");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutKeepAlive.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutKeepAlive.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutKeepAlive.a!=null;
	}
}
