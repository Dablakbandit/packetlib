package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInBlockPlace extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInBlockPlace
	//a : net.minecraft.server.v1_8_R1.BlockPosition
	//b : net.minecraft.server.v1_8_R1.BlockPosition
	//c : int
	//d : net.minecraft.server.v1_8_R1.ItemStack
	//e : float
	//f : float
	//g : float
	//timestamp : long

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInBlockPlace
	//a : net.minecraft.server.v1_8_R2.BlockPosition
	//b : net.minecraft.server.v1_8_R2.BlockPosition
	//c : int
	//d : net.minecraft.server.v1_8_R2.ItemStack
	//e : float
	//f : float
	//g : float
	//timestamp : long

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInBlockPlace
	//a : net.minecraft.server.v1_8_R3.BlockPosition
	//b : net.minecraft.server.v1_8_R3.BlockPosition
	//c : int
	//d : net.minecraft.server.v1_8_R3.ItemStack
	//e : float
	//f : float
	//g : float
	//timestamp : long

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInBlockPlace
	//a : int
	//b : int
	//c : int
	//d : int
	//e : net.minecraft.server.v1_7_R4.ItemStack
	//f : float
	//g : float
	//h : float

	protected PacketPlayInBlockPlace(Object packet){
		super(packet, PacketType.PacketPlayInBlockPlace);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInBlockPlace");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getAAsInt(){
		try{
			return (int)PacketPlayInBlockPlace.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setAAsInt(int o){
		try{
			PacketPlayInBlockPlace.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsInt(){
		return PacketPlayInBlockPlace.a!=null&&int.class.isAssignableFrom(PacketPlayInBlockPlace.a.getType());
	}

	public Object getAAsObject(){
		try{
			return (Object)PacketPlayInBlockPlace.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setAAsObject(Object o){
		try{
			PacketPlayInBlockPlace.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsObject(){
		return PacketPlayInBlockPlace.a!=null&&Object.class.isAssignableFrom(PacketPlayInBlockPlace.a.getType());
	}
	public boolean hasA(){
		return PacketPlayInBlockPlace.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getBAsInt(){
		try{
			return (int)PacketPlayInBlockPlace.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setBAsInt(int o){
		try{
			PacketPlayInBlockPlace.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsInt(){
		return PacketPlayInBlockPlace.b!=null&&int.class.isAssignableFrom(PacketPlayInBlockPlace.b.getType());
	}

	public Object getBAsObject(){
		try{
			return (Object)PacketPlayInBlockPlace.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setBAsObject(Object o){
		try{
			PacketPlayInBlockPlace.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsObject(){
		return PacketPlayInBlockPlace.b!=null&&Object.class.isAssignableFrom(PacketPlayInBlockPlace.b.getType());
	}
	public boolean hasB(){
		return PacketPlayInBlockPlace.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayInBlockPlace.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayInBlockPlace.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayInBlockPlace.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getDAsInt(){
		try{
			return (int)PacketPlayInBlockPlace.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setDAsInt(int o){
		try{
			PacketPlayInBlockPlace.d.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasDAsInt(){
		return PacketPlayInBlockPlace.d!=null&&int.class.isAssignableFrom(PacketPlayInBlockPlace.d.getType());
	}

	public Object getDAsObject(){
		try{
			return (Object)PacketPlayInBlockPlace.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setDAsObject(Object o){
		try{
			PacketPlayInBlockPlace.d.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasDAsObject(){
		return PacketPlayInBlockPlace.d!=null&&Object.class.isAssignableFrom(PacketPlayInBlockPlace.d.getType());
	}
	public boolean hasD(){
		return PacketPlayInBlockPlace.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public Object getEAsObject(){
		try{
			return (Object)PacketPlayInBlockPlace.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setEAsObject(Object o){
		try{
			PacketPlayInBlockPlace.e.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasEAsObject(){
		return PacketPlayInBlockPlace.e!=null&&Object.class.isAssignableFrom(PacketPlayInBlockPlace.e.getType());
	}

	public float getEAsFloat(){
		try{
			return (float)PacketPlayInBlockPlace.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setEAsFloat(float o){
		try{
			PacketPlayInBlockPlace.e.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasEAsFloat(){
		return PacketPlayInBlockPlace.e!=null&&float.class.isAssignableFrom(PacketPlayInBlockPlace.e.getType());
	}
	public boolean hasE(){
		return PacketPlayInBlockPlace.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public float getF(){
		try{
			return (float)PacketPlayInBlockPlace.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setF(float f){
		try{
			PacketPlayInBlockPlace.f.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayInBlockPlace.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public float getG(){
		try{
			return (float)PacketPlayInBlockPlace.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setG(float f){
		try{
			PacketPlayInBlockPlace.g.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayInBlockPlace.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public float getH(){
		try{
			return (float)PacketPlayInBlockPlace.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setH(float f){
		try{
			PacketPlayInBlockPlace.h.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayInBlockPlace.h!=null;
	}

	private static Field timestamp = NMSUtils.getFieldSilent(packetclass, "timestamp");

	public long getTimestamp(){
		try{
			return (long)PacketPlayInBlockPlace.timestamp.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setTimestamp(long l){
		try{
			PacketPlayInBlockPlace.timestamp.set(packet, l);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasTimestamp(){
		return PacketPlayInBlockPlace.timestamp!=null;
	}
}
