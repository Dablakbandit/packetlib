package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutSetSlot extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutSetSlot
	//a : int
	//b : int
	//c : net.minecraft.server.v1_8_R1.ItemStack

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutSetSlot
	//a : int
	//b : int
	//c : net.minecraft.server.v1_8_R2.ItemStack

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutSetSlot
	//a : int
	//b : int
	//c : net.minecraft.server.v1_8_R3.ItemStack

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutSetSlot
	//a : int
	//b : int
	//c : net.minecraft.server.v1_7_R4.ItemStack

	protected PacketPlayOutSetSlot(Object packet){
		super(packet, PacketType.PacketPlayOutSetSlot);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutSetSlot");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutSetSlot.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutSetSlot.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutSetSlot.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutSetSlot.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutSetSlot.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutSetSlot.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public Object getC(){
		try{
			return PacketPlayOutSetSlot.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setC(Object o){
		try{
			PacketPlayOutSetSlot.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutSetSlot.c!=null;
	}
}
