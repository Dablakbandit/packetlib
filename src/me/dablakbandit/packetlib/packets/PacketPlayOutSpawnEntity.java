package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutSpawnEntity extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutSpawnEntity
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int
	//f : int
	//g : int
	//h : int
	//i : int
	//j : int
	//k : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutSpawnEntity
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int
	//f : int
	//g : int
	//h : int
	//i : int
	//j : int
	//k : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntity
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int
	//f : int
	//g : int
	//h : int
	//i : int
	//j : int
	//k : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutSpawnEntity
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int
	//f : int
	//g : int
	//h : int
	//i : int
	//j : int
	//k : int

	protected PacketPlayOutSpawnEntity(Object packet){
		super(packet, PacketType.PacketPlayOutSpawnEntity);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutSpawnEntity");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutSpawnEntity.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutSpawnEntity.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutSpawnEntity.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutSpawnEntity.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutSpawnEntity.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutSpawnEntity.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutSpawnEntity.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutSpawnEntity.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutSpawnEntity.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutSpawnEntity.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutSpawnEntity.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutSpawnEntity.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public int getE(){
		try{
			return (int)PacketPlayOutSpawnEntity.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(int i){
		try{
			PacketPlayOutSpawnEntity.e.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutSpawnEntity.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public int getF(){
		try{
			return (int)PacketPlayOutSpawnEntity.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setF(int i){
		try{
			PacketPlayOutSpawnEntity.f.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutSpawnEntity.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public int getG(){
		try{
			return (int)PacketPlayOutSpawnEntity.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setG(int i){
		try{
			PacketPlayOutSpawnEntity.g.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayOutSpawnEntity.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public int getH(){
		try{
			return (int)PacketPlayOutSpawnEntity.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setH(int i){
		try{
			PacketPlayOutSpawnEntity.h.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutSpawnEntity.h!=null;
	}

	private static Field i = NMSUtils.getFieldSilent(packetclass, "i");

	public int getI(){
		try{
			return (int)PacketPlayOutSpawnEntity.i.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setI(int i){
		try{
			PacketPlayOutSpawnEntity.i.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasI(){
		return PacketPlayOutSpawnEntity.i!=null;
	}

	private static Field j = NMSUtils.getFieldSilent(packetclass, "j");

	public int getJ(){
		try{
			return (int)PacketPlayOutSpawnEntity.j.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setJ(int i){
		try{
			PacketPlayOutSpawnEntity.j.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasJ(){
		return PacketPlayOutSpawnEntity.j!=null;
	}

	private static Field k = NMSUtils.getFieldSilent(packetclass, "k");

	public int getK(){
		try{
			return (int)PacketPlayOutSpawnEntity.k.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setK(int i){
		try{
			PacketPlayOutSpawnEntity.k.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasK(){
		return PacketPlayOutSpawnEntity.k!=null;
	}
}
