package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutScoreboardObjective extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutScoreboardObjective
	//a : java.lang.String
	//b : java.lang.String
	//c : net.minecraft.server.v1_8_R1.EnumScoreboardHealthDisplay
	//d : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutScoreboardObjective
	//a : java.lang.String
	//b : java.lang.String
	//c : net.minecraft.server.v1_8_R2.IScoreboardCriteria$EnumScoreboardHealthDisplay
	//d : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutScoreboardObjective
	//a : java.lang.String
	//b : java.lang.String
	//c : net.minecraft.server.v1_8_R3.IScoreboardCriteria$EnumScoreboardHealthDisplay
	//d : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutScoreboardObjective
	//a : java.lang.String
	//b : java.lang.String
	//c : int

	protected PacketPlayOutScoreboardObjective(Object packet){
		super(packet, PacketType.PacketPlayOutScoreboardObjective);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutScoreboardObjective");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String getA(){
		try{
			return (String)PacketPlayOutScoreboardObjective.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(String s){
		try{
			PacketPlayOutScoreboardObjective.a.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutScoreboardObjective.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public String getB(){
		try{
			return (String)PacketPlayOutScoreboardObjective.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(String s){
		try{
			PacketPlayOutScoreboardObjective.b.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutScoreboardObjective.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getCAsInt(){
		try{
			return (int)PacketPlayOutScoreboardObjective.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setCAsInt(int o){
		try{
			PacketPlayOutScoreboardObjective.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasCAsInt(){
		return PacketPlayOutScoreboardObjective.c!=null&&int.class.isAssignableFrom(PacketPlayOutScoreboardObjective.c.getType());
	}

	public Object getCAsObject(){
		try{
			return (Object)PacketPlayOutScoreboardObjective.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setCAsObject(Object o){
		try{
			PacketPlayOutScoreboardObjective.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasCAsObject(){
		return PacketPlayOutScoreboardObjective.c!=null&&Object.class.isAssignableFrom(PacketPlayOutScoreboardObjective.c.getType());
	}
	public boolean hasC(){
		return PacketPlayOutScoreboardObjective.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutScoreboardObjective.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutScoreboardObjective.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutScoreboardObjective.d!=null;
	}
}
