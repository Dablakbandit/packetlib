package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutSpawnEntityWeather extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutSpawnEntityWeather
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutSpawnEntityWeather
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityWeather
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutSpawnEntityWeather
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int

	protected PacketPlayOutSpawnEntityWeather(Object packet){
		super(packet, PacketType.PacketPlayOutSpawnEntityWeather);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutSpawnEntityWeather");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutSpawnEntityWeather.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutSpawnEntityWeather.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutSpawnEntityWeather.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutSpawnEntityWeather.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutSpawnEntityWeather.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutSpawnEntityWeather.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutSpawnEntityWeather.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutSpawnEntityWeather.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutSpawnEntityWeather.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutSpawnEntityWeather.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutSpawnEntityWeather.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutSpawnEntityWeather.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public int getE(){
		try{
			return (int)PacketPlayOutSpawnEntityWeather.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(int i){
		try{
			PacketPlayOutSpawnEntityWeather.e.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutSpawnEntityWeather.e!=null;
	}
}
