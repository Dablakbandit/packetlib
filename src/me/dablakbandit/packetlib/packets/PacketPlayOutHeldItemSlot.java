package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutHeldItemSlot extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutHeldItemSlot
	//a : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutHeldItemSlot
	//a : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutHeldItemSlot
	//a : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutHeldItemSlot
	//a : int

	protected PacketPlayOutHeldItemSlot(Object packet){
		super(packet, PacketType.PacketPlayOutHeldItemSlot);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutHeldItemSlot");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutHeldItemSlot.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutHeldItemSlot.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutHeldItemSlot.a!=null;
	}
}
