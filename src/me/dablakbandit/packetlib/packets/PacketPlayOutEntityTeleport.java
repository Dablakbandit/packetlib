package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutEntityTeleport extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutEntityTeleport
	//a : int
	//b : int
	//c : int
	//d : int
	//e : byte
	//f : byte
	//g : boolean

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutEntityTeleport
	//a : int
	//b : int
	//c : int
	//d : int
	//e : byte
	//f : byte
	//g : boolean

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutEntityTeleport
	//a : int
	//b : int
	//c : int
	//d : int
	//e : byte
	//f : byte
	//g : boolean

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutEntityTeleport
	//a : int
	//b : int
	//c : int
	//d : int
	//e : byte
	//f : byte
	//onGround : boolean
	//heightCorrection : boolean

	protected PacketPlayOutEntityTeleport(Object packet){
		super(packet, PacketType.PacketPlayOutEntityTeleport);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutEntityTeleport");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutEntityTeleport.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutEntityTeleport.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutEntityTeleport.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutEntityTeleport.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutEntityTeleport.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutEntityTeleport.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutEntityTeleport.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutEntityTeleport.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutEntityTeleport.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutEntityTeleport.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutEntityTeleport.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutEntityTeleport.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public byte getE(){
		try{
			return (byte)PacketPlayOutEntityTeleport.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(byte b){
		try{
			PacketPlayOutEntityTeleport.e.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutEntityTeleport.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public byte getF(){
		try{
			return (byte)PacketPlayOutEntityTeleport.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setF(byte b){
		try{
			PacketPlayOutEntityTeleport.f.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutEntityTeleport.f!=null;
	}
}
