package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInSpectate extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInSpectate
	//a : java.util.UUID

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInSpectate
	//a : java.util.UUID

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInSpectate
	//a : java.util.UUID

	protected PacketPlayInSpectate(Object packet){
		super(packet, PacketType.PacketPlayInSpectate);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInSpectate");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketPlayInSpectate.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketPlayInSpectate.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInSpectate.a!=null;
	}
}
