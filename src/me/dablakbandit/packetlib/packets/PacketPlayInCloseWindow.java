package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInCloseWindow extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInCloseWindow
	//id : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInCloseWindow
	//id : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInCloseWindow
	//id : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInCloseWindow
	//a : int

	protected PacketPlayInCloseWindow(Object packet){
		super(packet, PacketType.PacketPlayInCloseWindow);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInCloseWindow");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayInCloseWindow.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayInCloseWindow.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInCloseWindow.a!=null;
	}
}
