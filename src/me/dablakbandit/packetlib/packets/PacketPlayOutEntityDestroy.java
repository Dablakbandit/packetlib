package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutEntityDestroy extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutEntityDestroy
	//a : [I

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutEntityDestroy
	//a : [I

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy
	//a : [I

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutEntityDestroy
	//a : [I

	protected PacketPlayOutEntityDestroy(Object packet){
		super(packet, PacketType.PacketPlayOutEntityDestroy);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutEntityDestroy");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int[] getA(){
		try{
			return (int[])PacketPlayOutEntityDestroy.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(int[] i){
		try{
			PacketPlayOutEntityDestroy.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutEntityDestroy.a!=null;
	}
}
