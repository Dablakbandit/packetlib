package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInUpdateSign extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInUpdateSign
	//a : net.minecraft.server.v1_8_R1.BlockPosition
	//b : [Lnet.minecraft.server.v1_8_R1.IChatBaseComponent;

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInUpdateSign
	//a : net.minecraft.server.v1_8_R2.BlockPosition
	//b : [Lnet.minecraft.server.v1_8_R2.IChatBaseComponent;

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInUpdateSign
	//a : net.minecraft.server.v1_8_R3.BlockPosition
	//b : [Lnet.minecraft.server.v1_8_R3.IChatBaseComponent;

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInUpdateSign
	//a : int
	//b : int
	//c : int
	//d : [Ljava.lang.String;

	protected PacketPlayInUpdateSign(Object packet){
		super(packet, PacketType.PacketPlayInUpdateSign);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInUpdateSign");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getAAsInt(){
		try{
			return (int)PacketPlayInUpdateSign.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setAAsInt(int o){
		try{
			PacketPlayInUpdateSign.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsInt(){
		return PacketPlayInUpdateSign.a!=null&&int.class.isAssignableFrom(PacketPlayInUpdateSign.a.getType());
	}

	public Object getAAsObject(){
		try{
			return (Object)PacketPlayInUpdateSign.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setAAsObject(Object o){
		try{
			PacketPlayInUpdateSign.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsObject(){
		return PacketPlayInUpdateSign.a!=null&&Object.class.isAssignableFrom(PacketPlayInUpdateSign.a.getType());
	}
	public boolean hasA(){
		return PacketPlayInUpdateSign.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getBAsInt(){
		try{
			return (int)PacketPlayInUpdateSign.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setBAsInt(int o){
		try{
			PacketPlayInUpdateSign.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsInt(){
		return PacketPlayInUpdateSign.b!=null&&int.class.isAssignableFrom(PacketPlayInUpdateSign.b.getType());
	}

	public Object getBAsObject(){
		try{
			return (Object)PacketPlayInUpdateSign.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setBAsObject(Object o){
		try{
			PacketPlayInUpdateSign.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsObject(){
		return PacketPlayInUpdateSign.b!=null&&Object.class.isAssignableFrom(PacketPlayInUpdateSign.b.getType());
	}
	public boolean hasB(){
		return PacketPlayInUpdateSign.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayInUpdateSign.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayInUpdateSign.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayInUpdateSign.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public String[] getD(){
		try{
			return (String[])PacketPlayInUpdateSign.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setD(String[] s){
		try{
			PacketPlayInUpdateSign.d.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayInUpdateSign.d!=null;
	}
}
