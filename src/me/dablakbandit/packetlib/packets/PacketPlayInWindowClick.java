package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInWindowClick extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInWindowClick
	//a : int
	//slot : int
	//button : int
	//d : short
	//item : net.minecraft.server.v1_8_R1.ItemStack
	//shift : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInWindowClick
	//a : int
	//slot : int
	//button : int
	//d : short
	//item : net.minecraft.server.v1_8_R2.ItemStack
	//shift : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInWindowClick
	//a : int
	//slot : int
	//button : int
	//d : short
	//item : net.minecraft.server.v1_8_R3.ItemStack
	//shift : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInWindowClick
	//a : int
	//slot : int
	//button : int
	//d : short
	//item : net.minecraft.server.v1_7_R4.ItemStack
	//shift : int

	protected PacketPlayInWindowClick(Object packet){
		super(packet, PacketType.PacketPlayInWindowClick);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInWindowClick");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayInWindowClick.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayInWindowClick.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInWindowClick.a!=null;
	}

	private static Field slot = NMSUtils.getFieldSilent(packetclass, "slot");

	public int getSlot(){
		try{
			return (int)PacketPlayInWindowClick.slot.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setSlot(int i){
		try{
			PacketPlayInWindowClick.slot.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasSlot(){
		return PacketPlayInWindowClick.slot!=null;
	}

	private static Field button = NMSUtils.getFieldSilent(packetclass, "button");

	public int getButton(){
		try{
			return (int)PacketPlayInWindowClick.button.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setButton(int i){
		try{
			PacketPlayInWindowClick.button.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasButton(){
		return PacketPlayInWindowClick.button!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public short getD(){
		try{
			return (short)PacketPlayInWindowClick.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(short s){
		try{
			PacketPlayInWindowClick.d.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayInWindowClick.d!=null;
	}

	private static Field item = NMSUtils.getFieldSilent(packetclass, "item");

	public Object getItem(){
		try{
			return PacketPlayInWindowClick.item.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setItem(Object o){
		try{
			PacketPlayInWindowClick.item.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasItem(){
		return PacketPlayInWindowClick.item!=null;
	}

	private static Field shift = NMSUtils.getFieldSilent(packetclass, "shift");

	public int getShift(){
		try{
			return (int)PacketPlayInWindowClick.shift.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setShift(int i){
		try{
			PacketPlayInWindowClick.shift.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasShift(){
		return PacketPlayInWindowClick.shift!=null;
	}
}
