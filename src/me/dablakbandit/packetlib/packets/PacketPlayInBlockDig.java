package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInBlockDig extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInBlockDig
	//a : net.minecraft.server.v1_8_R1.BlockPosition
	//b : net.minecraft.server.v1_8_R1.EnumDirection
	//c : net.minecraft.server.v1_8_R1.EnumPlayerDigType

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInBlockDig
	//a : net.minecraft.server.v1_8_R2.BlockPosition
	//b : net.minecraft.server.v1_8_R2.EnumDirection
	//c : net.minecraft.server.v1_8_R2.PacketPlayInBlockDig$EnumPlayerDigType

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInBlockDig
	//a : net.minecraft.server.v1_8_R3.BlockPosition
	//b : net.minecraft.server.v1_8_R3.EnumDirection
	//c : net.minecraft.server.v1_8_R3.PacketPlayInBlockDig$EnumPlayerDigType

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInBlockDig
	//a : int
	//b : int
	//c : int
	//face : int
	//e : int

	protected PacketPlayInBlockDig(Object packet){
		super(packet, PacketType.PacketPlayInBlockDig);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInBlockDig");

	private static Field face = NMSUtils.getFieldSilent(packetclass, "face");

	public int getFace(){
		try{
			return (int)PacketPlayInBlockDig.face.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setFace(int i){
		try{
			PacketPlayInBlockDig.face.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasFace(){
		return PacketPlayInBlockDig.face!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public int getE(){
		try{
			return (int)PacketPlayInBlockDig.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(int i){
		try{
			PacketPlayInBlockDig.e.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayInBlockDig.e!=null;
	}

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getAAsInt(){
		try{
			return (int)PacketPlayInBlockDig.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setAAsInt(int o){
		try{
			PacketPlayInBlockDig.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsInt(){
		return PacketPlayInBlockDig.a!=null&&int.class.isAssignableFrom(PacketPlayInBlockDig.a.getType());
	}

	public Object getAAsObject(){
		try{
			return (Object)PacketPlayInBlockDig.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setAAsObject(Object o){
		try{
			PacketPlayInBlockDig.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsObject(){
		return PacketPlayInBlockDig.a!=null&&Object.class.isAssignableFrom(PacketPlayInBlockDig.a.getType());
	}
	public boolean hasA(){
		return PacketPlayInBlockDig.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getBAsInt(){
		try{
			return (int)PacketPlayInBlockDig.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setBAsInt(int o){
		try{
			PacketPlayInBlockDig.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsInt(){
		return PacketPlayInBlockDig.b!=null&&int.class.isAssignableFrom(PacketPlayInBlockDig.b.getType());
	}

	public Object getBAsObject(){
		try{
			return (Object)PacketPlayInBlockDig.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setBAsObject(Object o){
		try{
			PacketPlayInBlockDig.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsObject(){
		return PacketPlayInBlockDig.b!=null&&Object.class.isAssignableFrom(PacketPlayInBlockDig.b.getType());
	}
	public boolean hasB(){
		return PacketPlayInBlockDig.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getCAsInt(){
		try{
			return (int)PacketPlayInBlockDig.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setCAsInt(int o){
		try{
			PacketPlayInBlockDig.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasCAsInt(){
		return PacketPlayInBlockDig.c!=null&&int.class.isAssignableFrom(PacketPlayInBlockDig.c.getType());
	}

	public Object getCAsObject(){
		try{
			return (Object)PacketPlayInBlockDig.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setCAsObject(Object o){
		try{
			PacketPlayInBlockDig.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasCAsObject(){
		return PacketPlayInBlockDig.c!=null&&Object.class.isAssignableFrom(PacketPlayInBlockDig.c.getType());
	}
	public boolean hasC(){
		return PacketPlayInBlockDig.c!=null;
	}
}
