package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutResourcePackSend extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutResourcePackSend
	//a : java.lang.String
	//b : java.lang.String

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutResourcePackSend
	//a : java.lang.String
	//b : java.lang.String

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutResourcePackSend
	//a : java.lang.String
	//b : java.lang.String

	protected PacketPlayOutResourcePackSend(Object packet){
		super(packet, PacketType.PacketPlayOutResourcePackSend);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutResourcePackSend");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String getA(){
		try{
			return (String)PacketPlayOutResourcePackSend.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(String s){
		try{
			PacketPlayOutResourcePackSend.a.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutResourcePackSend.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public String getB(){
		try{
			return (String)PacketPlayOutResourcePackSend.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(String s){
		try{
			PacketPlayOutResourcePackSend.b.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutResourcePackSend.b!=null;
	}
}
