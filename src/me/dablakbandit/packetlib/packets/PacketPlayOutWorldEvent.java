package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutWorldEvent extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutWorldEvent
	//a : int
	//b : net.minecraft.server.v1_8_R1.BlockPosition
	//c : int
	//d : boolean

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutWorldEvent
	//a : int
	//b : net.minecraft.server.v1_8_R2.BlockPosition
	//c : int
	//d : boolean

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutWorldEvent
	//a : int
	//b : net.minecraft.server.v1_8_R3.BlockPosition
	//c : int
	//d : boolean

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutWorldEvent
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int
	//f : boolean

	protected PacketPlayOutWorldEvent(Object packet){
		super(packet, PacketType.PacketPlayOutWorldEvent);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutWorldEvent");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutWorldEvent.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutWorldEvent.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutWorldEvent.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getBAsInt(){
		try{
			return (int)PacketPlayOutWorldEvent.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setBAsInt(int o){
		try{
			PacketPlayOutWorldEvent.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsInt(){
		return PacketPlayOutWorldEvent.b!=null&&int.class.isAssignableFrom(PacketPlayOutWorldEvent.b.getType());
	}

	public Object getBAsObject(){
		try{
			return (Object)PacketPlayOutWorldEvent.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setBAsObject(Object o){
		try{
			PacketPlayOutWorldEvent.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsObject(){
		return PacketPlayOutWorldEvent.b!=null&&Object.class.isAssignableFrom(PacketPlayOutWorldEvent.b.getType());
	}
	public boolean hasB(){
		return PacketPlayOutWorldEvent.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutWorldEvent.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutWorldEvent.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutWorldEvent.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getDAsInt(){
		try{
			return (int)PacketPlayOutWorldEvent.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setDAsInt(int o){
		try{
			PacketPlayOutWorldEvent.d.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasDAsInt(){
		return PacketPlayOutWorldEvent.d!=null&&int.class.isAssignableFrom(PacketPlayOutWorldEvent.d.getType());
	}

	public boolean getDAsBoolean(){
		try{
			return (boolean)PacketPlayOutWorldEvent.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setDAsBoolean(boolean o){
		try{
			PacketPlayOutWorldEvent.d.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasDAsBoolean(){
		return PacketPlayOutWorldEvent.d!=null&&boolean.class.isAssignableFrom(PacketPlayOutWorldEvent.d.getType());
	}
	public boolean hasD(){
		return PacketPlayOutWorldEvent.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public int getE(){
		try{
			return (int)PacketPlayOutWorldEvent.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(int i){
		try{
			PacketPlayOutWorldEvent.e.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutWorldEvent.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public boolean getF(){
		try{
			return (boolean)PacketPlayOutWorldEvent.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setF(boolean b){
		try{
			PacketPlayOutWorldEvent.f.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutWorldEvent.f!=null;
	}
}
