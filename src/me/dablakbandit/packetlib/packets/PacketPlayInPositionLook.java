package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInPositionLook extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInPositionLook

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInFlying$PacketPlayInPositionLook

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInFlying$PacketPlayInPositionLook

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInPositionLook

	protected PacketPlayInPositionLook(Object packet){
		super(packet, PacketType.PacketPlayInPositionLook);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInPositionLook", "PacketPlayInFlying");

	private static Field x = NMSUtils.getFieldSilent(packetclass, "x");

	public int getX(){
		try{
			return (int)PacketPlayInPositionLook.x.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setX(int i){
		try{
			PacketPlayInPositionLook.x.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasX(){
		return PacketPlayInPositionLook.x!=null;
	}

	private static Field y = NMSUtils.getFieldSilent(packetclass, "y");

	public int getY(){
		try{
			return (int)PacketPlayInPositionLook.y.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setY(int i){
		try{
			PacketPlayInPositionLook.y.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasY(){
		return PacketPlayInPositionLook.y!=null;
	}

	private static Field z = NMSUtils.getFieldSilent(packetclass, "z");

	public int getZ(){
		try{
			return (int)PacketPlayInPositionLook.z.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setZ(int i){
		try{
			PacketPlayInPositionLook.z.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasZ(){
		return PacketPlayInPositionLook.z!=null;
	}

	private static Field stance = NMSUtils.getFieldSilent(packetclass, "stance");

	public double getStance(){
		try{
			return (double)PacketPlayInPositionLook.stance.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}

	public void setStance(double d){
		try{
			PacketPlayInPositionLook.stance.set(packet, d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasStance(){
		return PacketPlayInPositionLook.stance!=null;
	}

	private static Field yaw = NMSUtils.getFieldSilent(packetclass, "yaw");

	public float getYaw(){
		try{
			return (float)PacketPlayInPositionLook.yaw.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setYaw(float f){
		try{
			PacketPlayInPositionLook.yaw.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasYaw(){
		return PacketPlayInPositionLook.yaw!=null;
	}

	private static Field pitch = NMSUtils.getFieldSilent(packetclass, "pitch");

	public float getPitch(){
		try{
			return (float)PacketPlayInPositionLook.pitch.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setPitch(float f){
		try{
			PacketPlayInPositionLook.pitch.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasPitch(){
		return PacketPlayInPositionLook.pitch!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public boolean getF(){
		try{
			return (boolean)PacketPlayInPositionLook.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setF(boolean b){
		try{
			PacketPlayInPositionLook.f.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayInPositionLook.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public boolean getG(){
		try{
			return (boolean)PacketPlayInPositionLook.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setG(boolean b){
		try{
			PacketPlayInPositionLook.g.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayInPositionLook.g!=null;
	}

	private static Field hasPos = NMSUtils.getFieldSilent(packetclass, "hasPos");

	public boolean getHasPos(){
		try{
			return (boolean)PacketPlayInPositionLook.hasPos.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setHasPos(boolean b){
		try{
			PacketPlayInPositionLook.hasPos.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasHasPos(){
		return PacketPlayInPositionLook.hasPos!=null;
	}

	private static Field hasLook = NMSUtils.getFieldSilent(packetclass, "hasLook");

	public boolean getHasLook(){
		try{
			return (boolean)PacketPlayInPositionLook.hasLook.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setHasLook(boolean b){
		try{
			PacketPlayInPositionLook.hasLook.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasHasLook(){
		return PacketPlayInPositionLook.hasLook!=null;
	}
}
