package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutWorldParticles extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutWorldParticles
	//a : net.minecraft.server.v1_8_R1.EnumParticle
	//b : float
	//c : float
	//d : float
	//e : float
	//f : float
	//g : float
	//h : float
	//i : int
	//j : boolean
	//k : [I

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutWorldParticles
	//a : net.minecraft.server.v1_8_R2.EnumParticle
	//b : float
	//c : float
	//d : float
	//e : float
	//f : float
	//g : float
	//h : float
	//i : int
	//j : boolean
	//k : [I

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles
	//a : net.minecraft.server.v1_8_R3.EnumParticle
	//b : float
	//c : float
	//d : float
	//e : float
	//f : float
	//g : float
	//h : float
	//i : int
	//j : boolean
	//k : [I

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutWorldParticles
	//a : java.lang.String
	//b : float
	//c : float
	//d : float
	//e : float
	//f : float
	//g : float
	//h : float
	//i : int

	protected PacketPlayOutWorldParticles(Object packet){
		super(packet, PacketType.PacketPlayOutWorldParticles);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutWorldParticles");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String getAAsString(){
		try{
			return (String)PacketPlayOutWorldParticles.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setAAsString(String o){
		try{
			PacketPlayOutWorldParticles.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsString(){
		return PacketPlayOutWorldParticles.a!=null&&String.class.isAssignableFrom(PacketPlayOutWorldParticles.a.getType());
	}

	public Object getAAsObject(){
		try{
			return (Object)PacketPlayOutWorldParticles.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setAAsObject(Object o){
		try{
			PacketPlayOutWorldParticles.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsObject(){
		return PacketPlayOutWorldParticles.a!=null&&Object.class.isAssignableFrom(PacketPlayOutWorldParticles.a.getType());
	}
	public boolean hasA(){
		return PacketPlayOutWorldParticles.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public float getB(){
		try{
			return (float)PacketPlayOutWorldParticles.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setB(float f){
		try{
			PacketPlayOutWorldParticles.b.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutWorldParticles.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public float getC(){
		try{
			return (float)PacketPlayOutWorldParticles.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setC(float f){
		try{
			PacketPlayOutWorldParticles.c.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutWorldParticles.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public float getD(){
		try{
			return (float)PacketPlayOutWorldParticles.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setD(float f){
		try{
			PacketPlayOutWorldParticles.d.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutWorldParticles.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public float getE(){
		try{
			return (float)PacketPlayOutWorldParticles.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setE(float f){
		try{
			PacketPlayOutWorldParticles.e.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutWorldParticles.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public float getF(){
		try{
			return (float)PacketPlayOutWorldParticles.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setF(float f){
		try{
			PacketPlayOutWorldParticles.f.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutWorldParticles.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public float getG(){
		try{
			return (float)PacketPlayOutWorldParticles.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setG(float f){
		try{
			PacketPlayOutWorldParticles.g.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayOutWorldParticles.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public float getH(){
		try{
			return (float)PacketPlayOutWorldParticles.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setH(float f){
		try{
			PacketPlayOutWorldParticles.h.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutWorldParticles.h!=null;
	}

	private static Field i = NMSUtils.getFieldSilent(packetclass, "i");

	public int getI(){
		try{
			return (int)PacketPlayOutWorldParticles.i.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setI(int i){
		try{
			PacketPlayOutWorldParticles.i.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasI(){
		return PacketPlayOutWorldParticles.i!=null;
	}

	private static Field j = NMSUtils.getFieldSilent(packetclass, "j");

	public boolean getJ(){
		try{
			return (boolean)PacketPlayOutWorldParticles.j.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setJ(boolean b){
		try{
			PacketPlayOutWorldParticles.j.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasJ(){
		return PacketPlayOutWorldParticles.j!=null;
	}

	private static Field k = NMSUtils.getFieldSilent(packetclass, "k");

	public int[] getK(){
		try{
			return (int[])PacketPlayOutWorldParticles.k.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setK(int[] i){
		try{
			PacketPlayOutWorldParticles.k.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasK(){
		return PacketPlayOutWorldParticles.k!=null;
	}
}
