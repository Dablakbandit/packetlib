package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutUpdateAttributes extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutUpdateAttributes
	//a : int
	//b : java.util.List

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutUpdateAttributes
	//a : int
	//b : java.util.List

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutUpdateAttributes
	//a : int
	//b : java.util.List

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutUpdateAttributes
	//a : int
	//b : java.util.List

	protected PacketPlayOutUpdateAttributes(Object packet){
		super(packet, PacketType.PacketPlayOutUpdateAttributes);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutUpdateAttributes");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutUpdateAttributes.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutUpdateAttributes.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutUpdateAttributes.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayOutUpdateAttributes.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayOutUpdateAttributes.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutUpdateAttributes.b!=null;
	}
}
