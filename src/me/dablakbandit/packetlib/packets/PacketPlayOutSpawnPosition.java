package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutSpawnPosition extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutSpawnPosition
	//position : net.minecraft.server.v1_8_R1.BlockPosition

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutSpawnPosition
	//position : net.minecraft.server.v1_8_R2.BlockPosition

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutSpawnPosition
	//position : net.minecraft.server.v1_8_R3.BlockPosition

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutSpawnPosition
	//x : int
	//y : int
	//z : int

	protected PacketPlayOutSpawnPosition(Object packet){
		super(packet, PacketType.PacketPlayOutSpawnPosition);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutSpawnPosition");

	private static Field position = NMSUtils.getFieldSilent(packetclass, "position");

	public Object getPosition(){
		try{
			return PacketPlayOutSpawnPosition.position.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setPosition(Object o){
		try{
			PacketPlayOutSpawnPosition.position.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasPosition(){
		return PacketPlayOutSpawnPosition.position!=null;
	}

	private static Field x = NMSUtils.getFieldSilent(packetclass, "x");

	public int getX(){
		try{
			return (int)PacketPlayOutSpawnPosition.x.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setX(int i){
		try{
			PacketPlayOutSpawnPosition.x.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasX(){
		return PacketPlayOutSpawnPosition.x!=null;
	}

	private static Field y = NMSUtils.getFieldSilent(packetclass, "y");

	public int getY(){
		try{
			return (int)PacketPlayOutSpawnPosition.y.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setY(int i){
		try{
			PacketPlayOutSpawnPosition.y.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasY(){
		return PacketPlayOutSpawnPosition.y!=null;
	}

	private static Field z = NMSUtils.getFieldSilent(packetclass, "z");

	public int getZ(){
		try{
			return (int)PacketPlayOutSpawnPosition.z.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setZ(int i){
		try{
			PacketPlayOutSpawnPosition.z.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasZ(){
		return PacketPlayOutSpawnPosition.z!=null;
	}
}
