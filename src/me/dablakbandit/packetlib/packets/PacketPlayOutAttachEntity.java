package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutAttachEntity extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutAttachEntity
	//a : int
	//b : int
	//c : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutAttachEntity
	//a : int
	//b : int
	//c : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutAttachEntity
	//a : int
	//b : int
	//c : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutAttachEntity
	//a : int
	//b : int
	//c : int

	protected PacketPlayOutAttachEntity(Object packet){
		super(packet, PacketType.PacketPlayOutAttachEntity);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutAttachEntity");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutAttachEntity.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutAttachEntity.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutAttachEntity.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutAttachEntity.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutAttachEntity.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutAttachEntity.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutAttachEntity.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutAttachEntity.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutAttachEntity.c!=null;
	}
}
