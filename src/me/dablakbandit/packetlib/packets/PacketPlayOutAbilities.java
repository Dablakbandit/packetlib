package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutAbilities extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutAbilities
	//a : boolean
	//b : boolean
	//c : boolean
	//d : boolean
	//e : float
	//f : float

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutAbilities
	//a : boolean
	//b : boolean
	//c : boolean
	//d : boolean
	//e : float
	//f : float

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutAbilities
	//a : boolean
	//b : boolean
	//c : boolean
	//d : boolean
	//e : float
	//f : float

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutAbilities
	//a : boolean
	//b : boolean
	//c : boolean
	//d : boolean
	//e : float
	//f : float

	protected PacketPlayOutAbilities(Object packet){
		super(packet, PacketType.PacketPlayOutAbilities);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutAbilities");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public boolean getA(){
		try{
			return (boolean)PacketPlayOutAbilities.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setA(boolean b){
		try{
			PacketPlayOutAbilities.a.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutAbilities.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public boolean getB(){
		try{
			return (boolean)PacketPlayOutAbilities.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setB(boolean b){
		try{
			PacketPlayOutAbilities.b.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutAbilities.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public boolean getC(){
		try{
			return (boolean)PacketPlayOutAbilities.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setC(boolean b){
		try{
			PacketPlayOutAbilities.c.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutAbilities.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public boolean getD(){
		try{
			return (boolean)PacketPlayOutAbilities.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setD(boolean b){
		try{
			PacketPlayOutAbilities.d.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutAbilities.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public float getE(){
		try{
			return (float)PacketPlayOutAbilities.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setE(float f){
		try{
			PacketPlayOutAbilities.e.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutAbilities.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public float getF(){
		try{
			return (float)PacketPlayOutAbilities.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setF(float f){
		try{
			PacketPlayOutAbilities.f.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutAbilities.f!=null;
	}
}
