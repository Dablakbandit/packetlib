package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInPosition extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInPosition

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInFlying$PacketPlayInPosition

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInFlying$PacketPlayInPosition

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInPosition

	protected PacketPlayInPosition(Object packet){
		super(packet, PacketType.PacketPlayInPosition);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInPosition", "PacketPlayInFlying");

	private static Field x = NMSUtils.getFieldSilent(packetclass, "x");

	public int getX(){
		try{
			return (int)PacketPlayInPosition.x.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setX(int i){
		try{
			PacketPlayInPosition.x.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasX(){
		return PacketPlayInPosition.x!=null;
	}

	private static Field y = NMSUtils.getFieldSilent(packetclass, "y");

	public int getY(){
		try{
			return (int)PacketPlayInPosition.y.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setY(int i){
		try{
			PacketPlayInPosition.y.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasY(){
		return PacketPlayInPosition.y!=null;
	}

	private static Field z = NMSUtils.getFieldSilent(packetclass, "z");

	public int getZ(){
		try{
			return (int)PacketPlayInPosition.z.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setZ(int i){
		try{
			PacketPlayInPosition.z.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasZ(){
		return PacketPlayInPosition.z!=null;
	}

	private static Field stance = NMSUtils.getFieldSilent(packetclass, "stance");

	public double getStance(){
		try{
			return (double)PacketPlayInPosition.stance.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}

	public void setStance(double d){
		try{
			PacketPlayInPosition.stance.set(packet, d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasStance(){
		return PacketPlayInPosition.stance!=null;
	}

	private static Field yaw = NMSUtils.getFieldSilent(packetclass, "yaw");

	public float getYaw(){
		try{
			return (float)PacketPlayInPosition.yaw.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setYaw(float f){
		try{
			PacketPlayInPosition.yaw.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasYaw(){
		return PacketPlayInPosition.yaw!=null;
	}

	private static Field pitch = NMSUtils.getFieldSilent(packetclass, "pitch");

	public float getPitch(){
		try{
			return (float)PacketPlayInPosition.pitch.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setPitch(float f){
		try{
			PacketPlayInPosition.pitch.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasPitch(){
		return PacketPlayInPosition.pitch!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public boolean getF(){
		try{
			return (boolean)PacketPlayInPosition.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setF(boolean b){
		try{
			PacketPlayInPosition.f.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayInPosition.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public boolean getG(){
		try{
			return (boolean)PacketPlayInPosition.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setG(boolean b){
		try{
			PacketPlayInPosition.g.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayInPosition.g!=null;
	}

	private static Field hasPos = NMSUtils.getFieldSilent(packetclass, "hasPos");

	public boolean getHasPos(){
		try{
			return (boolean)PacketPlayInPosition.hasPos.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setHasPos(boolean b){
		try{
			PacketPlayInPosition.hasPos.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasHasPos(){
		return PacketPlayInPosition.hasPos!=null;
	}

	private static Field hasLook = NMSUtils.getFieldSilent(packetclass, "hasLook");

	public boolean getHasLook(){
		try{
			return (boolean)PacketPlayInPosition.hasLook.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setHasLook(boolean b){
		try{
			PacketPlayInPosition.hasLook.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasHasLook(){
		return PacketPlayInPosition.hasLook!=null;
	}
}
