package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutScoreboardTeam extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutScoreboardTeam
	//a : java.lang.String
	//b : java.lang.String
	//c : java.lang.String
	//d : java.lang.String
	//e : java.lang.String
	//f : int
	//g : java.util.Collection
	//h : int
	//i : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutScoreboardTeam
	//a : java.lang.String
	//b : java.lang.String
	//c : java.lang.String
	//d : java.lang.String
	//e : java.lang.String
	//f : int
	//g : java.util.Collection
	//h : int
	//i : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutScoreboardTeam
	//a : java.lang.String
	//b : java.lang.String
	//c : java.lang.String
	//d : java.lang.String
	//e : java.lang.String
	//f : int
	//g : java.util.Collection
	//h : int
	//i : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutScoreboardTeam
	//a : java.lang.String
	//b : java.lang.String
	//c : java.lang.String
	//d : java.lang.String
	//e : java.util.Collection
	//f : int
	//g : int

	protected PacketPlayOutScoreboardTeam(Object packet){
		super(packet, PacketType.PacketPlayOutScoreboardTeam);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutScoreboardTeam");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String getA(){
		try{
			return (String)PacketPlayOutScoreboardTeam.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(String s){
		try{
			PacketPlayOutScoreboardTeam.a.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutScoreboardTeam.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public String getB(){
		try{
			return (String)PacketPlayOutScoreboardTeam.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(String s){
		try{
			PacketPlayOutScoreboardTeam.b.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutScoreboardTeam.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public String getC(){
		try{
			return (String)PacketPlayOutScoreboardTeam.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setC(String s){
		try{
			PacketPlayOutScoreboardTeam.c.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutScoreboardTeam.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public String getD(){
		try{
			return (String)PacketPlayOutScoreboardTeam.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setD(String s){
		try{
			PacketPlayOutScoreboardTeam.d.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutScoreboardTeam.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public Object getEAsObject(){
		try{
			return (Object)PacketPlayOutScoreboardTeam.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setEAsObject(Object o){
		try{
			PacketPlayOutScoreboardTeam.e.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasEAsObject(){
		return PacketPlayOutScoreboardTeam.e!=null&&Object.class.isAssignableFrom(PacketPlayOutScoreboardTeam.e.getType());
	}

	public int getEAsInt(){
		try{
			return (int)PacketPlayOutScoreboardTeam.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setEAsInt(int o){
		try{
			PacketPlayOutScoreboardTeam.e.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasEAsInt(){
		return PacketPlayOutScoreboardTeam.e!=null&&int.class.isAssignableFrom(PacketPlayOutScoreboardTeam.e.getType());
	}
	public boolean hasE(){
		return PacketPlayOutScoreboardTeam.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public int getF(){
		try{
			return (int)PacketPlayOutScoreboardTeam.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setF(int i){
		try{
			PacketPlayOutScoreboardTeam.f.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutScoreboardTeam.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public int getGAsInt(){
		try{
			return (int)PacketPlayOutScoreboardTeam.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setGAsInt(int o){
		try{
			PacketPlayOutScoreboardTeam.g.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasGAsInt(){
		return PacketPlayOutScoreboardTeam.g!=null&&int.class.isAssignableFrom(PacketPlayOutScoreboardTeam.g.getType());
	}

	public Object getGAsObject(){
		try{
			return (Object)PacketPlayOutScoreboardTeam.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setGAsObject(Object o){
		try{
			PacketPlayOutScoreboardTeam.g.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasGAsObject(){
		return PacketPlayOutScoreboardTeam.g!=null&&Object.class.isAssignableFrom(PacketPlayOutScoreboardTeam.g.getType());
	}
	public boolean hasG(){
		return PacketPlayOutScoreboardTeam.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public int getH(){
		try{
			return (int)PacketPlayOutScoreboardTeam.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setH(int i){
		try{
			PacketPlayOutScoreboardTeam.h.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutScoreboardTeam.h!=null;
	}

	private static Field i = NMSUtils.getFieldSilent(packetclass, "i");

	public int getI(){
		try{
			return (int)PacketPlayOutScoreboardTeam.i.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setI(int i){
		try{
			PacketPlayOutScoreboardTeam.i.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasI(){
		return PacketPlayOutScoreboardTeam.i!=null;
	}
}
