package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutMapChunk extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutMapChunk
	//a : int
	//b : int
	//c : net.minecraft.server.v1_8_R1.ChunkMap
	//d : boolean

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutMapChunk
	//a : int
	//b : int
	//c : net.minecraft.server.v1_8_R2.PacketPlayOutMapChunk$ChunkMap
	//d : boolean

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutMapChunk
	//a : int
	//b : int
	//c : net.minecraft.server.v1_8_R3.PacketPlayOutMapChunk$ChunkMap
	//d : boolean

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutMapChunk
	//a : int
	//b : int
	//c : int
	//d : int
	//e : [B
	//f : [B
	//g : boolean
	//h : int
	//i : [B
	//chunk : net.minecraft.server.v1_7_R4.Chunk
	//mask : int

	protected PacketPlayOutMapChunk(Object packet){
		super(packet, PacketType.PacketPlayOutMapChunk);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutMapChunk");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutMapChunk.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutMapChunk.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutMapChunk.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutMapChunk.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutMapChunk.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutMapChunk.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getCAsInt(){
		try{
			return (int)PacketPlayOutMapChunk.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setCAsInt(int o){
		try{
			PacketPlayOutMapChunk.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasCAsInt(){
		return PacketPlayOutMapChunk.c!=null&&int.class.isAssignableFrom(PacketPlayOutMapChunk.c.getType());
	}

	public Object getCAsObject(){
		try{
			return (Object)PacketPlayOutMapChunk.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setCAsObject(Object o){
		try{
			PacketPlayOutMapChunk.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasCAsObject(){
		return PacketPlayOutMapChunk.c!=null&&Object.class.isAssignableFrom(PacketPlayOutMapChunk.c.getType());
	}
	public boolean hasC(){
		return PacketPlayOutMapChunk.c!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public byte[] getE(){
		try{
			return (byte[])PacketPlayOutMapChunk.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setE(byte[] b){
		try{
			PacketPlayOutMapChunk.e.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutMapChunk.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public byte[] getF(){
		try{
			return (byte[])PacketPlayOutMapChunk.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setF(byte[] b){
		try{
			PacketPlayOutMapChunk.f.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutMapChunk.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public boolean getG(){
		try{
			return (boolean)PacketPlayOutMapChunk.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setG(boolean b){
		try{
			PacketPlayOutMapChunk.g.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayOutMapChunk.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public int getH(){
		try{
			return (int)PacketPlayOutMapChunk.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setH(int i){
		try{
			PacketPlayOutMapChunk.h.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutMapChunk.h!=null;
	}

	private static Field i = NMSUtils.getFieldSilent(packetclass, "i");

	public byte[] getI(){
		try{
			return (byte[])PacketPlayOutMapChunk.i.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setI(byte[] b){
		try{
			PacketPlayOutMapChunk.i.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasI(){
		return PacketPlayOutMapChunk.i!=null;
	}

	private static Field chunk = NMSUtils.getFieldSilent(packetclass, "chunk");

	public Object getChunk(){
		try{
			return PacketPlayOutMapChunk.chunk.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setChunk(Object o){
		try{
			PacketPlayOutMapChunk.chunk.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasChunk(){
		return PacketPlayOutMapChunk.chunk!=null;
	}

	private static Field mask = NMSUtils.getFieldSilent(packetclass, "mask");

	public int getMask(){
		try{
			return (int)PacketPlayOutMapChunk.mask.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setMask(int i){
		try{
			PacketPlayOutMapChunk.mask.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasMask(){
		return PacketPlayOutMapChunk.mask!=null;
	}
}
