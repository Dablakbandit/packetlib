package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutCombatEvent extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutCombatEvent
	//a : net.minecraft.server.v1_8_R1.EnumCombatEventType
	//b : int
	//c : int
	//d : int
	//e : java.lang.String

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutCombatEvent
	//a : net.minecraft.server.v1_8_R2.PacketPlayOutCombatEvent$EnumCombatEventType
	//b : int
	//c : int
	//d : int
	//e : java.lang.String

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutCombatEvent
	//a : net.minecraft.server.v1_8_R3.PacketPlayOutCombatEvent$EnumCombatEventType
	//b : int
	//c : int
	//d : int
	//e : java.lang.String

	protected PacketPlayOutCombatEvent(Object packet){
		super(packet, PacketType.PacketPlayOutCombatEvent);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutCombatEvent");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketPlayOutCombatEvent.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketPlayOutCombatEvent.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutCombatEvent.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutCombatEvent.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutCombatEvent.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutCombatEvent.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutCombatEvent.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutCombatEvent.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutCombatEvent.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutCombatEvent.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutCombatEvent.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutCombatEvent.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public short getE(){
		try{
			return (short)PacketPlayOutCombatEvent.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(short s){
		try{
			PacketPlayOutCombatEvent.e.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutCombatEvent.e!=null;
	}
}
