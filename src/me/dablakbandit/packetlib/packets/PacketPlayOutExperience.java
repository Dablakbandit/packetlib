package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutExperience extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutExperience
	//a : float
	//b : int
	//c : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutExperience
	//a : float
	//b : int
	//c : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutExperience
	//a : float
	//b : int
	//c : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutExperience
	//a : float
	//b : int
	//c : int

	protected PacketPlayOutExperience(Object packet){
		super(packet, PacketType.PacketPlayOutExperience);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutExperience");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public float getA(){
		try{
			return (float)PacketPlayOutExperience.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setA(float f){
		try{
			PacketPlayOutExperience.a.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutExperience.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public float getB(){
		try{
			return (float)PacketPlayOutExperience.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setB(float f){
		try{
			PacketPlayOutExperience.b.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutExperience.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public float getC(){
		try{
			return (float)PacketPlayOutExperience.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setC(float f){
		try{
			PacketPlayOutExperience.c.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutExperience.c!=null;
	}
}
