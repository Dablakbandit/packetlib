package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInSetCreativeSlot extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInSetCreativeSlot
	//slot : int
	//b : net.minecraft.server.v1_8_R1.ItemStack

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInSetCreativeSlot
	//slot : int
	//b : net.minecraft.server.v1_8_R2.ItemStack

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInSetCreativeSlot
	//slot : int
	//b : net.minecraft.server.v1_8_R3.ItemStack

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInSetCreativeSlot
	//slot : int
	//b : net.minecraft.server.v1_7_R4.ItemStack

	protected PacketPlayInSetCreativeSlot(Object packet){
		super(packet, PacketType.PacketPlayInSetCreativeSlot);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInSetCreativeSlot");

	private static Field slot = NMSUtils.getFieldSilent(packetclass, "slot");

	public int getSlot(){
		try{
			return (int)PacketPlayInSetCreativeSlot.slot.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setSlot(int i){
		try{
			PacketPlayInSetCreativeSlot.slot.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasSlot(){
		return PacketPlayInSetCreativeSlot.slot!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayInSetCreativeSlot.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayInSetCreativeSlot.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayInSetCreativeSlot.b!=null;
	}
}
