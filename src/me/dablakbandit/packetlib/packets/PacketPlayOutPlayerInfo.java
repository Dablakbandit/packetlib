package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutPlayerInfo extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutPlayerInfo
	//a : net.minecraft.server.v1_8_R1.EnumPlayerInfoAction
	//b : java.util.List

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutPlayerInfo
	//a : net.minecraft.server.v1_8_R2.PacketPlayOutPlayerInfo$EnumPlayerInfoAction
	//b : java.util.List

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo
	//a : net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo$EnumPlayerInfoAction
	//b : java.util.List

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutPlayerInfo
	//ADD_PLAYER : int
	//UPDATE_GAMEMODE : int
	//UPDATE_LATENCY : int
	//UPDATE_DISPLAY_NAME : int
	//REMOVE_PLAYER : int
	//action : int
	//player : net.minecraft.util.com.mojang.authlib.GameProfile
	//gamemode : int
	//ping : int
	//username : java.lang.String

	protected PacketPlayOutPlayerInfo(Object packet){
		super(packet, PacketType.PacketPlayOutPlayerInfo);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutPlayerInfo");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketPlayOutPlayerInfo.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketPlayOutPlayerInfo.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutPlayerInfo.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayOutPlayerInfo.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayOutPlayerInfo.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutPlayerInfo.b!=null;
	}

	private static Field action = NMSUtils.getFieldSilent(packetclass, "action");

	public int getAction(){
		try{
			return (int)PacketPlayOutPlayerInfo.action.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setAction(int i){
		try{
			PacketPlayOutPlayerInfo.action.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAction(){
		return PacketPlayOutPlayerInfo.action!=null;
	}

	private static Field player = NMSUtils.getFieldSilent(packetclass, "player");

	public Object getPlayer(){
		try{
			return PacketPlayOutPlayerInfo.player.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setPlayer(Object o){
		try{
			PacketPlayOutPlayerInfo.player.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasPlayer(){
		return PacketPlayOutPlayerInfo.player!=null;
	}

	private static Field gamemode = NMSUtils.getFieldSilent(packetclass, "gamemode");

	public int getGamemode(){
		try{
			return (int)PacketPlayOutPlayerInfo.gamemode.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setGamemode(int i){
		try{
			PacketPlayOutPlayerInfo.gamemode.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasGamemode(){
		return PacketPlayOutPlayerInfo.gamemode!=null;
	}

	private static Field ping = NMSUtils.getFieldSilent(packetclass, "ping");

	public int getPing(){
		try{
			return (int)PacketPlayOutPlayerInfo.ping.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setPing(int i){
		try{
			PacketPlayOutPlayerInfo.ping.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasPing(){
		return PacketPlayOutPlayerInfo.ping!=null;
	}

	private static Field username = NMSUtils.getFieldSilent(packetclass, "username");

	public String getUsername(){
		try{
			return (String)PacketPlayOutPlayerInfo.username.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setUsername(String s){
		try{
			PacketPlayOutPlayerInfo.username.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasUsername(){
		return PacketPlayOutPlayerInfo.username!=null;
	}
}
