package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutPosition extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutPosition
	//a : double
	//b : double
	//c : double
	//d : float
	//e : float
	//f : java.util.Set

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutPosition
	//a : double
	//b : double
	//c : double
	//d : float
	//e : float
	//f : java.util.Set

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutPosition
	//a : double
	//b : double
	//c : double
	//d : float
	//e : float
	//f : java.util.Set

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutPosition
	//a : double
	//b : double
	//c : double
	//d : float
	//e : float
	//f : boolean
	//relativeBitMask : byte

	protected PacketPlayOutPosition(Object packet){
		super(packet, PacketType.PacketPlayOutPosition);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutPosition");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public double getA(){
		try{
			return (double)PacketPlayOutPosition.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}

	public void setA(double d){
		try{
			PacketPlayOutPosition.a.set(packet, d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutPosition.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public double getB(){
		try{
			return (double)PacketPlayOutPosition.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}

	public void setB(double d){
		try{
			PacketPlayOutPosition.b.set(packet, d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutPosition.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public double getC(){
		try{
			return (double)PacketPlayOutPosition.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}

	public void setC(double d){
		try{
			PacketPlayOutPosition.c.set(packet, d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutPosition.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public float getD(){
		try{
			return (float)PacketPlayOutPosition.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setD(float f){
		try{
			PacketPlayOutPosition.d.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutPosition.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public float getE(){
		try{
			return (float)PacketPlayOutPosition.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setE(float f){
		try{
			PacketPlayOutPosition.e.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutPosition.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public boolean getFAsBoolean(){
		try{
			return (boolean)PacketPlayOutPosition.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setFAsBoolean(boolean o){
		try{
			PacketPlayOutPosition.f.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasFAsBoolean(){
		return PacketPlayOutPosition.f!=null&&boolean.class.isAssignableFrom(PacketPlayOutPosition.f.getType());
	}

	public Object getFAsObject(){
		try{
			return (Object)PacketPlayOutPosition.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setFAsObject(Object o){
		try{
			PacketPlayOutPosition.f.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasFAsObject(){
		return PacketPlayOutPosition.f!=null&&Object.class.isAssignableFrom(PacketPlayOutPosition.f.getType());
	}
	public boolean hasF(){
		return PacketPlayOutPosition.f!=null;
	}

	private static Field relativeBitMask = NMSUtils.getFieldSilent(packetclass, "relativeBitMask");

	public byte getRelativeBitMask(){
		try{
			return (byte)PacketPlayOutPosition.relativeBitMask.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setRelativeBitMask(byte b){
		try{
			PacketPlayOutPosition.relativeBitMask.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasRelativeBitMask(){
		return PacketPlayOutPosition.relativeBitMask!=null;
	}
}
