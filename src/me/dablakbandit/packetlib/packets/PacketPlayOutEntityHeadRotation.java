package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutEntityHeadRotation extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutEntityHeadRotation
	//a : int
	//b : byte

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutEntityHeadRotation
	//a : int
	//b : byte

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutEntityHeadRotation
	//a : int
	//b : byte

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutEntityHeadRotation
	//a : int
	//b : byte

	protected PacketPlayOutEntityHeadRotation(Object packet){
		super(packet, PacketType.PacketPlayOutEntityHeadRotation);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutEntityHeadRotation");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutEntityHeadRotation.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutEntityHeadRotation.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutEntityHeadRotation.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public byte getB(){
		try{
			return (byte)PacketPlayOutEntityHeadRotation.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(byte b){
		try{
			PacketPlayOutEntityHeadRotation.b.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutEntityHeadRotation.b!=null;
	}
}
