package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutBlockChange extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutBlockChange
	//a : net.minecraft.server.v1_8_R1.BlockPosition
	//block : net.minecraft.server.v1_8_R1.IBlockData

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutBlockChange
	//a : net.minecraft.server.v1_8_R2.BlockPosition
	//block : net.minecraft.server.v1_8_R2.IBlockData

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutBlockChange
	//a : net.minecraft.server.v1_8_R3.BlockPosition
	//block : net.minecraft.server.v1_8_R3.IBlockData

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutBlockChange
	//a : int
	//b : int
	//c : int
	//block : net.minecraft.server.v1_7_R4.Block
	//data : int

	protected PacketPlayOutBlockChange(Object packet){
		super(packet, PacketType.PacketPlayOutBlockChange);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutBlockChange");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getAAsInt(){
		try{
			return (int)PacketPlayOutBlockChange.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setAAsInt(int o){
		try{
			PacketPlayOutBlockChange.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsInt(){
		return PacketPlayOutBlockChange.a!=null&&int.class.isAssignableFrom(PacketPlayOutBlockChange.a.getType());
	}

	public Object getAAsObject(){
		try{
			return (Object)PacketPlayOutBlockChange.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setAAsObject(Object o){
		try{
			PacketPlayOutBlockChange.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsObject(){
		return PacketPlayOutBlockChange.a!=null&&Object.class.isAssignableFrom(PacketPlayOutBlockChange.a.getType());
	}
	public boolean hasA(){
		return PacketPlayOutBlockChange.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getBAsInt(){
		try{
			return (int)PacketPlayOutBlockChange.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setBAsInt(int o){
		try{
			PacketPlayOutBlockChange.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsInt(){
		return PacketPlayOutBlockChange.b!=null&&int.class.isAssignableFrom(PacketPlayOutBlockChange.b.getType());
	}

	public Object getBAsObject(){
		try{
			return (Object)PacketPlayOutBlockChange.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setBAsObject(Object o){
		try{
			PacketPlayOutBlockChange.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsObject(){
		return PacketPlayOutBlockChange.b!=null&&Object.class.isAssignableFrom(PacketPlayOutBlockChange.b.getType());
	}
	public boolean hasB(){
		return PacketPlayOutBlockChange.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutBlockChange.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutBlockChange.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutBlockChange.c!=null;
	}

	private static Field block = NMSUtils.getFieldSilent(packetclass, "block");

	public Object getBlock(){
		try{
			return PacketPlayOutBlockChange.block.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setBlock(Object o){
		try{
			PacketPlayOutBlockChange.block.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBlock(){
		return PacketPlayOutBlockChange.block!=null;
	}

	private static Field data = NMSUtils.getFieldSilent(packetclass, "data");

	public int getData(){
		try{
			return (int)PacketPlayOutBlockChange.data.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setData(int i){
		try{
			PacketPlayOutBlockChange.data.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasData(){
		return PacketPlayOutBlockChange.data!=null;
	}
}
