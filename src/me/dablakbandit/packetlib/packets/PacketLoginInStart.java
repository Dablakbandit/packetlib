package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketLoginInStart extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketLoginInStart
	//a : com.mojang.authlib.GameProfile

	//Fields
	//net.minecraft.server.v1_8_R2.PacketLoginInStart
	//a : com.mojang.authlib.GameProfile

	//Fields
	//net.minecraft.server.v1_8_R3.PacketLoginInStart
	//a : com.mojang.authlib.GameProfile

	//Fields
	//net.minecraft.server.v1_7_R4.PacketLoginInStart
	//a : net.minecraft.util.com.mojang.authlib.GameProfile

	protected PacketLoginInStart(Object packet){
		super(packet, PacketType.PacketLoginInStart);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketLoginInStart");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketLoginInStart.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketLoginInStart.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketLoginInStart.a!=null;
	}
}
