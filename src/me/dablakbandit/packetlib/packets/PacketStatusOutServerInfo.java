package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketStatusOutServerInfo extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketStatusOutServerInfo
	//a : org.bukkit.craftbukkit.libs.com.google.gson.Gson
	//b : net.minecraft.server.v1_8_R1.ServerPing

	//Fields
	//net.minecraft.server.v1_8_R2.PacketStatusOutServerInfo
	//a : com.google.gson.Gson
	//b : net.minecraft.server.v1_8_R2.ServerPing

	//Fields
	//net.minecraft.server.v1_8_R3.PacketStatusOutServerInfo
	//a : com.google.gson.Gson
	//b : net.minecraft.server.v1_8_R3.ServerPing

	//Fields
	//net.minecraft.server.v1_7_R4.PacketStatusOutServerInfo
	//a : net.minecraft.util.com.google.gson.Gson
	//b : net.minecraft.server.v1_7_R4.ServerPing

	protected PacketStatusOutServerInfo(Object packet){
		super(packet, PacketType.PacketStatusOutServerInfo);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketStatusOutServerInfo");

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketStatusOutServerInfo.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketStatusOutServerInfo.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketStatusOutServerInfo.b!=null;
	}
}
