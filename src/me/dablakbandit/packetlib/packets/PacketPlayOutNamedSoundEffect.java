package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutNamedSoundEffect extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutNamedSoundEffect
	//a : java.lang.String
	//b : int
	//c : int
	//d : int
	//e : float
	//f : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutNamedSoundEffect
	//a : java.lang.String
	//b : int
	//c : int
	//d : int
	//e : float
	//f : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutNamedSoundEffect
	//a : java.lang.String
	//b : int
	//c : int
	//d : int
	//e : float
	//f : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutNamedSoundEffect
	//a : java.lang.String
	//b : int
	//c : int
	//d : int
	//e : float
	//f : int

	protected PacketPlayOutNamedSoundEffect(Object packet){
		super(packet, PacketType.PacketPlayOutNamedSoundEffect);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutNamedSoundEffect");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String getA(){
		try{
			return (String)PacketPlayOutNamedSoundEffect.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(String s){
		try{
			PacketPlayOutNamedSoundEffect.a.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutNamedSoundEffect.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutNamedSoundEffect.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutNamedSoundEffect.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutNamedSoundEffect.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutNamedSoundEffect.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutNamedSoundEffect.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutNamedSoundEffect.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutNamedSoundEffect.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutNamedSoundEffect.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutNamedSoundEffect.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public float getE(){
		try{
			return (float)PacketPlayOutNamedSoundEffect.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setE(float f){
		try{
			PacketPlayOutNamedSoundEffect.e.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutNamedSoundEffect.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public int getF(){
		try{
			return (int)PacketPlayOutNamedSoundEffect.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setF(int i){
		try{
			PacketPlayOutNamedSoundEffect.f.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutNamedSoundEffect.f!=null;
	}
}
