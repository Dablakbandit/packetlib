package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInAbilities extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInAbilities
	//a : boolean
	//b : boolean
	//c : boolean
	//d : boolean
	//e : float
	//f : float

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInAbilities
	//a : boolean
	//b : boolean
	//c : boolean
	//d : boolean
	//e : float
	//f : float

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInAbilities
	//a : boolean
	//b : boolean
	//c : boolean
	//d : boolean
	//e : float
	//f : float

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInAbilities
	//a : boolean
	//b : boolean
	//c : boolean
	//d : boolean
	//e : float
	//f : float

	protected PacketPlayInAbilities(Object packet){
		super(packet, PacketType.PacketPlayInAbilities);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInAbilities");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public boolean getA(){
		try{
			return (boolean)PacketPlayInAbilities.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setA(boolean b){
		try{
			PacketPlayInAbilities.a.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInAbilities.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public boolean getB(){
		try{
			return (boolean)PacketPlayInAbilities.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setB(boolean b){
		try{
			PacketPlayInAbilities.b.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayInAbilities.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public boolean getC(){
		try{
			return (boolean)PacketPlayInAbilities.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setC(boolean b){
		try{
			PacketPlayInAbilities.c.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayInAbilities.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public boolean getD(){
		try{
			return (boolean)PacketPlayInAbilities.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setD(boolean b){
		try{
			PacketPlayInAbilities.d.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayInAbilities.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public float getE(){
		try{
			return (float)PacketPlayInAbilities.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setE(float f){
		try{
			PacketPlayInAbilities.e.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayInAbilities.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public float getF(){
		try{
			return (float)PacketPlayInAbilities.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setF(float f){
		try{
			PacketPlayInAbilities.f.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayInAbilities.f!=null;
	}
}
