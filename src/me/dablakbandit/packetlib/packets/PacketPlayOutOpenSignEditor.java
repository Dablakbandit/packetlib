package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutOpenSignEditor extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutOpenSignEditor
	//a : net.minecraft.server.v1_8_R1.BlockPosition

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutOpenSignEditor
	//a : net.minecraft.server.v1_8_R2.BlockPosition

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutOpenSignEditor
	//a : net.minecraft.server.v1_8_R3.BlockPosition

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutOpenSignEditor
	//a : int
	//b : int
	//c : int

	protected PacketPlayOutOpenSignEditor(Object packet){
		super(packet, PacketType.PacketPlayOutOpenSignEditor);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutOpenSignEditor");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getAAsInt(){
		try{
			return (int)PacketPlayOutOpenSignEditor.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setAAsInt(int o){
		try{
			PacketPlayOutOpenSignEditor.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsInt(){
		return PacketPlayOutOpenSignEditor.a!=null&&int.class.isAssignableFrom(PacketPlayOutOpenSignEditor.a.getType());
	}

	public Object getAAsObject(){
		try{
			return (Object)PacketPlayOutOpenSignEditor.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setAAsObject(Object o){
		try{
			PacketPlayOutOpenSignEditor.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsObject(){
		return PacketPlayOutOpenSignEditor.a!=null&&Object.class.isAssignableFrom(PacketPlayOutOpenSignEditor.a.getType());
	}
	public boolean hasA(){
		return PacketPlayOutOpenSignEditor.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutOpenSignEditor.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutOpenSignEditor.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutOpenSignEditor.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutOpenSignEditor.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutOpenSignEditor.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutOpenSignEditor.c!=null;
	}
}
