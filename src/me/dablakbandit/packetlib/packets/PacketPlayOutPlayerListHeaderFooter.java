package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutPlayerListHeaderFooter extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutPlayerListHeaderFooter
	//a : net.minecraft.server.v1_8_R1.IChatBaseComponent
	//b : net.minecraft.server.v1_8_R1.IChatBaseComponent

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutPlayerListHeaderFooter
	//a : net.minecraft.server.v1_8_R2.IChatBaseComponent
	//b : net.minecraft.server.v1_8_R2.IChatBaseComponent

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter
	//a : net.minecraft.server.v1_8_R3.IChatBaseComponent
	//b : net.minecraft.server.v1_8_R3.IChatBaseComponent

	protected PacketPlayOutPlayerListHeaderFooter(Object packet){
		super(packet, PacketType.PacketPlayOutPlayerListHeaderFooter);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutPlayerListHeaderFooter");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketPlayOutPlayerListHeaderFooter.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketPlayOutPlayerListHeaderFooter.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutPlayerListHeaderFooter.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayOutPlayerListHeaderFooter.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayOutPlayerListHeaderFooter.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutPlayerListHeaderFooter.b!=null;
	}
}
