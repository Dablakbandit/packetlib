package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutSpawnEntityExperienceOrb extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutSpawnEntityExperienceOrb
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutSpawnEntityExperienceOrb
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityExperienceOrb
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutSpawnEntityExperienceOrb
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int

	protected PacketPlayOutSpawnEntityExperienceOrb(Object packet){
		super(packet, PacketType.PacketPlayOutSpawnEntityExperienceOrb);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutSpawnEntityExperienceOrb");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutSpawnEntityExperienceOrb.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutSpawnEntityExperienceOrb.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutSpawnEntityExperienceOrb.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutSpawnEntityExperienceOrb.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutSpawnEntityExperienceOrb.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutSpawnEntityExperienceOrb.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutSpawnEntityExperienceOrb.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutSpawnEntityExperienceOrb.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutSpawnEntityExperienceOrb.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutSpawnEntityExperienceOrb.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutSpawnEntityExperienceOrb.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutSpawnEntityExperienceOrb.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public int getE(){
		try{
			return (int)PacketPlayOutSpawnEntityExperienceOrb.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(int i){
		try{
			PacketPlayOutSpawnEntityExperienceOrb.e.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutSpawnEntityExperienceOrb.e!=null;
	}
}
