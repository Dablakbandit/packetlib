package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutCamera extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutCamera
	//a : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutCamera
	//a : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutCamera
	//a : int

	protected PacketPlayOutCamera(Object packet){
		super(packet, PacketType.PacketPlayOutCamera);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutCamera");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutCamera.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutCamera.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutCamera.a!=null;
	}
}
