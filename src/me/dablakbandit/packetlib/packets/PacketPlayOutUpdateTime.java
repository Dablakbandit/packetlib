package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutUpdateTime extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutUpdateTime
	//a : long
	//b : long

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutUpdateTime
	//a : long
	//b : long

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutUpdateTime
	//a : long
	//b : long

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutUpdateTime
	//a : long
	//b : long

	protected PacketPlayOutUpdateTime(Object packet){
		super(packet, PacketType.PacketPlayOutUpdateTime);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutUpdateTime");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public long getA(){
		try{
			return (long)PacketPlayOutUpdateTime.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(long l){
		try{
			PacketPlayOutUpdateTime.a.set(packet, l);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutUpdateTime.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public long getB(){
		try{
			return (long)PacketPlayOutUpdateTime.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(long l){
		try{
			PacketPlayOutUpdateTime.b.set(packet, l);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutUpdateTime.b!=null;
	}
}
