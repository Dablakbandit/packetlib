package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutUpdateSign extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutUpdateSign
	//a : net.minecraft.server.v1_8_R1.World
	//b : net.minecraft.server.v1_8_R1.BlockPosition
	//c : [Lnet.minecraft.server.v1_8_R1.IChatBaseComponent;

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutUpdateSign
	//a : net.minecraft.server.v1_8_R2.World
	//b : net.minecraft.server.v1_8_R2.BlockPosition
	//c : [Lnet.minecraft.server.v1_8_R2.IChatBaseComponent;

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutUpdateSign
	//a : net.minecraft.server.v1_8_R3.World
	//b : net.minecraft.server.v1_8_R3.BlockPosition
	//c : [Lnet.minecraft.server.v1_8_R3.IChatBaseComponent;

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutUpdateSign
	//x : int
	//y : int
	//z : int
	//lines : [Ljava.lang.String;

	protected PacketPlayOutUpdateSign(Object packet){
		super(packet, PacketType.PacketPlayOutUpdateSign);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutUpdateSign");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketPlayOutUpdateSign.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketPlayOutUpdateSign.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutUpdateSign.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayOutUpdateSign.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayOutUpdateSign.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutUpdateSign.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public Object getC(){
		try{
			return PacketPlayOutUpdateSign.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setC(Object o){
		try{
			PacketPlayOutUpdateSign.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutUpdateSign.c!=null;
	}

	private static Field x = NMSUtils.getFieldSilent(packetclass, "x");

	public int getX(){
		try{
			return (int)PacketPlayOutUpdateSign.x.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setX(int i){
		try{
			PacketPlayOutUpdateSign.x.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasX(){
		return PacketPlayOutUpdateSign.x!=null;
	}

	private static Field y = NMSUtils.getFieldSilent(packetclass, "y");

	public int getY(){
		try{
			return (int)PacketPlayOutUpdateSign.y.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setY(int i){
		try{
			PacketPlayOutUpdateSign.y.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasY(){
		return PacketPlayOutUpdateSign.y!=null;
	}

	private static Field z = NMSUtils.getFieldSilent(packetclass, "z");

	public int getZ(){
		try{
			return (int)PacketPlayOutUpdateSign.z.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setZ(int i){
		try{
			PacketPlayOutUpdateSign.z.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasZ(){
		return PacketPlayOutUpdateSign.z!=null;
	}

	private static Field lines = NMSUtils.getFieldSilent(packetclass, "lines");

	public Object getLines(){
		try{
			return PacketPlayOutUpdateSign.lines.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setLines(Object o){
		try{
			PacketPlayOutUpdateSign.lines.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasLines(){
		return PacketPlayOutUpdateSign.lines!=null;
	}
}
