package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInKeepAlive extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInKeepAlive
	//a : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInKeepAlive
	//a : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInKeepAlive
	//a : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInKeepAlive
	//a : int

	protected PacketPlayInKeepAlive(Object packet){
		super(packet, PacketType.PacketPlayInKeepAlive);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInKeepAlive");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayInKeepAlive.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayInKeepAlive.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInKeepAlive.a!=null;
	}
}
