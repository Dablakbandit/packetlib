package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInEnchantItem extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInEnchantItem
	//a : int
	//b : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInEnchantItem
	//a : int
	//b : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInEnchantItem
	//a : int
	//b : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInEnchantItem
	//a : int
	//b : int

	protected PacketPlayInEnchantItem(Object packet){
		super(packet, PacketType.PacketPlayInEnchantItem);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInEnchantItem");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayInEnchantItem.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayInEnchantItem.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInEnchantItem.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayInEnchantItem.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayInEnchantItem.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayInEnchantItem.b!=null;
	}
}
