package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutServerDifficulty extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutServerDifficulty
	//a : net.minecraft.server.v1_8_R1.EnumDifficulty
	//b : boolean

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutServerDifficulty
	//a : net.minecraft.server.v1_8_R2.EnumDifficulty
	//b : boolean

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutServerDifficulty
	//a : net.minecraft.server.v1_8_R3.EnumDifficulty
	//b : boolean

	protected PacketPlayOutServerDifficulty(Object packet){
		super(packet, PacketType.PacketPlayOutServerDifficulty);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutServerDifficulty");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketPlayOutServerDifficulty.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketPlayOutServerDifficulty.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutServerDifficulty.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayOutServerDifficulty.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayOutServerDifficulty.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutServerDifficulty.b!=null;
	}
}
