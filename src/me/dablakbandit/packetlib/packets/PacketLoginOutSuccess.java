package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketLoginOutSuccess extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketLoginOutSuccess
	//a : com.mojang.authlib.GameProfile

	//Fields
	//net.minecraft.server.v1_8_R2.PacketLoginOutSuccess
	//a : com.mojang.authlib.GameProfile

	//Fields
	//net.minecraft.server.v1_8_R3.PacketLoginOutSuccess
	//a : com.mojang.authlib.GameProfile

	//Fields
	//net.minecraft.server.v1_7_R4.PacketLoginOutSuccess
	//a : net.minecraft.util.com.mojang.authlib.GameProfile

	protected PacketLoginOutSuccess(Object packet){
		super(packet, PacketType.PacketLoginOutSuccess);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketLoginOutSuccess");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketLoginOutSuccess.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketLoginOutSuccess.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketLoginOutSuccess.a!=null;
	}
}
