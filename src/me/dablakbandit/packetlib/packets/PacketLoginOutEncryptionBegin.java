package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketLoginOutEncryptionBegin extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketLoginOutEncryptionBegin
	//a : java.lang.String
	//b : java.security.PublicKey
	//c : [B

	//Fields
	//net.minecraft.server.v1_8_R2.PacketLoginOutEncryptionBegin
	//a : java.lang.String
	//b : java.security.PublicKey
	//c : [B

	//Fields
	//net.minecraft.server.v1_8_R3.PacketLoginOutEncryptionBegin
	//a : java.lang.String
	//b : java.security.PublicKey
	//c : [B

	//Fields
	//net.minecraft.server.v1_7_R4.PacketLoginOutEncryptionBegin
	//a : java.lang.String
	//b : java.security.PublicKey
	//c : [B

	protected PacketLoginOutEncryptionBegin(Object packet){
		super(packet, PacketType.PacketLoginOutEncryptionBegin);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketLoginOutEncryptionBegin");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String getA(){
		try{
			return (String)PacketLoginOutEncryptionBegin.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(String s){
		try{
			PacketLoginOutEncryptionBegin.a.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketLoginOutEncryptionBegin.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketLoginOutEncryptionBegin.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketLoginOutEncryptionBegin.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketLoginOutEncryptionBegin.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public byte[] getC(){
		try{
			return (byte[])PacketLoginOutEncryptionBegin.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setC(byte[] b){
		try{
			PacketLoginOutEncryptionBegin.c.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketLoginOutEncryptionBegin.c!=null;
	}
}
