package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutMap extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutMap
	//a : int
	//b : byte
	//c : [Lnet.minecraft.server.v1_8_R1.MapIcon;
	//d : int
	//e : int
	//f : int
	//g : int
	//h : [B

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutMap
	//a : int
	//b : byte
	//c : [Lnet.minecraft.server.v1_8_R2.MapIcon;
	//d : int
	//e : int
	//f : int
	//g : int
	//h : [B

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutMap
	//a : int
	//b : byte
	//c : [Lnet.minecraft.server.v1_8_R3.MapIcon;
	//d : int
	//e : int
	//f : int
	//g : int
	//h : [B

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutMap
	//a : int
	//b : [B
	//scale : byte

	protected PacketPlayOutMap(Object packet){
		super(packet, PacketType.PacketPlayOutMap);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutMap");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutMap.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutMap.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutMap.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public byte[] getBAsByteArray(){
		try{
			return (byte[])PacketPlayOutMap.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setBAsByteArray(byte[] o){
		try{
			PacketPlayOutMap.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsByteArray(){
		return PacketPlayOutMap.b!=null&&byte[].class.isAssignableFrom(PacketPlayOutMap.b.getType());
	}

	public byte getBAsbyte(){
		try{
			return (byte)PacketPlayOutMap.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setBAsbyte(byte o){
		try{
			PacketPlayOutMap.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsbyte(){
		return PacketPlayOutMap.b!=null&&byte.class.isAssignableFrom(PacketPlayOutMap.b.getType());
	}
	public boolean hasB(){
		return PacketPlayOutMap.b!=null;
	}

	private static Field scale = NMSUtils.getFieldSilent(packetclass, "scale");

	public byte getScale(){
		try{
			return (byte)PacketPlayOutMap.scale.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setScale(byte b){
		try{
			PacketPlayOutMap.scale.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasScale(){
		return PacketPlayOutMap.scale!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public Object getC(){
		try{
			return PacketPlayOutMap.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setC(Object o){
		try{
			PacketPlayOutMap.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutMap.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutMap.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutMap.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutMap.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public int getE(){
		try{
			return (int)PacketPlayOutMap.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(int i){
		try{
			PacketPlayOutMap.e.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutMap.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public int getF(){
		try{
			return (int)PacketPlayOutMap.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setF(int i){
		try{
			PacketPlayOutMap.f.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutMap.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public int getG(){
		try{
			return (int)PacketPlayOutMap.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setG(int i){
		try{
			PacketPlayOutMap.g.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayOutMap.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public byte[] getH(){
		try{
			return (byte[])PacketPlayOutMap.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setH(byte[] b){
		try{
			PacketPlayOutMap.h.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutMap.h!=null;
	}
}
