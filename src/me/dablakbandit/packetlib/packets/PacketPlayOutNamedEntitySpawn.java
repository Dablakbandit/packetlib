package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutNamedEntitySpawn extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutNamedEntitySpawn
	//a : int
	//b : java.util.UUID
	//c : int
	//d : int
	//e : int
	//f : byte
	//g : byte
	//h : int
	//i : net.minecraft.server.v1_8_R1.DataWatcher
	//j : java.util.List

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutNamedEntitySpawn
	//a : int
	//b : java.util.UUID
	//c : int
	//d : int
	//e : int
	//f : byte
	//g : byte
	//h : int
	//i : net.minecraft.server.v1_8_R2.DataWatcher
	//j : java.util.List

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn
	//a : int
	//b : java.util.UUID
	//c : int
	//d : int
	//e : int
	//f : byte
	//g : byte
	//h : int
	//i : net.minecraft.server.v1_8_R3.DataWatcher
	//j : java.util.List

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutNamedEntitySpawn
	//a : int
	//b : net.minecraft.util.com.mojang.authlib.GameProfile
	//c : int
	//d : int
	//e : int
	//f : byte
	//g : byte
	//h : int
	//i : net.minecraft.server.v1_7_R4.DataWatcher
	//j : java.util.List

	protected PacketPlayOutNamedEntitySpawn(Object packet){
		super(packet, PacketType.PacketPlayOutNamedEntitySpawn);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutNamedEntitySpawn");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutNamedEntitySpawn.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutNamedEntitySpawn.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutNamedEntitySpawn.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayOutNamedEntitySpawn.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayOutNamedEntitySpawn.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutNamedEntitySpawn.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutNamedEntitySpawn.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutNamedEntitySpawn.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutNamedEntitySpawn.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutNamedEntitySpawn.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutNamedEntitySpawn.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutNamedEntitySpawn.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public int getE(){
		try{
			return (int)PacketPlayOutNamedEntitySpawn.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(int i){
		try{
			PacketPlayOutNamedEntitySpawn.e.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutNamedEntitySpawn.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public byte getF(){
		try{
			return (byte)PacketPlayOutNamedEntitySpawn.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setF(byte b){
		try{
			PacketPlayOutNamedEntitySpawn.f.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutNamedEntitySpawn.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public byte getG(){
		try{
			return (byte)PacketPlayOutNamedEntitySpawn.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setG(byte b){
		try{
			PacketPlayOutNamedEntitySpawn.g.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayOutNamedEntitySpawn.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public int getH(){
		try{
			return (int)PacketPlayOutNamedEntitySpawn.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setH(int i){
		try{
			PacketPlayOutNamedEntitySpawn.h.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutNamedEntitySpawn.h!=null;
	}

	private static Field i = NMSUtils.getFieldSilent(packetclass, "i");

	public Object getI(){
		try{
			return PacketPlayOutNamedEntitySpawn.i.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setI(Object o){
		try{
			PacketPlayOutNamedEntitySpawn.i.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasI(){
		return PacketPlayOutNamedEntitySpawn.i!=null;
	}

	private static Field j = NMSUtils.getFieldSilent(packetclass, "j");

	public Object getJ(){
		try{
			return PacketPlayOutNamedEntitySpawn.j.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setJ(Object o){
		try{
			PacketPlayOutNamedEntitySpawn.j.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasJ(){
		return PacketPlayOutNamedEntitySpawn.j!=null;
	}
}
