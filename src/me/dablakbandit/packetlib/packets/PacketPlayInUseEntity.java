package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInUseEntity extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInUseEntity
	//a : int
	//action : net.minecraft.server.v1_8_R1.EnumEntityUseAction
	//c : net.minecraft.server.v1_8_R1.Vec3D

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInUseEntity
	//a : int
	//action : net.minecraft.server.v1_8_R2.PacketPlayInUseEntity$EnumEntityUseAction
	//c : net.minecraft.server.v1_8_R2.Vec3D

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInUseEntity
	//a : int
	//action : net.minecraft.server.v1_8_R3.PacketPlayInUseEntity$EnumEntityUseAction
	//c : net.minecraft.server.v1_8_R3.Vec3D

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInUseEntity
	//a : int
	//action : net.minecraft.server.v1_7_R4.EnumEntityUseAction

	protected PacketPlayInUseEntity(Object packet){
		super(packet, PacketType.PacketPlayInUseEntity);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInUseEntity");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayInUseEntity.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayInUseEntity.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInUseEntity.a!=null;
	}

	private static Field action = NMSUtils.getFieldSilent(packetclass, "action");

	public Object getAction(){
		try{
			return PacketPlayInUseEntity.action.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setAction(Object o){
		try{
			PacketPlayInUseEntity.action.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAction(){
		return PacketPlayInUseEntity.action!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public Object getC(){
		try{
			return PacketPlayInUseEntity.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setC(Object o){
		try{
			PacketPlayInUseEntity.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayInUseEntity.c!=null;
	}
}
