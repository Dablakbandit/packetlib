package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketLoginInEncryptionBegin extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketLoginInEncryptionBegin
	//a : [B
	//b : [B

	//Fields
	//net.minecraft.server.v1_8_R2.PacketLoginInEncryptionBegin
	//a : [B
	//b : [B

	//Fields
	//net.minecraft.server.v1_8_R3.PacketLoginInEncryptionBegin
	//a : [B
	//b : [B

	//Fields
	//net.minecraft.server.v1_7_R4.PacketLoginInEncryptionBegin
	//a : [B
	//b : [B

	protected PacketLoginInEncryptionBegin(Object packet){
		super(packet, PacketType.PacketLoginInEncryptionBegin);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketLoginInEncryptionBegin");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public byte[] getA(){
		try{
			return (byte[])PacketLoginInEncryptionBegin.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(byte[] b){
		try{
			PacketLoginInEncryptionBegin.a.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketLoginInEncryptionBegin.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public byte[] getB(){
		try{
			return (byte[])PacketLoginInEncryptionBegin.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(byte[] b){
		try{
			PacketLoginInEncryptionBegin.b.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketLoginInEncryptionBegin.b!=null;
	}
}
