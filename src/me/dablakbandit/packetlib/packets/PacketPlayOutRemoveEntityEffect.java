package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutRemoveEntityEffect extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutRemoveEntityEffect
	//a : int
	//b : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutRemoveEntityEffect
	//a : int
	//b : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutRemoveEntityEffect
	//a : int
	//b : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutRemoveEntityEffect
	//a : int
	//b : int

	protected PacketPlayOutRemoveEntityEffect(Object packet){
		super(packet, PacketType.PacketPlayOutRemoveEntityEffect);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutRemoveEntityEffect");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutRemoveEntityEffect.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutRemoveEntityEffect.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutRemoveEntityEffect.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutRemoveEntityEffect.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutRemoveEntityEffect.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutRemoveEntityEffect.b!=null;
	}
}
