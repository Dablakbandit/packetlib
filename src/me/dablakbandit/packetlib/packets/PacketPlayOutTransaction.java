package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutTransaction extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutTransaction
	//a : int
	//b : short
	//c : boolean

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutTransaction
	//a : int
	//b : short
	//c : boolean

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutTransaction
	//a : int
	//b : short
	//c : boolean

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutTransaction
	//a : int
	//b : short
	//c : boolean

	protected PacketPlayOutTransaction(Object packet){
		super(packet, PacketType.PacketPlayOutTransaction);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutTransaction");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutTransaction.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutTransaction.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutTransaction.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public short getB(){
		try{
			return (short)PacketPlayOutTransaction.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(short s){
		try{
			PacketPlayOutTransaction.b.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutTransaction.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public boolean getC(){
		try{
			return (boolean)PacketPlayOutTransaction.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setC(boolean b){
		try{
			PacketPlayOutTransaction.c.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutTransaction.c!=null;
	}
}
