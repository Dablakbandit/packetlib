package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutEntityMetadata extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutEntityMetadata
	//a : int
	//b : java.util.List

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutEntityMetadata
	//a : int
	//b : java.util.List

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutEntityMetadata
	//a : int
	//b : java.util.List

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutEntityMetadata
	//a : int
	//b : java.util.List

	protected PacketPlayOutEntityMetadata(Object packet){
		super(packet, PacketType.PacketPlayOutEntityMetadata);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutEntityMetadata");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutEntityMetadata.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutEntityMetadata.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutEntityMetadata.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayOutEntityMetadata.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayOutEntityMetadata.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutEntityMetadata.b!=null;
	}
}
