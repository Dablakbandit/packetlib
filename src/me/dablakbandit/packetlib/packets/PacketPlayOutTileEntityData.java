package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutTileEntityData extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutTileEntityData
	//a : net.minecraft.server.v1_8_R1.BlockPosition
	//b : int
	//c : net.minecraft.server.v1_8_R1.NBTTagCompound

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutTileEntityData
	//a : net.minecraft.server.v1_8_R2.BlockPosition
	//b : int
	//c : net.minecraft.server.v1_8_R2.NBTTagCompound

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutTileEntityData
	//a : net.minecraft.server.v1_8_R3.BlockPosition
	//b : int
	//c : net.minecraft.server.v1_8_R3.NBTTagCompound

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutTileEntityData
	//a : int
	//b : int
	//c : int
	//d : int
	//e : net.minecraft.server.v1_7_R4.NBTTagCompound

	protected PacketPlayOutTileEntityData(Object packet){
		super(packet, PacketType.PacketPlayOutTileEntityData);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutTileEntityData");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getAAsInt(){
		try{
			return (int)PacketPlayOutTileEntityData.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setAAsInt(int o){
		try{
			PacketPlayOutTileEntityData.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsInt(){
		return PacketPlayOutTileEntityData.a!=null&&int.class.isAssignableFrom(PacketPlayOutTileEntityData.a.getType());
	}

	public Object getAAsObject(){
		try{
			return (Object)PacketPlayOutTileEntityData.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setAAsObject(Object o){
		try{
			PacketPlayOutTileEntityData.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsObject(){
		return PacketPlayOutTileEntityData.a!=null&&Object.class.isAssignableFrom(PacketPlayOutTileEntityData.a.getType());
	}
	public boolean hasA(){
		return PacketPlayOutTileEntityData.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutTileEntityData.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutTileEntityData.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutTileEntityData.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getCAsInt(){
		try{
			return (int)PacketPlayOutTileEntityData.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setCAsInt(int o){
		try{
			PacketPlayOutTileEntityData.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasCAsInt(){
		return PacketPlayOutTileEntityData.c!=null&&int.class.isAssignableFrom(PacketPlayOutTileEntityData.c.getType());
	}

	public Object getCAsObject(){
		try{
			return (Object)PacketPlayOutTileEntityData.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setCAsObject(Object o){
		try{
			PacketPlayOutTileEntityData.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasCAsObject(){
		return PacketPlayOutTileEntityData.c!=null&&Object.class.isAssignableFrom(PacketPlayOutTileEntityData.c.getType());
	}
	public boolean hasC(){
		return PacketPlayOutTileEntityData.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutTileEntityData.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutTileEntityData.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutTileEntityData.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public Object getE(){
		try{
			return PacketPlayOutTileEntityData.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setE(Object o){
		try{
			PacketPlayOutTileEntityData.e.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutTileEntityData.e!=null;
	}
}
