package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import org.bukkit.entity.Player;

public abstract class Packet{

	protected Object packet;
	private PacketType type;

	public Packet(Object packet, PacketType type){
		this.packet = packet;
		this.type = type;
	}

	public Object getPacket(){
		return packet;
	}

	public PacketType getPacketType(){
		return type;
	}

	public Field getField(String name) throws Exception{
		return NMSUtils.getFieldWithException(packet.getClass(), name);
	}

	public void setField(String name, Object value) throws Exception{
		getField(name).set(packet, value);
	}

	private static Field playerconnection = NMSUtils.getField(NMSUtils.getNMSClass("EntityPlayer"), "playerConnection");
	private static Method sendToPlayer = NMSUtils.getMethod(NMSUtils.getNMSClass("PlayerConnection"), "sendPacket", NMSUtils.getNMSClass("Packet"));

	public void sendTo(Player player){
		try{
			Object ep = NMSUtils.getHandle(player);
			sendToPlayer.invoke(playerconnection.get(ep), packet);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static Packet getPacketClass(Object packet){
		switch(packet.getClass().getSimpleName()){
		case "PacketPlayOutEntityEffect": return new PacketPlayOutEntityEffect(packet);
		case "PacketPlayOutEntity": return new PacketPlayOutEntity(packet);
		case "PacketPlayInCloseWindow": return new PacketPlayInCloseWindow(packet);
		case "PacketPlayInWindowClick": return new PacketPlayInWindowClick(packet);
		case "PacketPlayInKeepAlive": return new PacketPlayInKeepAlive(packet);
		case "PacketPlayInTabComplete": return new PacketPlayInTabComplete(packet);
		case "PacketPlayInUpdateSign": return new PacketPlayInUpdateSign(packet);
		case "PacketPlayInCustomPayload": return new PacketPlayInCustomPayload(packet);
		case "PacketPlayOutScoreboardTeam": return new PacketPlayOutScoreboardTeam(packet);
		case "PacketPlayOutWindowItems": return new PacketPlayOutWindowItems(packet);
		case "PacketPlayOutUpdateAttributes": return new PacketPlayOutUpdateAttributes(packet);
		case "PacketPlayOutWorldParticles": return new PacketPlayOutWorldParticles(packet);
		case "PacketPlayInBlockPlace": return new PacketPlayInBlockPlace(packet);
		case "PacketPlayOutExperience": return new PacketPlayOutExperience(packet);
		case "PacketPlayOutEntityEquipment": return new PacketPlayOutEntityEquipment(packet);
		case "PacketPlayOutPosition": return new PacketPlayOutPosition(packet);
		case "PacketPlayOutBlockBreakAnimation": return new PacketPlayOutBlockBreakAnimation(packet);
		case "PacketPlayOutWorldEvent": return new PacketPlayOutWorldEvent(packet);
		case "PacketPlayOutNamedEntitySpawn": return new PacketPlayOutNamedEntitySpawn(packet);
		case "PacketPlayOutSetSlot": return new PacketPlayOutSetSlot(packet);
		case "PacketPlayOutChat": return new PacketPlayOutChat(packet);
		case "PacketPlayInUseEntity": return new PacketPlayInUseEntity(packet);
		case "PacketPlayOutRelEntityMove": return new PacketPlayOutRelEntityMove(packet);
		case "PacketPlayOutEntityDestroy": return new PacketPlayOutEntityDestroy(packet);
		case "PacketPlayOutKeepAlive": return new PacketPlayOutKeepAlive(packet);
		case "PacketPlayOutRelEntityMoveLook": return new PacketPlayOutRelEntityMoveLook(packet);
		case "PacketPlayOutCustomPayload": return new PacketPlayOutCustomPayload(packet);
		case "PacketPlayOutEntityLook": return new PacketPlayOutEntityLook(packet);
		case "PacketPlayOutScoreboardObjective": return new PacketPlayOutScoreboardObjective(packet);
		case "PacketPlayInEntityAction": return new PacketPlayInEntityAction(packet);
		case "PacketPlayOutEntityMetadata": return new PacketPlayOutEntityMetadata(packet);
		case "PacketPlayOutScoreboardScore": return new PacketPlayOutScoreboardScore(packet);
		case "PacketPlayOutRemoveEntityEffect": return new PacketPlayOutRemoveEntityEffect(packet);
		case "PacketLoginOutSuccess": return new PacketLoginOutSuccess(packet);
		case "PacketPlayOutOpenWindow": return new PacketPlayOutOpenWindow(packet);
		case "PacketPlayOutMultiBlockChange": return new PacketPlayOutMultiBlockChange(packet);
		case "PacketPlayOutSpawnPosition": return new PacketPlayOutSpawnPosition(packet);
		case "PacketPlayOutMapChunk": return new PacketPlayOutMapChunk(packet);
		case "PacketPlayOutOpenSignEditor": return new PacketPlayOutOpenSignEditor(packet);
		case "PacketHandshakingInSetProtocol": return new PacketHandshakingInSetProtocol(packet);
		case "PacketPlayOutBlockAction": return new PacketPlayOutBlockAction(packet);
		case "PacketPlayOutEntityHeadRotation": return new PacketPlayOutEntityHeadRotation(packet);
		case "PacketPlayInSteerVehicle": return new PacketPlayInSteerVehicle(packet);
		case "PacketPlayOutSpawnEntityExperienceOrb": return new PacketPlayOutSpawnEntityExperienceOrb(packet);
		case "PacketPlayOutSpawnEntityWeather": return new PacketPlayOutSpawnEntityWeather(packet);
		case "PacketPlayOutAnimation": return new PacketPlayOutAnimation(packet);
		case "PacketPlayOutStatistic": return new PacketPlayOutStatistic(packet);
		case "PacketPlayOutTabComplete": return new PacketPlayOutTabComplete(packet);
		case "PacketPlayOutTransaction": return new PacketPlayOutTransaction(packet);
		case "PacketPlayOutCloseWindow": return new PacketPlayOutCloseWindow(packet);
		case "PacketPlayOutWindowData": return new PacketPlayOutWindowData(packet);
		case "PacketPlayOutKickDisconnect": return new PacketPlayOutKickDisconnect(packet);
		case "PacketPlayOutEntityStatus": return new PacketPlayOutEntityStatus(packet);
		case "PacketPlayOutExplosion": return new PacketPlayOutExplosion(packet);
		case "PacketPlayOutGameStateChange": return new PacketPlayOutGameStateChange(packet);
		case "PacketPlayOutNamedSoundEffect": return new PacketPlayOutNamedSoundEffect(packet);
		case "PacketPlayOutAbilities": return new PacketPlayOutAbilities(packet);
		case "PacketPlayOutRespawn": return new PacketPlayOutRespawn(packet);
		case "PacketPlayOutHeldItemSlot": return new PacketPlayOutHeldItemSlot(packet);
		case "PacketPlayOutScoreboardDisplayObjective": return new PacketPlayOutScoreboardDisplayObjective(packet);
		case "PacketPlayOutAttachEntity": return new PacketPlayOutAttachEntity(packet);
		case "PacketPlayOutUpdateTime": return new PacketPlayOutUpdateTime(packet);
		case "PacketPlayInClientCommand": return new PacketPlayInClientCommand(packet);
		case "PacketPlayInTransaction": return new PacketPlayInTransaction(packet);
		case "PacketPlayInEnchantItem": return new PacketPlayInEnchantItem(packet);
		case "PacketPlayInFlying": return new PacketPlayInFlying(packet);
		case "PacketPlayInLook": return new PacketPlayInLook(packet);
		case "PacketPlayInAbilities": return new PacketPlayInAbilities(packet);
		case "PacketPlayInHeldItemSlot": return new PacketPlayInHeldItemSlot(packet);
		case "PacketPlayInSetCreativeSlot": return new PacketPlayInSetCreativeSlot(packet);
		case "PacketLoginOutEncryptionBegin": return new PacketLoginOutEncryptionBegin(packet);
		case "PacketLoginOutDisconnect": return new PacketLoginOutDisconnect(packet);
		case "PacketLoginInStart": return new PacketLoginInStart(packet);
		case "PacketLoginInEncryptionBegin": return new PacketLoginInEncryptionBegin(packet);
		case "PacketStatusOutPong": return new PacketStatusOutPong(packet);
		case "PacketStatusOutServerInfo": return new PacketStatusOutServerInfo(packet);
		case "PacketStatusInPing": return new PacketStatusInPing(packet);
		case "PacketStatusInStart": return new PacketStatusInStart(packet);
		case "PacketPlayOutMapChunkBulk": return new PacketPlayOutMapChunkBulk(packet);
		case "PacketPlayOutPlayerInfo": return new PacketPlayOutPlayerInfo(packet);
		case "PacketPlayInChat": return new PacketPlayInChat(packet);
		case "PacketPlayInSettings": return new PacketPlayInSettings(packet);
		case "PacketPlayOutSpawnEntityLiving": return new PacketPlayOutSpawnEntityLiving(packet);
		case "PacketPlayOutCollect": return new PacketPlayOutCollect(packet);
		case "PacketPlayOutSpawnEntity": return new PacketPlayOutSpawnEntity(packet);
		case "PacketPlayOutUpdateSign": return new PacketPlayOutUpdateSign(packet);
		case "PacketPlayOutEntityTeleport": return new PacketPlayOutEntityTeleport(packet);
		case "PacketPlayOutLogin": return new PacketPlayOutLogin(packet);
		case "PacketPlayOutMap": return new PacketPlayOutMap(packet);
		case "PacketPlayInArmAnimation": return new PacketPlayInArmAnimation(packet);
		case "PacketPlayOutBlockChange": return new PacketPlayOutBlockChange(packet);
		case "PacketPlayOutSpawnEntityPainting": return new PacketPlayOutSpawnEntityPainting(packet);
		case "PacketPlayInBlockDig": return new PacketPlayInBlockDig(packet);
		case "PacketPlayInPosition": return new PacketPlayInPosition(packet);
		case "PacketPlayOutTileEntityData": return new PacketPlayOutTileEntityData(packet);
		case "PacketPlayOutBed": return new PacketPlayOutBed(packet);
		case "PacketPlayOutEntityVelocity": return new PacketPlayOutEntityVelocity(packet);
		case "PacketPlayOutUpdateHealth": return new PacketPlayOutUpdateHealth(packet);
		case "PacketPlayInPositionLook": return new PacketPlayInPositionLook(packet);
		case "PacketPlayOutServerDifficulty": return new PacketPlayOutServerDifficulty(packet);
		case "PacketPlayOutUpdateEntityNBT": return new PacketPlayOutUpdateEntityNBT(packet);
		case "PacketPlayOutSetCompression": return new PacketPlayOutSetCompression(packet);
		case "PacketPlayOutCombatEvent": return new PacketPlayOutCombatEvent(packet);
		case "PacketPlayOutResourcePackSend": return new PacketPlayOutResourcePackSend(packet);
		case "PacketPlayOutWorldBorder": return new PacketPlayOutWorldBorder(packet);
		case "PacketPlayOutCamera": return new PacketPlayOutCamera(packet);
		case "PacketPlayOutTitle": return new PacketPlayOutTitle(packet);
		case "PacketPlayOutPlayerListHeaderFooter": return new PacketPlayOutPlayerListHeaderFooter(packet);
		case "PacketPlayInResourcePackStatus": return new PacketPlayInResourcePackStatus(packet);
		case "PacketPlayInSpectate": return new PacketPlayInSpectate(packet);
		case "PacketLoginOutSetCompression": return new PacketLoginOutSetCompression(packet);
		}
		System.out.print("[PacketLib] Couldn't find class for Packet object: " + packet.getClass().getName());
		return new UnknownPacket(packet);
	}

	public static class UnknownPacket extends Packet{

		public UnknownPacket(Object packet){
			super(packet, PacketType.UnknownPacket);
		}

	}

	public enum PacketType{

	PacketPlayOutEntityEffect, PacketPlayOutEntity, PacketPlayInCloseWindow, PacketPlayInWindowClick, PacketPlayInKeepAlive, PacketPlayInTabComplete, PacketPlayInUpdateSign, PacketPlayInCustomPayload, PacketPlayOutScoreboardTeam, PacketPlayOutWindowItems, PacketPlayOutUpdateAttributes, PacketPlayOutWorldParticles, PacketPlayInBlockPlace, PacketPlayOutExperience, PacketPlayOutEntityEquipment, PacketPlayOutPosition, PacketPlayOutBlockBreakAnimation, PacketPlayOutWorldEvent, PacketPlayOutNamedEntitySpawn, PacketPlayOutSetSlot, PacketPlayOutChat, PacketPlayInUseEntity, PacketPlayOutRelEntityMove, PacketPlayOutEntityDestroy, PacketPlayOutKeepAlive, PacketPlayOutRelEntityMoveLook, PacketPlayOutCustomPayload, PacketPlayOutEntityLook, PacketPlayOutScoreboardObjective, PacketPlayInEntityAction, PacketPlayOutEntityMetadata, PacketPlayOutScoreboardScore, PacketPlayOutRemoveEntityEffect, PacketLoginOutSuccess, PacketPlayOutOpenWindow, PacketPlayOutMultiBlockChange, PacketPlayOutSpawnPosition, PacketPlayOutMapChunk, PacketPlayOutOpenSignEditor, PacketHandshakingInSetProtocol, PacketPlayOutBlockAction, PacketPlayOutEntityHeadRotation, PacketPlayInSteerVehicle, PacketPlayOutSpawnEntityExperienceOrb, PacketPlayOutSpawnEntityWeather, PacketPlayOutAnimation, PacketPlayOutStatistic, PacketPlayOutTabComplete, PacketPlayOutTransaction, PacketPlayOutCloseWindow, PacketPlayOutWindowData, PacketPlayOutKickDisconnect, PacketPlayOutEntityStatus, PacketPlayOutExplosion, PacketPlayOutGameStateChange, PacketPlayOutNamedSoundEffect, PacketPlayOutAbilities, PacketPlayOutRespawn, PacketPlayOutHeldItemSlot, PacketPlayOutScoreboardDisplayObjective, PacketPlayOutAttachEntity, PacketPlayOutUpdateTime, PacketPlayInClientCommand, PacketPlayInTransaction, PacketPlayInEnchantItem, PacketPlayInFlying, PacketPlayInLook, PacketPlayInAbilities, PacketPlayInHeldItemSlot, PacketPlayInSetCreativeSlot, PacketLoginOutEncryptionBegin, PacketLoginOutDisconnect, PacketLoginInStart, PacketLoginInEncryptionBegin, PacketStatusOutPong, PacketStatusOutServerInfo, PacketStatusInPing, PacketStatusInStart, PacketPlayOutMapChunkBulk, PacketPlayOutPlayerInfo, PacketPlayInChat, PacketPlayInSettings, PacketPlayOutSpawnEntityLiving, PacketPlayOutCollect, PacketPlayOutSpawnEntity, PacketPlayOutUpdateSign, PacketPlayOutEntityTeleport, PacketPlayOutLogin, PacketPlayOutMap, PacketPlayInArmAnimation, PacketPlayOutBlockChange, PacketPlayOutSpawnEntityPainting, PacketPlayInBlockDig, PacketPlayInPosition, PacketPlayOutTileEntityData, PacketPlayOutBed, PacketPlayOutEntityVelocity, PacketPlayOutUpdateHealth, PacketPlayInPositionLook, PacketPlayOutServerDifficulty, PacketPlayOutUpdateEntityNBT, PacketPlayOutSetCompression, PacketPlayOutCombatEvent, PacketPlayOutResourcePackSend, PacketPlayOutWorldBorder, PacketPlayOutCamera, PacketPlayOutTitle, PacketPlayOutPlayerListHeaderFooter, PacketPlayInResourcePackStatus, PacketPlayInSpectate, PacketLoginOutSetCompression, UnknownPacket;

	}
}
