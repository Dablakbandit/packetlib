package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutUpdateHealth extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutUpdateHealth
	//a : float
	//b : int
	//c : float

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutUpdateHealth
	//a : float
	//b : int
	//c : float

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutUpdateHealth
	//a : float
	//b : int
	//c : float

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutUpdateHealth
	//a : float
	//b : int
	//c : float

	protected PacketPlayOutUpdateHealth(Object packet){
		super(packet, PacketType.PacketPlayOutUpdateHealth);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutUpdateHealth");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public float getA(){
		try{
			return (float)PacketPlayOutUpdateHealth.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setA(float f){
		try{
			PacketPlayOutUpdateHealth.a.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutUpdateHealth.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutUpdateHealth.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutUpdateHealth.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutUpdateHealth.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public float getC(){
		try{
			return (float)PacketPlayOutUpdateHealth.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setC(float f){
		try{
			PacketPlayOutUpdateHealth.c.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutUpdateHealth.c!=null;
	}
}
