package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInHeldItemSlot extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInHeldItemSlot
	//itemInHandIndex : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInHeldItemSlot
	//itemInHandIndex : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInHeldItemSlot
	//itemInHandIndex : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInHeldItemSlot
	//itemInHandIndex : int

	protected PacketPlayInHeldItemSlot(Object packet){
		super(packet, PacketType.PacketPlayInHeldItemSlot);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInHeldItemSlot");

	private static Field itemInHandIndex = NMSUtils.getFieldSilent(packetclass, "itemInHandIndex");

	public int getItemInHandIndex(){
		try{
			return (int)PacketPlayInHeldItemSlot.itemInHandIndex.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setItemInHandIndex(int i){
		try{
			PacketPlayInHeldItemSlot.itemInHandIndex.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasItemInHandIndex(){
		return PacketPlayInHeldItemSlot.itemInHandIndex!=null;
	}
}
