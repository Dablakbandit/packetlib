package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutScoreboardDisplayObjective extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutScoreboardDisplayObjective
	//a : int
	//b : java.lang.String

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutScoreboardDisplayObjective
	//a : int
	//b : java.lang.String

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutScoreboardDisplayObjective
	//a : int
	//b : java.lang.String

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutScoreboardDisplayObjective
	//a : int
	//b : java.lang.String

	protected PacketPlayOutScoreboardDisplayObjective(Object packet){
		super(packet, PacketType.PacketPlayOutScoreboardDisplayObjective);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutScoreboardDisplayObjective");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutScoreboardDisplayObjective.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutScoreboardDisplayObjective.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutScoreboardDisplayObjective.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public String getB(){
		try{
			return (String)PacketPlayOutScoreboardDisplayObjective.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(String s){
		try{
			PacketPlayOutScoreboardDisplayObjective.b.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutScoreboardDisplayObjective.b!=null;
	}
}
