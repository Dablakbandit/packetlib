package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutScoreboardScore extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutScoreboardScore
	//a : java.lang.String
	//b : java.lang.String
	//c : int
	//d : net.minecraft.server.v1_8_R1.EnumScoreboardAction

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutScoreboardScore
	//a : java.lang.String
	//b : java.lang.String
	//c : int
	//d : net.minecraft.server.v1_8_R2.PacketPlayOutScoreboardScore$EnumScoreboardAction

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutScoreboardScore
	//a : java.lang.String
	//b : java.lang.String
	//c : int
	//d : net.minecraft.server.v1_8_R3.PacketPlayOutScoreboardScore$EnumScoreboardAction

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutScoreboardScore
	//a : java.lang.String
	//b : java.lang.String
	//c : int
	//d : int

	protected PacketPlayOutScoreboardScore(Object packet){
		super(packet, PacketType.PacketPlayOutScoreboardScore);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutScoreboardScore");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String getA(){
		try{
			return (String)PacketPlayOutScoreboardScore.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(String s){
		try{
			PacketPlayOutScoreboardScore.a.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutScoreboardScore.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public String getB(){
		try{
			return (String)PacketPlayOutScoreboardScore.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(String s){
		try{
			PacketPlayOutScoreboardScore.b.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutScoreboardScore.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutScoreboardScore.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutScoreboardScore.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutScoreboardScore.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getDAsInt(){
		try{
			return (int)PacketPlayOutScoreboardScore.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setDAsInt(int o){
		try{
			PacketPlayOutScoreboardScore.d.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasDAsInt(){
		return PacketPlayOutScoreboardScore.d!=null&&int.class.isAssignableFrom(PacketPlayOutScoreboardScore.d.getType());
	}

	public Object getDAsObject(){
		try{
			return (Object)PacketPlayOutScoreboardScore.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setDAsObject(Object o){
		try{
			PacketPlayOutScoreboardScore.d.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasDAsObject(){
		return PacketPlayOutScoreboardScore.d!=null&&Object.class.isAssignableFrom(PacketPlayOutScoreboardScore.d.getType());
	}
	public boolean hasD(){
		return PacketPlayOutScoreboardScore.d!=null;
	}
}
