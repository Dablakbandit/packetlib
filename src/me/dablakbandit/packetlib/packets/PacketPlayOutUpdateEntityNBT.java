package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutUpdateEntityNBT extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutUpdateEntityNBT
	//a : int
	//b : net.minecraft.server.v1_8_R1.NBTTagCompound

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutUpdateEntityNBT
	//a : int
	//b : net.minecraft.server.v1_8_R2.NBTTagCompound

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutUpdateEntityNBT
	//a : int
	//b : net.minecraft.server.v1_8_R3.NBTTagCompound

	protected PacketPlayOutUpdateEntityNBT(Object packet){
		super(packet, PacketType.PacketPlayOutUpdateEntityNBT);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutUpdateEntityNBT");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutUpdateEntityNBT.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutUpdateEntityNBT.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutUpdateEntityNBT.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayOutUpdateEntityNBT.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayOutUpdateEntityNBT.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutUpdateEntityNBT.b!=null;
	}
}
