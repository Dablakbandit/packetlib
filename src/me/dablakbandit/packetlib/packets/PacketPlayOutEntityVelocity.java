package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutEntityVelocity extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutEntityVelocity
	//a : int
	//b : int
	//c : int
	//d : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutEntityVelocity
	//a : int
	//b : int
	//c : int
	//d : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutEntityVelocity
	//a : int
	//b : int
	//c : int
	//d : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutEntityVelocity
	//a : int
	//b : int
	//c : int
	//d : int

	protected PacketPlayOutEntityVelocity(Object packet){
		super(packet, PacketType.PacketPlayOutEntityVelocity);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutEntityVelocity");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutEntityVelocity.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutEntityVelocity.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutEntityVelocity.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutEntityVelocity.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutEntityVelocity.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutEntityVelocity.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutEntityVelocity.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutEntityVelocity.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutEntityVelocity.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutEntityVelocity.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutEntityVelocity.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutEntityVelocity.d!=null;
	}
}
