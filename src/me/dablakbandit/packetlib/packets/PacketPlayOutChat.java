package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutChat extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutChat
	//a : net.minecraft.server.v1_8_R1.IChatBaseComponent
	//b : byte

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutChat
	//a : net.minecraft.server.v1_8_R2.IChatBaseComponent
	//components : [Lnet.md_5.bungee.api.chat.BaseComponent;
	//b : byte

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutChat
	//a : net.minecraft.server.v1_8_R3.IChatBaseComponent
	//components : [Lnet.md_5.bungee.api.chat.BaseComponent;
	//b : byte

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutChat
	//a : net.minecraft.server.v1_7_R4.IChatBaseComponent
	//components : [Lnet.md_5.bungee.api.chat.BaseComponent;
	//b : boolean
	//pos : int

	protected PacketPlayOutChat(Object packet){
		super(packet, PacketType.PacketPlayOutChat);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutChat");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketPlayOutChat.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketPlayOutChat.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutChat.a!=null;
	}

	private static Field components = NMSUtils.getFieldSilent(packetclass, "components");

	public Object getComponents(){
		try{
			return PacketPlayOutChat.components.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setComponents(Object o){
		try{
			PacketPlayOutChat.components.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasComponents(){
		return PacketPlayOutChat.components!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public boolean getBAsBoolean(){
		try{
			return (boolean)PacketPlayOutChat.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setBAsBoolean(boolean o){
		try{
			PacketPlayOutChat.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsBoolean(){
		return PacketPlayOutChat.b!=null&&boolean.class.isAssignableFrom(PacketPlayOutChat.b.getType());
	}

	public byte getBAsbyte(){
		try{
			return (byte)PacketPlayOutChat.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setBAsbyte(byte o){
		try{
			PacketPlayOutChat.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsbyte(){
		return PacketPlayOutChat.b!=null&&byte.class.isAssignableFrom(PacketPlayOutChat.b.getType());
	}
	public boolean hasB(){
		return PacketPlayOutChat.b!=null;
	}

	private static Field pos = NMSUtils.getFieldSilent(packetclass, "pos");

	public int getPos(){
		try{
			return (int)PacketPlayOutChat.pos.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setPos(int i){
		try{
			PacketPlayOutChat.pos.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasPos(){
		return PacketPlayOutChat.pos!=null;
	}
}
