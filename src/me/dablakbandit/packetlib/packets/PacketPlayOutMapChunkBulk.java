package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutMapChunkBulk extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutMapChunkBulk
	//a : [I
	//b : [I
	//c : [Lnet.minecraft.server.v1_8_R1.ChunkMap;
	//d : boolean
	//world : net.minecraft.server.v1_8_R1.World

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutMapChunkBulk
	//a : [I
	//b : [I
	//c : [Lnet.minecraft.server.v1_8_R2.PacketPlayOutMapChunk$ChunkMap;
	//d : boolean
	//world : net.minecraft.server.v1_8_R2.World

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutMapChunkBulk
	//a : [I
	//b : [I
	//c : [Lnet.minecraft.server.v1_8_R3.PacketPlayOutMapChunk$ChunkMap;
	//d : boolean
	//world : net.minecraft.server.v1_8_R3.World

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutMapChunkBulk
	//a : [I
	//b : [I
	//c : [I
	//d : [I
	//buffer : [B
	//inflatedBuffers : [[B
	//size : int
	//h : boolean
	//buildBuffer : [B
	//localDeflater : java.lang.ThreadLocal
	//world : net.minecraft.server.v1_7_R4.World

	protected PacketPlayOutMapChunkBulk(Object packet){
		super(packet, PacketType.PacketPlayOutMapChunkBulk);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutMapChunkBulk");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int[] getA(){
		try{
			return (int[])PacketPlayOutMapChunkBulk.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(int[] i){
		try{
			PacketPlayOutMapChunkBulk.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutMapChunkBulk.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int[] getB(){
		try{
			return (int[])PacketPlayOutMapChunkBulk.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(int[] i){
		try{
			PacketPlayOutMapChunkBulk.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutMapChunkBulk.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getCAsInt(){
		try{
			return (int)PacketPlayOutMapChunkBulk.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setCAsInt(int o){
		try{
			PacketPlayOutMapChunkBulk.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasCAsInt(){
		return PacketPlayOutMapChunkBulk.c!=null&&int.class.isAssignableFrom(PacketPlayOutMapChunkBulk.c.getType());
	}

	public Object getCAsObject(){
		try{
			return (Object)PacketPlayOutMapChunkBulk.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setCAsObject(Object o){
		try{
			PacketPlayOutMapChunkBulk.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasCAsObject(){
		return PacketPlayOutMapChunkBulk.c!=null&&Object.class.isAssignableFrom(PacketPlayOutMapChunkBulk.c.getType());
	}
	public boolean hasC(){
		return PacketPlayOutMapChunkBulk.c!=null;
	}

	private static Field buffer = NMSUtils.getFieldSilent(packetclass, "buffer");

	public byte[] getBuffer(){
		try{
			return (byte[])PacketPlayOutMapChunkBulk.buffer.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setBuffer(byte[] b){
		try{
			PacketPlayOutMapChunkBulk.buffer.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBuffer(){
		return PacketPlayOutMapChunkBulk.buffer!=null;
	}

	private static Field inflatedBuffers = NMSUtils.getFieldSilent(packetclass, "inflatedBuffers");

	public byte[][] getInflatedBuffers(){
		try{
			return (byte[][])PacketPlayOutMapChunkBulk.inflatedBuffers.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setInflatedBuffers(byte[][] b){
		try{
			PacketPlayOutMapChunkBulk.inflatedBuffers.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasInflatedBuffers(){
		return PacketPlayOutMapChunkBulk.inflatedBuffers!=null;
	}

	private static Field size = NMSUtils.getFieldSilent(packetclass, "size");

	public int getSize(){
		try{
			return (int)PacketPlayOutMapChunkBulk.size.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setSize(int i){
		try{
			PacketPlayOutMapChunkBulk.size.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasSize(){
		return PacketPlayOutMapChunkBulk.size!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public boolean getH(){
		try{
			return (boolean)PacketPlayOutMapChunkBulk.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setH(boolean b){
		try{
			PacketPlayOutMapChunkBulk.h.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutMapChunkBulk.h!=null;
	}

	private static Field buildBuffer = NMSUtils.getFieldSilent(packetclass, "buildBuffer");

	public byte[][] getBuildBuffer(){
		try{
			return (byte[][])PacketPlayOutMapChunkBulk.buildBuffer.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setBuildBuffer(byte[][] b){
		try{
			PacketPlayOutMapChunkBulk.buildBuffer.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBuildBuffer(){
		return PacketPlayOutMapChunkBulk.buildBuffer!=null;
	}

	private static Field localDeflater = NMSUtils.getFieldSilent(packetclass, "localDeflater");

	public Object getLocalDeflater(){
		try{
			return PacketPlayOutMapChunkBulk.localDeflater.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setLocalDeflater(Object o){
		try{
			PacketPlayOutMapChunkBulk.localDeflater.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasLocalDeflater(){
		return PacketPlayOutMapChunkBulk.localDeflater!=null;
	}

	private static Field world = NMSUtils.getFieldSilent(packetclass, "world");

	public Object getWorld(){
		try{
			return PacketPlayOutMapChunkBulk.world.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setWorld(Object o){
		try{
			PacketPlayOutMapChunkBulk.world.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasWorld(){
		return PacketPlayOutMapChunkBulk.world!=null;
	}
}
