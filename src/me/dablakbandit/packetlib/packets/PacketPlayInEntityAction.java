package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInEntityAction extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInEntityAction
	//a : int
	//animation : net.minecraft.server.v1_8_R1.EnumPlayerAction
	//c : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInEntityAction
	//a : int
	//animation : net.minecraft.server.v1_8_R2.PacketPlayInEntityAction$EnumPlayerAction
	//c : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInEntityAction
	//a : int
	//animation : net.minecraft.server.v1_8_R3.PacketPlayInEntityAction$EnumPlayerAction
	//c : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInEntityAction
	//a : int
	//animation : int
	//c : int

	protected PacketPlayInEntityAction(Object packet){
		super(packet, PacketType.PacketPlayInEntityAction);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInEntityAction");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayInEntityAction.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayInEntityAction.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInEntityAction.a!=null;
	}

	private static Field animation = NMSUtils.getFieldSilent(packetclass, "animation");

	public int getAnimationAsInt(){
		try{
			return (int)PacketPlayInEntityAction.animation.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setAnimationAsInt(int o){
		try{
			PacketPlayInEntityAction.animation.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAnimationAsInt(){
		return PacketPlayInEntityAction.animation!=null&&int.class.isAssignableFrom(PacketPlayInEntityAction.animation.getType());
	}

	public Object getAnimationAsObject(){
		try{
			return (Object)PacketPlayInEntityAction.animation.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setAnimationAsObject(Object o){
		try{
			PacketPlayInEntityAction.animation.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAnimationAsObject(){
		return PacketPlayInEntityAction.animation!=null&&Object.class.isAssignableFrom(PacketPlayInEntityAction.animation.getType());
	}
	public boolean hasAnimation(){
		return PacketPlayInEntityAction.animation!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayInEntityAction.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayInEntityAction.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayInEntityAction.c!=null;
	}
}
