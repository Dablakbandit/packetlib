package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutCloseWindow extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutCloseWindow
	//a : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutCloseWindow
	//a : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutCloseWindow
	//a : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutCloseWindow
	//a : int

	protected PacketPlayOutCloseWindow(Object packet){
		super(packet, PacketType.PacketPlayOutCloseWindow);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutCloseWindow");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutCloseWindow.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutCloseWindow.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutCloseWindow.a!=null;
	}
}
