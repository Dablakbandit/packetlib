package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketStatusOutPong extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketStatusOutPong
	//a : long

	//Fields
	//net.minecraft.server.v1_8_R2.PacketStatusOutPong
	//a : long

	//Fields
	//net.minecraft.server.v1_8_R3.PacketStatusOutPong
	//a : long

	//Fields
	//net.minecraft.server.v1_7_R4.PacketStatusOutPong
	//a : long

	protected PacketStatusOutPong(Object packet){
		super(packet, PacketType.PacketStatusOutPong);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketStatusOutPong");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public long getA(){
		try{
			return (long)PacketStatusOutPong.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(long l){
		try{
			PacketStatusOutPong.a.set(packet, l);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketStatusOutPong.a!=null;
	}
}
