package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInSettings extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInSettings
	//a : java.lang.String
	//b : int
	//c : net.minecraft.server.v1_8_R1.EnumChatVisibility
	//d : boolean
	//e : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInSettings
	//a : java.lang.String
	//b : int
	//c : net.minecraft.server.v1_8_R2.EntityHuman$EnumChatVisibility
	//d : boolean
	//e : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInSettings
	//a : java.lang.String
	//b : int
	//c : net.minecraft.server.v1_8_R3.EntityHuman$EnumChatVisibility
	//d : boolean
	//e : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInSettings
	//a : java.lang.String
	//b : int
	//c : net.minecraft.server.v1_7_R4.EnumChatVisibility
	//d : boolean
	//e : net.minecraft.server.v1_7_R4.EnumDifficulty
	//f : boolean
	//version : int
	//flags : int

	protected PacketPlayInSettings(Object packet){
		super(packet, PacketType.PacketPlayInSettings);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInSettings");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String getA(){
		try{
			return (String)PacketPlayInSettings.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(String s){
		try{
			PacketPlayInSettings.a.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInSettings.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayInSettings.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayInSettings.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayInSettings.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public Object getC(){
		try{
			return PacketPlayInSettings.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setC(Object o){
		try{
			PacketPlayInSettings.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayInSettings.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public boolean getD(){
		try{
			return (boolean)PacketPlayInSettings.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setD(boolean b){
		try{
			PacketPlayInSettings.d.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayInSettings.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public Object getEAsObject(){
		try{
			return (Object)PacketPlayInSettings.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setEAsObject(Object o){
		try{
			PacketPlayInSettings.e.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasEAsObject(){
		return PacketPlayInSettings.e!=null&&Object.class.isAssignableFrom(PacketPlayInSettings.e.getType());
	}

	public int getEAsInt(){
		try{
			return (int)PacketPlayInSettings.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setEAsInt(int o){
		try{
			PacketPlayInSettings.e.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasEAsInt(){
		return PacketPlayInSettings.e!=null&&int.class.isAssignableFrom(PacketPlayInSettings.e.getType());
	}
	public boolean hasE(){
		return PacketPlayInSettings.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public boolean getF(){
		try{
			return (boolean)PacketPlayInSettings.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setF(boolean b){
		try{
			PacketPlayInSettings.f.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayInSettings.f!=null;
	}

	private static Field version = NMSUtils.getFieldSilent(packetclass, "version");

	public int getVersion(){
		try{
			return (int)PacketPlayInSettings.version.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setVersion(int i){
		try{
			PacketPlayInSettings.version.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasVersion(){
		return PacketPlayInSettings.version!=null;
	}

	private static Field flags = NMSUtils.getFieldSilent(packetclass, "flags");

	public int getFlags(){
		try{
			return (int)PacketPlayInSettings.flags.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setFlags(int i){
		try{
			PacketPlayInSettings.flags.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasFlags(){
		return PacketPlayInSettings.flags!=null;
	}
}
