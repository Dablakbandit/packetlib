package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutSpawnEntityPainting extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutSpawnEntityPainting
	//a : int
	//b : net.minecraft.server.v1_8_R1.BlockPosition
	//c : net.minecraft.server.v1_8_R1.EnumDirection
	//d : java.lang.String

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutSpawnEntityPainting
	//a : int
	//b : net.minecraft.server.v1_8_R2.BlockPosition
	//c : net.minecraft.server.v1_8_R2.EnumDirection
	//d : java.lang.String

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityPainting
	//a : int
	//b : net.minecraft.server.v1_8_R3.BlockPosition
	//c : net.minecraft.server.v1_8_R3.EnumDirection
	//d : java.lang.String

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutSpawnEntityPainting
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int
	//f : java.lang.String

	protected PacketPlayOutSpawnEntityPainting(Object packet){
		super(packet, PacketType.PacketPlayOutSpawnEntityPainting);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutSpawnEntityPainting");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutSpawnEntityPainting.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutSpawnEntityPainting.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutSpawnEntityPainting.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getBAsInt(){
		try{
			return (int)PacketPlayOutSpawnEntityPainting.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setBAsInt(int o){
		try{
			PacketPlayOutSpawnEntityPainting.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsInt(){
		return PacketPlayOutSpawnEntityPainting.b!=null&&int.class.isAssignableFrom(PacketPlayOutSpawnEntityPainting.b.getType());
	}

	public Object getBAsObject(){
		try{
			return (Object)PacketPlayOutSpawnEntityPainting.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setBAsObject(Object o){
		try{
			PacketPlayOutSpawnEntityPainting.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsObject(){
		return PacketPlayOutSpawnEntityPainting.b!=null&&Object.class.isAssignableFrom(PacketPlayOutSpawnEntityPainting.b.getType());
	}
	public boolean hasB(){
		return PacketPlayOutSpawnEntityPainting.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getCAsInt(){
		try{
			return (int)PacketPlayOutSpawnEntityPainting.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setCAsInt(int o){
		try{
			PacketPlayOutSpawnEntityPainting.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasCAsInt(){
		return PacketPlayOutSpawnEntityPainting.c!=null&&int.class.isAssignableFrom(PacketPlayOutSpawnEntityPainting.c.getType());
	}

	public Object getCAsObject(){
		try{
			return (Object)PacketPlayOutSpawnEntityPainting.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setCAsObject(Object o){
		try{
			PacketPlayOutSpawnEntityPainting.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasCAsObject(){
		return PacketPlayOutSpawnEntityPainting.c!=null&&Object.class.isAssignableFrom(PacketPlayOutSpawnEntityPainting.c.getType());
	}
	public boolean hasC(){
		return PacketPlayOutSpawnEntityPainting.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getDAsInt(){
		try{
			return (int)PacketPlayOutSpawnEntityPainting.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setDAsInt(int o){
		try{
			PacketPlayOutSpawnEntityPainting.d.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasDAsInt(){
		return PacketPlayOutSpawnEntityPainting.d!=null&&int.class.isAssignableFrom(PacketPlayOutSpawnEntityPainting.d.getType());
	}

	public String getDAsString(){
		try{
			return (String)PacketPlayOutSpawnEntityPainting.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setDAsString(String o){
		try{
			PacketPlayOutSpawnEntityPainting.d.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasDAsString(){
		return PacketPlayOutSpawnEntityPainting.d!=null&&String.class.isAssignableFrom(PacketPlayOutSpawnEntityPainting.d.getType());
	}
	public boolean hasD(){
		return PacketPlayOutSpawnEntityPainting.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public int getE(){
		try{
			return (int)PacketPlayOutSpawnEntityPainting.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(int i){
		try{
			PacketPlayOutSpawnEntityPainting.e.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutSpawnEntityPainting.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public String getF(){
		try{
			return (String)PacketPlayOutSpawnEntityPainting.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setF(String s){
		try{
			PacketPlayOutSpawnEntityPainting.f.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutSpawnEntityPainting.f!=null;
	}
}
