package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketHandshakingInSetProtocol extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketHandshakingInSetProtocol
	//a : int
	//b : java.lang.String
	//c : int
	//d : net.minecraft.server.v1_8_R1.EnumProtocol

	//Fields
	//net.minecraft.server.v1_8_R2.PacketHandshakingInSetProtocol
	//a : int
	//b : java.lang.String
	//c : int
	//d : net.minecraft.server.v1_8_R2.EnumProtocol

	//Fields
	//net.minecraft.server.v1_8_R3.PacketHandshakingInSetProtocol
	//a : int
	//b : java.lang.String
	//c : int
	//d : net.minecraft.server.v1_8_R3.EnumProtocol

	//Fields
	//net.minecraft.server.v1_7_R4.PacketHandshakingInSetProtocol
	//a : int
	//b : java.lang.String
	//c : int
	//d : net.minecraft.server.v1_7_R4.EnumProtocol

	protected PacketHandshakingInSetProtocol(Object packet){
		super(packet, PacketType.PacketHandshakingInSetProtocol);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketHandshakingInSetProtocol");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketHandshakingInSetProtocol.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketHandshakingInSetProtocol.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketHandshakingInSetProtocol.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public String getB(){
		try{
			return (String)PacketHandshakingInSetProtocol.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(String s){
		try{
			PacketHandshakingInSetProtocol.b.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketHandshakingInSetProtocol.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketHandshakingInSetProtocol.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketHandshakingInSetProtocol.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketHandshakingInSetProtocol.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public Object getD(){
		try{
			return PacketHandshakingInSetProtocol.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setD(Object o){
		try{
			PacketHandshakingInSetProtocol.d.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketHandshakingInSetProtocol.d!=null;
	}
}
