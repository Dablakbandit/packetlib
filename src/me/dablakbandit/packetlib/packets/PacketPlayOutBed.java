package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutBed extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutBed
	//a : int
	//b : net.minecraft.server.v1_8_R1.BlockPosition

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutBed
	//a : int
	//b : net.minecraft.server.v1_8_R2.BlockPosition

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutBed
	//a : int
	//b : net.minecraft.server.v1_8_R3.BlockPosition

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutBed
	//a : int
	//b : int
	//c : int
	//d : int

	protected PacketPlayOutBed(Object packet){
		super(packet, PacketType.PacketPlayOutBed);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutBed");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutBed.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutBed.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutBed.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getBAsInt(){
		try{
			return (int)PacketPlayOutBed.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setBAsInt(int o){
		try{
			PacketPlayOutBed.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsInt(){
		return PacketPlayOutBed.b!=null&&int.class.isAssignableFrom(PacketPlayOutBed.b.getType());
	}

	public Object getBAsObject(){
		try{
			return (Object)PacketPlayOutBed.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setBAsObject(Object o){
		try{
			PacketPlayOutBed.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsObject(){
		return PacketPlayOutBed.b!=null&&Object.class.isAssignableFrom(PacketPlayOutBed.b.getType());
	}
	public boolean hasB(){
		return PacketPlayOutBed.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutBed.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutBed.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutBed.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutBed.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutBed.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutBed.d!=null;
	}
}
