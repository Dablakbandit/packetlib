package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutTabComplete extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutTabComplete
	//a : [Ljava.lang.String;

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutTabComplete
	//a : [Ljava.lang.String;

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutTabComplete
	//a : [Ljava.lang.String;

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutTabComplete
	//a : [Ljava.lang.String;

	protected PacketPlayOutTabComplete(Object packet){
		super(packet, PacketType.PacketPlayOutTabComplete);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutTabComplete");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String[] getA(){
		try{
			return (String[])PacketPlayOutTabComplete.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(String[] s){
		try{
			PacketPlayOutTabComplete.a.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutTabComplete.a!=null;
	}
}
