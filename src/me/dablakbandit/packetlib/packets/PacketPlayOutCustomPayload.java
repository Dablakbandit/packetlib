package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutCustomPayload extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutCustomPayload
	//a : java.lang.String
	//b : net.minecraft.server.v1_8_R1.PacketDataSerializer

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutCustomPayload
	//a : java.lang.String
	//b : net.minecraft.server.v1_8_R2.PacketDataSerializer

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutCustomPayload
	//a : java.lang.String
	//b : net.minecraft.server.v1_8_R3.PacketDataSerializer

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutCustomPayload
	//tag : java.lang.String
	//data : [B

	protected PacketPlayOutCustomPayload(Object packet){
		super(packet, PacketType.PacketPlayOutCustomPayload);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutCustomPayload");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String getA(){
		try{
			return (String)PacketPlayOutCustomPayload.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(String s){
		try{
			PacketPlayOutCustomPayload.a.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutCustomPayload.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayOutCustomPayload.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayOutCustomPayload.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutCustomPayload.b!=null;
	}

	private static Field data = NMSUtils.getFieldSilent(packetclass, "data");

	public byte[] getData(){
		try{
			return (byte[])PacketPlayOutCustomPayload.data.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setData(byte[] b){
		try{
			PacketPlayOutCustomPayload.data.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasData(){
		return PacketPlayOutCustomPayload.data!=null;
	}
}
