package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutGameStateChange extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutGameStateChange
	//a : [Ljava.lang.String;
	//b : int
	//c : float

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutGameStateChange
	//a : [Ljava.lang.String;
	//b : int
	//c : float

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutGameStateChange
	//a : [Ljava.lang.String;
	//b : int
	//c : float

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutGameStateChange
	//a : [Ljava.lang.String;
	//b : int
	//c : float

	protected PacketPlayOutGameStateChange(Object packet){
		super(packet, PacketType.PacketPlayOutGameStateChange);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutGameStateChange");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String[] getA(){
		try{
			return (String[])PacketPlayOutGameStateChange.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(String[] s){
		try{
			PacketPlayOutGameStateChange.a.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutGameStateChange.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutGameStateChange.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutGameStateChange.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutGameStateChange.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public float getC(){
		try{
			return (float)PacketPlayOutGameStateChange.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setC(float f){
		try{
			PacketPlayOutGameStateChange.c.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutGameStateChange.c!=null;
	}
}
