package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutCollect extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutCollect
	//a : int
	//b : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutCollect
	//a : int
	//b : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutCollect
	//a : int
	//b : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutCollect
	//a : int
	//b : int

	protected PacketPlayOutCollect(Object packet){
		super(packet, PacketType.PacketPlayOutCollect);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutCollect");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutCollect.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutCollect.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutCollect.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutCollect.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutCollect.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutCollect.b!=null;
	}
}
