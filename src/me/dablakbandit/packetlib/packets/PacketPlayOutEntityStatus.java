package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutEntityStatus extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutEntityStatus
	//a : int
	//b : byte

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutEntityStatus
	//a : int
	//b : byte

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutEntityStatus
	//a : int
	//b : byte

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutEntityStatus
	//a : int
	//b : byte

	protected PacketPlayOutEntityStatus(Object packet){
		super(packet, PacketType.PacketPlayOutEntityStatus);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutEntityStatus");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutEntityStatus.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutEntityStatus.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutEntityStatus.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public byte getB(){
		try{
			return (byte)PacketPlayOutEntityStatus.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(byte b){
		try{
			PacketPlayOutEntityStatus.b.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutEntityStatus.b!=null;
	}
}
