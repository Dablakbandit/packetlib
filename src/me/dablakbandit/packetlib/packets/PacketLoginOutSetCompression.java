package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketLoginOutSetCompression extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketLoginOutSetCompression
	//a : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketLoginOutSetCompression
	//a : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketLoginOutSetCompression
	//a : int

	protected PacketLoginOutSetCompression(Object packet){
		super(packet, PacketType.PacketLoginOutSetCompression);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketLoginOutSetCompression");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketLoginOutSetCompression.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketLoginOutSetCompression.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketLoginOutSetCompression.a!=null;
	}
}
