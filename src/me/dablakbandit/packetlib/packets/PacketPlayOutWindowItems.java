package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutWindowItems extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutWindowItems
	//a : int
	//b : [Lnet.minecraft.server.v1_8_R1.ItemStack;

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutWindowItems
	//a : int
	//b : [Lnet.minecraft.server.v1_8_R2.ItemStack;

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutWindowItems
	//a : int
	//b : [Lnet.minecraft.server.v1_8_R3.ItemStack;

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutWindowItems
	//a : int
	//b : [Lnet.minecraft.server.v1_7_R4.ItemStack;

	protected PacketPlayOutWindowItems(Object packet){
		super(packet, PacketType.PacketPlayOutWindowItems);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutWindowItems");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutWindowItems.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutWindowItems.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutWindowItems.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayOutWindowItems.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayOutWindowItems.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutWindowItems.b!=null;
	}
}
