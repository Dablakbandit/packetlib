package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutOpenWindow extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutOpenWindow
	//a : int
	//b : java.lang.String
	//c : net.minecraft.server.v1_8_R1.IChatBaseComponent
	//d : int
	//e : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutOpenWindow
	//a : int
	//b : java.lang.String
	//c : net.minecraft.server.v1_8_R2.IChatBaseComponent
	//d : int
	//e : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutOpenWindow
	//a : int
	//b : java.lang.String
	//c : net.minecraft.server.v1_8_R3.IChatBaseComponent
	//d : int
	//e : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutOpenWindow
	//a : int
	//b : int
	//c : java.lang.String
	//d : int
	//e : boolean
	//f : int

	protected PacketPlayOutOpenWindow(Object packet){
		super(packet, PacketType.PacketPlayOutOpenWindow);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutOpenWindow");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutOpenWindow.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutOpenWindow.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutOpenWindow.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getBAsInt(){
		try{
			return (int)PacketPlayOutOpenWindow.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setBAsInt(int o){
		try{
			PacketPlayOutOpenWindow.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsInt(){
		return PacketPlayOutOpenWindow.b!=null&&int.class.isAssignableFrom(PacketPlayOutOpenWindow.b.getType());
	}

	public String getBAsString(){
		try{
			return (String)PacketPlayOutOpenWindow.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setBAsString(String o){
		try{
			PacketPlayOutOpenWindow.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsString(){
		return PacketPlayOutOpenWindow.b!=null&&String.class.isAssignableFrom(PacketPlayOutOpenWindow.b.getType());
	}
	public boolean hasB(){
		return PacketPlayOutOpenWindow.b!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutOpenWindow.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutOpenWindow.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutOpenWindow.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public boolean getEAsBoolean(){
		try{
			return (boolean)PacketPlayOutOpenWindow.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setEAsBoolean(boolean o){
		try{
			PacketPlayOutOpenWindow.e.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasEAsBoolean(){
		return PacketPlayOutOpenWindow.e!=null&&boolean.class.isAssignableFrom(PacketPlayOutOpenWindow.e.getType());
	}

	public int getEAsInt(){
		try{
			return (int)PacketPlayOutOpenWindow.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setEAsInt(int o){
		try{
			PacketPlayOutOpenWindow.e.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasEAsInt(){
		return PacketPlayOutOpenWindow.e!=null&&int.class.isAssignableFrom(PacketPlayOutOpenWindow.e.getType());
	}
	public boolean hasE(){
		return PacketPlayOutOpenWindow.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public int getF(){
		try{
			return (int)PacketPlayOutOpenWindow.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setF(int i){
		try{
			PacketPlayOutOpenWindow.f.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutOpenWindow.f!=null;
	}
}
