package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInLook extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInLook

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInFlying$PacketPlayInLook

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInFlying$PacketPlayInLook

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInLook

	protected PacketPlayInLook(Object packet){
		super(packet, PacketType.PacketPlayInLook);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInLook", "PacketPlayInFlying");

	private static Field x = NMSUtils.getFieldSilent(packetclass, "x");

	public int getX(){
		try{
			return (int)PacketPlayInLook.x.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setX(int i){
		try{
			PacketPlayInLook.x.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasX(){
		return PacketPlayInLook.x!=null;
	}

	private static Field y = NMSUtils.getFieldSilent(packetclass, "y");

	public int getY(){
		try{
			return (int)PacketPlayInLook.y.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setY(int i){
		try{
			PacketPlayInLook.y.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasY(){
		return PacketPlayInLook.y!=null;
	}

	private static Field z = NMSUtils.getFieldSilent(packetclass, "z");

	public int getZ(){
		try{
			return (int)PacketPlayInLook.z.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setZ(int i){
		try{
			PacketPlayInLook.z.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasZ(){
		return PacketPlayInLook.z!=null;
	}

	private static Field stance = NMSUtils.getFieldSilent(packetclass, "stance");

	public double getStance(){
		try{
			return (double)PacketPlayInLook.stance.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}

	public void setStance(double d){
		try{
			PacketPlayInLook.stance.set(packet, d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasStance(){
		return PacketPlayInLook.stance!=null;
	}

	private static Field yaw = NMSUtils.getFieldSilent(packetclass, "yaw");

	public float getYaw(){
		try{
			return (float)PacketPlayInLook.yaw.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setYaw(float f){
		try{
			PacketPlayInLook.yaw.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasYaw(){
		return PacketPlayInLook.yaw!=null;
	}

	private static Field pitch = NMSUtils.getFieldSilent(packetclass, "pitch");

	public float getPitch(){
		try{
			return (float)PacketPlayInLook.pitch.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setPitch(float f){
		try{
			PacketPlayInLook.pitch.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasPitch(){
		return PacketPlayInLook.pitch!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public boolean getF(){
		try{
			return (boolean)PacketPlayInLook.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setF(boolean b){
		try{
			PacketPlayInLook.f.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayInLook.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public boolean getG(){
		try{
			return (boolean)PacketPlayInLook.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setG(boolean b){
		try{
			PacketPlayInLook.g.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayInLook.g!=null;
	}

	private static Field hasPos = NMSUtils.getFieldSilent(packetclass, "hasPos");

	public boolean getHasPos(){
		try{
			return (boolean)PacketPlayInLook.hasPos.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setHasPos(boolean b){
		try{
			PacketPlayInLook.hasPos.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasHasPos(){
		return PacketPlayInLook.hasPos!=null;
	}

	private static Field hasLook = NMSUtils.getFieldSilent(packetclass, "hasLook");

	public boolean getHasLook(){
		try{
			return (boolean)PacketPlayInLook.hasLook.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setHasLook(boolean b){
		try{
			PacketPlayInLook.hasLook.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasHasLook(){
		return PacketPlayInLook.hasLook!=null;
	}
}
