package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutSetCompression extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutSetCompression
	//a : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutSetCompression
	//a : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutSetCompression
	//a : int

	protected PacketPlayOutSetCompression(Object packet){
		super(packet, PacketType.PacketPlayOutSetCompression);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutSetCompression");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutSetCompression.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutSetCompression.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutSetCompression.a!=null;
	}
}
