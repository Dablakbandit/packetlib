package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInClientCommand extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInClientCommand
	//a : net.minecraft.server.v1_8_R1.EnumClientCommand

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInClientCommand
	//a : net.minecraft.server.v1_8_R2.PacketPlayInClientCommand$EnumClientCommand

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInClientCommand
	//a : net.minecraft.server.v1_8_R3.PacketPlayInClientCommand$EnumClientCommand

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInClientCommand
	//a : net.minecraft.server.v1_7_R4.EnumClientCommand

	protected PacketPlayInClientCommand(Object packet){
		super(packet, PacketType.PacketPlayInClientCommand);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInClientCommand");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketPlayInClientCommand.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketPlayInClientCommand.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInClientCommand.a!=null;
	}
}
