package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutTitle extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutTitle
	//a : net.minecraft.server.v1_8_R1.EnumTitleAction
	//b : net.minecraft.server.v1_8_R1.IChatBaseComponent
	//c : int
	//d : int
	//e : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutTitle
	//a : net.minecraft.server.v1_8_R2.PacketPlayOutTitle$EnumTitleAction
	//b : net.minecraft.server.v1_8_R2.IChatBaseComponent
	//c : int
	//d : int
	//e : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutTitle
	//a : net.minecraft.server.v1_8_R3.PacketPlayOutTitle$EnumTitleAction
	//b : net.minecraft.server.v1_8_R3.IChatBaseComponent
	//c : int
	//d : int
	//e : int

	protected PacketPlayOutTitle(Object packet){
		super(packet, PacketType.PacketPlayOutTitle);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutTitle");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketPlayOutTitle.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketPlayOutTitle.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutTitle.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayOutTitle.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayOutTitle.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutTitle.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutTitle.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutTitle.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutTitle.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutTitle.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutTitle.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutTitle.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public int getE(){
		try{
			return (int)PacketPlayOutTitle.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(int i){
		try{
			PacketPlayOutTitle.e.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutTitle.e!=null;
	}
}
