package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInFlying extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInFlying
	//x : double
	//y : double
	//z : double
	//yaw : float
	//pitch : float
	//f : boolean
	//hasPos : boolean
	//hasLook : boolean

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInFlying
	//x : double
	//y : double
	//z : double
	//yaw : float
	//pitch : float
	//f : boolean
	//hasPos : boolean
	//hasLook : boolean

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInFlying
	//x : double
	//y : double
	//z : double
	//yaw : float
	//pitch : float
	//f : boolean
	//hasPos : boolean
	//hasLook : boolean

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInFlying
	//x : double
	//y : double
	//z : double
	//stance : double
	//yaw : float
	//pitch : float
	//g : boolean
	//hasPos : boolean
	//hasLook : boolean

	protected PacketPlayInFlying(Object packet){
		super(packet, PacketType.PacketPlayInFlying);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInFlying");

	private static Field x = NMSUtils.getFieldSilent(packetclass, "x");

	public int getX(){
		try{
			return (int)PacketPlayInFlying.x.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setX(int i){
		try{
			PacketPlayInFlying.x.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasX(){
		return PacketPlayInFlying.x!=null;
	}

	private static Field y = NMSUtils.getFieldSilent(packetclass, "y");

	public int getY(){
		try{
			return (int)PacketPlayInFlying.y.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setY(int i){
		try{
			PacketPlayInFlying.y.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasY(){
		return PacketPlayInFlying.y!=null;
	}

	private static Field z = NMSUtils.getFieldSilent(packetclass, "z");

	public int getZ(){
		try{
			return (int)PacketPlayInFlying.z.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setZ(int i){
		try{
			PacketPlayInFlying.z.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasZ(){
		return PacketPlayInFlying.z!=null;
	}

	private static Field stance = NMSUtils.getFieldSilent(packetclass, "stance");

	public double getStance(){
		try{
			return (double)PacketPlayInFlying.stance.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}

	public void setStance(double d){
		try{
			PacketPlayInFlying.stance.set(packet, d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasStance(){
		return PacketPlayInFlying.stance!=null;
	}

	private static Field yaw = NMSUtils.getFieldSilent(packetclass, "yaw");

	public float getYaw(){
		try{
			return (float)PacketPlayInFlying.yaw.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setYaw(float f){
		try{
			PacketPlayInFlying.yaw.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasYaw(){
		return PacketPlayInFlying.yaw!=null;
	}

	private static Field pitch = NMSUtils.getFieldSilent(packetclass, "pitch");

	public float getPitch(){
		try{
			return (float)PacketPlayInFlying.pitch.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setPitch(float f){
		try{
			PacketPlayInFlying.pitch.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasPitch(){
		return PacketPlayInFlying.pitch!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public boolean getF(){
		try{
			return (boolean)PacketPlayInFlying.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setF(boolean b){
		try{
			PacketPlayInFlying.f.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayInFlying.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public boolean getG(){
		try{
			return (boolean)PacketPlayInFlying.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setG(boolean b){
		try{
			PacketPlayInFlying.g.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayInFlying.g!=null;
	}

	private static Field hasPos = NMSUtils.getFieldSilent(packetclass, "hasPos");

	public boolean getHasPos(){
		try{
			return (boolean)PacketPlayInFlying.hasPos.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setHasPos(boolean b){
		try{
			PacketPlayInFlying.hasPos.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasHasPos(){
		return PacketPlayInFlying.hasPos!=null;
	}

	private static Field hasLook = NMSUtils.getFieldSilent(packetclass, "hasLook");

	public boolean getHasLook(){
		try{
			return (boolean)PacketPlayInFlying.hasLook.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void setHasLook(boolean b){
		try{
			PacketPlayInFlying.hasLook.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasHasLook(){
		return PacketPlayInFlying.hasLook!=null;
	}
}
