package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutAnimation extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutAnimation
	//a : int
	//b : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutAnimation
	//a : int
	//b : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutAnimation
	//a : int
	//b : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutAnimation
	//a : int
	//b : int

	protected PacketPlayOutAnimation(Object packet){
		super(packet, PacketType.PacketPlayOutAnimation);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutAnimation");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutAnimation.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutAnimation.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutAnimation.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutAnimation.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutAnimation.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutAnimation.b!=null;
	}
}
