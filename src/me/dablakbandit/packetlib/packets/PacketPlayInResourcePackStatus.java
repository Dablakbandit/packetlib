package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInResourcePackStatus extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInResourcePackStatus
	//a : java.lang.String
	//b : net.minecraft.server.v1_8_R1.EnumResourcePackStatus

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInResourcePackStatus
	//a : java.lang.String
	//b : net.minecraft.server.v1_8_R2.PacketPlayInResourcePackStatus$EnumResourcePackStatus

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInResourcePackStatus
	//a : java.lang.String
	//b : net.minecraft.server.v1_8_R3.PacketPlayInResourcePackStatus$EnumResourcePackStatus

	protected PacketPlayInResourcePackStatus(Object packet){
		super(packet, PacketType.PacketPlayInResourcePackStatus);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInResourcePackStatus");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String getA(){
		try{
			return (String)PacketPlayInResourcePackStatus.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(String s){
		try{
			PacketPlayInResourcePackStatus.a.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInResourcePackStatus.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayInResourcePackStatus.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayInResourcePackStatus.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayInResourcePackStatus.b!=null;
	}
}
