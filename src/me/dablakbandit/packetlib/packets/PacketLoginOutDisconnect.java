package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketLoginOutDisconnect extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketLoginOutDisconnect
	//a : net.minecraft.server.v1_8_R1.IChatBaseComponent

	//Fields
	//net.minecraft.server.v1_8_R2.PacketLoginOutDisconnect
	//a : net.minecraft.server.v1_8_R2.IChatBaseComponent

	//Fields
	//net.minecraft.server.v1_8_R3.PacketLoginOutDisconnect
	//a : net.minecraft.server.v1_8_R3.IChatBaseComponent

	//Fields
	//net.minecraft.server.v1_7_R4.PacketLoginOutDisconnect
	//a : net.minecraft.server.v1_7_R4.IChatBaseComponent

	protected PacketLoginOutDisconnect(Object packet){
		super(packet, PacketType.PacketLoginOutDisconnect);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketLoginOutDisconnect");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketLoginOutDisconnect.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketLoginOutDisconnect.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketLoginOutDisconnect.a!=null;
	}
}
