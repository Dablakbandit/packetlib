package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutBlockBreakAnimation extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutBlockBreakAnimation
	//a : int
	//b : net.minecraft.server.v1_8_R1.BlockPosition
	//c : int

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutBlockBreakAnimation
	//a : int
	//b : net.minecraft.server.v1_8_R2.BlockPosition
	//c : int

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutBlockBreakAnimation
	//a : int
	//b : net.minecraft.server.v1_8_R3.BlockPosition
	//c : int

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutBlockBreakAnimation
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int

	protected PacketPlayOutBlockBreakAnimation(Object packet){
		super(packet, PacketType.PacketPlayOutBlockBreakAnimation);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutBlockBreakAnimation");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutBlockBreakAnimation.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutBlockBreakAnimation.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutBlockBreakAnimation.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getBAsInt(){
		try{
			return (int)PacketPlayOutBlockBreakAnimation.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setBAsInt(int o){
		try{
			PacketPlayOutBlockBreakAnimation.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsInt(){
		return PacketPlayOutBlockBreakAnimation.b!=null&&int.class.isAssignableFrom(PacketPlayOutBlockBreakAnimation.b.getType());
	}

	public Object getBAsObject(){
		try{
			return (Object)PacketPlayOutBlockBreakAnimation.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setBAsObject(Object o){
		try{
			PacketPlayOutBlockBreakAnimation.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasBAsObject(){
		return PacketPlayOutBlockBreakAnimation.b!=null&&Object.class.isAssignableFrom(PacketPlayOutBlockBreakAnimation.b.getType());
	}
	public boolean hasB(){
		return PacketPlayOutBlockBreakAnimation.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutBlockBreakAnimation.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutBlockBreakAnimation.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutBlockBreakAnimation.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutBlockBreakAnimation.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutBlockBreakAnimation.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutBlockBreakAnimation.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public int getE(){
		try{
			return (int)PacketPlayOutBlockBreakAnimation.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(int i){
		try{
			PacketPlayOutBlockBreakAnimation.e.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutBlockBreakAnimation.e!=null;
	}
}
