package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInArmAnimation extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInArmAnimation

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInArmAnimation

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInArmAnimation
	//timestamp : long

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInArmAnimation
	//a : int
	//b : int

	protected PacketPlayInArmAnimation(Object packet){
		super(packet, PacketType.PacketPlayInArmAnimation);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInArmAnimation");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayInArmAnimation.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayInArmAnimation.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInArmAnimation.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayInArmAnimation.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayInArmAnimation.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayInArmAnimation.b!=null;
	}

	private static Field timestamp = NMSUtils.getFieldSilent(packetclass, "timestamp");

	public long getTimestamp(){
		try{
			return (long)PacketPlayInArmAnimation.timestamp.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setTimestamp(long l){
		try{
			PacketPlayInArmAnimation.timestamp.set(packet, l);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasTimestamp(){
		return PacketPlayInArmAnimation.timestamp!=null;
	}
}
