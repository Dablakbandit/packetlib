package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutStatistic extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutStatistic
	//a : java.util.Map

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutStatistic
	//a : java.util.Map

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutStatistic
	//a : java.util.Map

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutStatistic
	//a : java.util.Map

	protected PacketPlayOutStatistic(Object packet){
		super(packet, PacketType.PacketPlayOutStatistic);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutStatistic");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public Object getA(){
		try{
			return PacketPlayOutStatistic.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(Object o){
		try{
			PacketPlayOutStatistic.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutStatistic.a!=null;
	}
}
