package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutRespawn extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutRespawn
	//a : int
	//b : net.minecraft.server.v1_8_R1.EnumDifficulty
	//c : net.minecraft.server.v1_8_R1.EnumGamemode
	//d : net.minecraft.server.v1_8_R1.WorldType

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutRespawn
	//a : int
	//b : net.minecraft.server.v1_8_R2.EnumDifficulty
	//c : net.minecraft.server.v1_8_R2.WorldSettings$EnumGamemode
	//d : net.minecraft.server.v1_8_R2.WorldType

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutRespawn
	//a : int
	//b : net.minecraft.server.v1_8_R3.EnumDifficulty
	//c : net.minecraft.server.v1_8_R3.WorldSettings$EnumGamemode
	//d : net.minecraft.server.v1_8_R3.WorldType

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutRespawn
	//a : int
	//b : net.minecraft.server.v1_7_R4.EnumDifficulty
	//c : net.minecraft.server.v1_7_R4.EnumGamemode
	//d : net.minecraft.server.v1_7_R4.WorldType

	protected PacketPlayOutRespawn(Object packet){
		super(packet, PacketType.PacketPlayOutRespawn);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutRespawn");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutRespawn.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutRespawn.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutRespawn.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayOutRespawn.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayOutRespawn.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutRespawn.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public Object getC(){
		try{
			return PacketPlayOutRespawn.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setC(Object o){
		try{
			PacketPlayOutRespawn.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutRespawn.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public Object getD(){
		try{
			return PacketPlayOutRespawn.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setD(Object o){
		try{
			PacketPlayOutRespawn.d.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutRespawn.d!=null;
	}
}
