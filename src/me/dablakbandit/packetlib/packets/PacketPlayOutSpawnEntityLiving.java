package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutSpawnEntityLiving extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutSpawnEntityLiving
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int
	//f : int
	//g : int
	//h : int
	//i : byte
	//j : byte
	//k : byte
	//l : net.minecraft.server.v1_8_R1.DataWatcher
	//m : java.util.List

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutSpawnEntityLiving
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int
	//f : int
	//g : int
	//h : int
	//i : byte
	//j : byte
	//k : byte
	//l : net.minecraft.server.v1_8_R2.DataWatcher
	//m : java.util.List

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityLiving
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int
	//f : int
	//g : int
	//h : int
	//i : byte
	//j : byte
	//k : byte
	//l : net.minecraft.server.v1_8_R3.DataWatcher
	//m : java.util.List

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutSpawnEntityLiving
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int
	//f : int
	//g : int
	//h : int
	//i : byte
	//j : byte
	//k : byte
	//l : net.minecraft.server.v1_7_R4.DataWatcher
	//m : java.util.List

	protected PacketPlayOutSpawnEntityLiving(Object packet){
		super(packet, PacketType.PacketPlayOutSpawnEntityLiving);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutSpawnEntityLiving");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutSpawnEntityLiving.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutSpawnEntityLiving.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutSpawnEntityLiving.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutSpawnEntityLiving.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutSpawnEntityLiving.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutSpawnEntityLiving.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutSpawnEntityLiving.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutSpawnEntityLiving.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutSpawnEntityLiving.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getD(){
		try{
			return (int)PacketPlayOutSpawnEntityLiving.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setD(int i){
		try{
			PacketPlayOutSpawnEntityLiving.d.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutSpawnEntityLiving.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public int getE(){
		try{
			return (int)PacketPlayOutSpawnEntityLiving.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(int i){
		try{
			PacketPlayOutSpawnEntityLiving.e.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutSpawnEntityLiving.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public int getF(){
		try{
			return (int)PacketPlayOutSpawnEntityLiving.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setF(int i){
		try{
			PacketPlayOutSpawnEntityLiving.f.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutSpawnEntityLiving.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public int getG(){
		try{
			return (int)PacketPlayOutSpawnEntityLiving.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setG(int i){
		try{
			PacketPlayOutSpawnEntityLiving.g.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayOutSpawnEntityLiving.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public int getH(){
		try{
			return (int)PacketPlayOutSpawnEntityLiving.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setH(int i){
		try{
			PacketPlayOutSpawnEntityLiving.h.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutSpawnEntityLiving.h!=null;
	}

	private static Field i = NMSUtils.getFieldSilent(packetclass, "i");

	public byte getI(){
		try{
			return (byte)PacketPlayOutSpawnEntityLiving.i.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setI(byte b){
		try{
			PacketPlayOutSpawnEntityLiving.i.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasI(){
		return PacketPlayOutSpawnEntityLiving.i!=null;
	}

	private static Field j = NMSUtils.getFieldSilent(packetclass, "j");

	public byte getJ(){
		try{
			return (byte)PacketPlayOutSpawnEntityLiving.j.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setJ(byte b){
		try{
			PacketPlayOutSpawnEntityLiving.j.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasJ(){
		return PacketPlayOutSpawnEntityLiving.j!=null;
	}

	private static Field k = NMSUtils.getFieldSilent(packetclass, "k");

	public byte getK(){
		try{
			return (byte)PacketPlayOutSpawnEntityLiving.k.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setK(byte b){
		try{
			PacketPlayOutSpawnEntityLiving.k.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasK(){
		return PacketPlayOutSpawnEntityLiving.k!=null;
	}

	private static Field l = NMSUtils.getFieldSilent(packetclass, "l");

	public Object getL(){
		try{
			return PacketPlayOutSpawnEntityLiving.l.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setL(Object o){
		try{
			PacketPlayOutSpawnEntityLiving.l.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasL(){
		return PacketPlayOutSpawnEntityLiving.l!=null;
	}

	private static Field m = NMSUtils.getFieldSilent(packetclass, "m");

	public Object getM(){
		try{
			return PacketPlayOutSpawnEntityLiving.m.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setM(Object o){
		try{
			PacketPlayOutSpawnEntityLiving.m.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasM(){
		return PacketPlayOutSpawnEntityLiving.m!=null;
	}

	private static Class<?> entityliving = NMSUtils.getNMSClass("EntityLiving");
	private static java.lang.reflect.Constructor<?> cons = NMSUtils.getConstructor(NMSUtils.getNMSClass("PacketPlayOutSpawnEntityLiving"), entityliving);

	public static PacketPlayOutSpawnEntityLiving getNewPacketPlayOutSpawnEntityLiving(org.bukkit.entity.Entity e){
		Object o = NMSUtils.getHandle(e);
		if(entityliving.isAssignableFrom(o.getClass())){
			try{
				Object p = cons.newInstance(o);
				return new PacketPlayOutSpawnEntityLiving(p);
			}catch(Exception e1){
				e1.printStackTrace();
			}
		}
		return null;
	}
}
