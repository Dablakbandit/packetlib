package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutExplosion extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutExplosion
	//a : double
	//b : double
	//c : double
	//d : float
	//e : java.util.List
	//f : float
	//g : float
	//h : float

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutExplosion
	//a : double
	//b : double
	//c : double
	//d : float
	//e : java.util.List
	//f : float
	//g : float
	//h : float

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutExplosion
	//a : double
	//b : double
	//c : double
	//d : float
	//e : java.util.List
	//f : float
	//g : float
	//h : float

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutExplosion
	//a : double
	//b : double
	//c : double
	//d : float
	//e : java.util.List
	//f : float
	//g : float
	//h : float

	protected PacketPlayOutExplosion(Object packet){
		super(packet, PacketType.PacketPlayOutExplosion);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutExplosion");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public double getA(){
		try{
			return (double)PacketPlayOutExplosion.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}

	public void setA(double d){
		try{
			PacketPlayOutExplosion.a.set(packet, d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutExplosion.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public double getB(){
		try{
			return (double)PacketPlayOutExplosion.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}

	public void setB(double d){
		try{
			PacketPlayOutExplosion.b.set(packet, d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutExplosion.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public double getC(){
		try{
			return (double)PacketPlayOutExplosion.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}

	public void setC(double d){
		try{
			PacketPlayOutExplosion.c.set(packet, d);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutExplosion.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public float getD(){
		try{
			return (float)PacketPlayOutExplosion.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setD(float f){
		try{
			PacketPlayOutExplosion.d.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasD(){
		return PacketPlayOutExplosion.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public Object getE(){
		try{
			return PacketPlayOutExplosion.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setE(Object o){
		try{
			PacketPlayOutExplosion.e.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutExplosion.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public float getF(){
		try{
			return (float)PacketPlayOutExplosion.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setF(float f){
		try{
			PacketPlayOutExplosion.f.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutExplosion.f!=null;
	}

	private static Field g = NMSUtils.getFieldSilent(packetclass, "g");

	public float getG(){
		try{
			return (float)PacketPlayOutExplosion.g.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setG(float f){
		try{
			PacketPlayOutExplosion.g.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasG(){
		return PacketPlayOutExplosion.g!=null;
	}

	private static Field h = NMSUtils.getFieldSilent(packetclass, "h");

	public float getH(){
		try{
			return (float)PacketPlayOutExplosion.h.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0F;
	}

	public void setH(float f){
		try{
			PacketPlayOutExplosion.h.set(packet, f);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasH(){
		return PacketPlayOutExplosion.h!=null;
	}
}
