package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketStatusInPing extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketStatusInPing
	//a : long

	//Fields
	//net.minecraft.server.v1_8_R2.PacketStatusInPing
	//a : long

	//Fields
	//net.minecraft.server.v1_8_R3.PacketStatusInPing
	//a : long

	//Fields
	//net.minecraft.server.v1_7_R4.PacketStatusInPing
	//a : long

	protected PacketStatusInPing(Object packet){
		super(packet, PacketType.PacketStatusInPing);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketStatusInPing");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public long getA(){
		try{
			return (long)PacketStatusInPing.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(long l){
		try{
			PacketStatusInPing.a.set(packet, l);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketStatusInPing.a!=null;
	}
}
