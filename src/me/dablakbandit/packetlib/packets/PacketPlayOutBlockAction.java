package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutBlockAction extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutBlockAction
	//a : net.minecraft.server.v1_8_R1.BlockPosition
	//b : int
	//c : int
	//d : net.minecraft.server.v1_8_R1.Block

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutBlockAction
	//a : net.minecraft.server.v1_8_R2.BlockPosition
	//b : int
	//c : int
	//d : net.minecraft.server.v1_8_R2.Block

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutBlockAction
	//a : net.minecraft.server.v1_8_R3.BlockPosition
	//b : int
	//c : int
	//d : net.minecraft.server.v1_8_R3.Block

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutBlockAction
	//a : int
	//b : int
	//c : int
	//d : int
	//e : int
	//f : net.minecraft.server.v1_7_R4.Block

	protected PacketPlayOutBlockAction(Object packet){
		super(packet, PacketType.PacketPlayOutBlockAction);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutBlockAction");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getAAsInt(){
		try{
			return (int)PacketPlayOutBlockAction.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setAAsInt(int o){
		try{
			PacketPlayOutBlockAction.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsInt(){
		return PacketPlayOutBlockAction.a!=null&&int.class.isAssignableFrom(PacketPlayOutBlockAction.a.getType());
	}

	public Object getAAsObject(){
		try{
			return (Object)PacketPlayOutBlockAction.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setAAsObject(Object o){
		try{
			PacketPlayOutBlockAction.a.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasAAsObject(){
		return PacketPlayOutBlockAction.a!=null&&Object.class.isAssignableFrom(PacketPlayOutBlockAction.a.getType());
	}
	public boolean hasA(){
		return PacketPlayOutBlockAction.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutBlockAction.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutBlockAction.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutBlockAction.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public int getC(){
		try{
			return (int)PacketPlayOutBlockAction.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setC(int i){
		try{
			PacketPlayOutBlockAction.c.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutBlockAction.c!=null;
	}

	private static Field d = NMSUtils.getFieldSilent(packetclass, "d");

	public int getDAsInt(){
		try{
			return (int)PacketPlayOutBlockAction.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setDAsInt(int o){
		try{
			PacketPlayOutBlockAction.d.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasDAsInt(){
		return PacketPlayOutBlockAction.d!=null&&int.class.isAssignableFrom(PacketPlayOutBlockAction.d.getType());
	}

	public Object getDAsObject(){
		try{
			return (Object)PacketPlayOutBlockAction.d.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setDAsObject(Object o){
		try{
			PacketPlayOutBlockAction.d.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasDAsObject(){
		return PacketPlayOutBlockAction.d!=null&&Object.class.isAssignableFrom(PacketPlayOutBlockAction.d.getType());
	}
	public boolean hasD(){
		return PacketPlayOutBlockAction.d!=null;
	}

	private static Field e = NMSUtils.getFieldSilent(packetclass, "e");

	public int getE(){
		try{
			return (int)PacketPlayOutBlockAction.e.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setE(int i){
		try{
			PacketPlayOutBlockAction.e.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasE(){
		return PacketPlayOutBlockAction.e!=null;
	}

	private static Field f = NMSUtils.getFieldSilent(packetclass, "f");

	public Object getF(){
		try{
			return PacketPlayOutBlockAction.f.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setF(Object o){
		try{
			PacketPlayOutBlockAction.f.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasF(){
		return PacketPlayOutBlockAction.f!=null;
	}
}
