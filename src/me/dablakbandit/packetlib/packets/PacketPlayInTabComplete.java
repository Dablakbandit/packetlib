package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInTabComplete extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInTabComplete
	//a : java.lang.String
	//b : net.minecraft.server.v1_8_R1.BlockPosition

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInTabComplete
	//a : java.lang.String
	//b : net.minecraft.server.v1_8_R2.BlockPosition

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInTabComplete
	//a : java.lang.String
	//b : net.minecraft.server.v1_8_R3.BlockPosition

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInTabComplete
	//a : java.lang.String

	protected PacketPlayInTabComplete(Object packet){
		super(packet, PacketType.PacketPlayInTabComplete);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInTabComplete");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String getA(){
		try{
			return (String)PacketPlayInTabComplete.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(String s){
		try{
			PacketPlayInTabComplete.a.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInTabComplete.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayInTabComplete.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayInTabComplete.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayInTabComplete.b!=null;
	}
}
