package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayOutEntityEquipment extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayOutEntityEquipment
	//a : int
	//b : int
	//c : net.minecraft.server.v1_8_R1.ItemStack

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayOutEntityEquipment
	//a : int
	//b : int
	//c : net.minecraft.server.v1_8_R2.ItemStack

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayOutEntityEquipment
	//a : int
	//b : int
	//c : net.minecraft.server.v1_8_R3.ItemStack

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayOutEntityEquipment
	//a : int
	//b : int
	//c : net.minecraft.server.v1_7_R4.ItemStack

	protected PacketPlayOutEntityEquipment(Object packet){
		super(packet, PacketType.PacketPlayOutEntityEquipment);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayOutEntityEquipment");

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public int getA(){
		try{
			return (int)PacketPlayOutEntityEquipment.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setA(int i){
		try{
			PacketPlayOutEntityEquipment.a.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayOutEntityEquipment.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public int getB(){
		try{
			return (int)PacketPlayOutEntityEquipment.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setB(int i){
		try{
			PacketPlayOutEntityEquipment.b.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayOutEntityEquipment.b!=null;
	}

	private static Field c = NMSUtils.getFieldSilent(packetclass, "c");

	public Object getC(){
		try{
			return PacketPlayOutEntityEquipment.c.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setC(Object o){
		try{
			PacketPlayOutEntityEquipment.c.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasC(){
		return PacketPlayOutEntityEquipment.c!=null;
	}
}
