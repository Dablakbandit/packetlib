package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketPlayInCustomPayload extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketPlayInCustomPayload
	//a : java.lang.String
	//b : net.minecraft.server.v1_8_R1.PacketDataSerializer

	//Fields
	//net.minecraft.server.v1_8_R2.PacketPlayInCustomPayload
	//a : java.lang.String
	//b : net.minecraft.server.v1_8_R2.PacketDataSerializer

	//Fields
	//net.minecraft.server.v1_8_R3.PacketPlayInCustomPayload
	//a : java.lang.String
	//b : net.minecraft.server.v1_8_R3.PacketDataSerializer

	//Fields
	//net.minecraft.server.v1_7_R4.PacketPlayInCustomPayload
	//tag : java.lang.String
	//length : int
	//data : [B

	protected PacketPlayInCustomPayload(Object packet){
		super(packet, PacketType.PacketPlayInCustomPayload);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketPlayInCustomPayload");

	private static Field tag = NMSUtils.getFieldSilent(packetclass, "tag");

	public String getTag(){
		try{
			return (String)PacketPlayInCustomPayload.tag.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setTag(String s){
		try{
			PacketPlayInCustomPayload.tag.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasTag(){
		return PacketPlayInCustomPayload.tag!=null;
	}

	private static Field length = NMSUtils.getFieldSilent(packetclass, "length");

	public int getLength(){
		try{
			return (int)PacketPlayInCustomPayload.length.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void setLength(int i){
		try{
			PacketPlayInCustomPayload.length.set(packet, i);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasLength(){
		return PacketPlayInCustomPayload.length!=null;
	}

	private static Field data = NMSUtils.getFieldSilent(packetclass, "data");

	public byte[] getData(){
		try{
			return (byte[])PacketPlayInCustomPayload.data.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setData(byte[] b){
		try{
			PacketPlayInCustomPayload.data.set(packet, b);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasData(){
		return PacketPlayInCustomPayload.data!=null;
	}

	private static Field a = NMSUtils.getFieldSilent(packetclass, "a");

	public String getA(){
		try{
			return (String)PacketPlayInCustomPayload.a.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setA(String s){
		try{
			PacketPlayInCustomPayload.a.set(packet, s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasA(){
		return PacketPlayInCustomPayload.a!=null;
	}

	private static Field b = NMSUtils.getFieldSilent(packetclass, "b");

	public Object getB(){
		try{
			return PacketPlayInCustomPayload.b.get(packet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public void setB(Object o){
		try{
			PacketPlayInCustomPayload.b.set(packet, o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean hasB(){
		return PacketPlayInCustomPayload.b!=null;
	}
}
