package me.dablakbandit.packetlib.packets;

import me.dablakbandit.nmsutils.NMSUtils;
import java.lang.reflect.Field;

public class PacketStatusInStart extends Packet{

	//Fields
	//net.minecraft.server.v1_8_R1.PacketStatusInStart

	//Fields
	//net.minecraft.server.v1_8_R2.PacketStatusInStart

	//Fields
	//net.minecraft.server.v1_8_R3.PacketStatusInStart

	//Fields
	//net.minecraft.server.v1_7_R4.PacketStatusInStart

	protected PacketStatusInStart(Object packet){
		super(packet, PacketType.PacketStatusInStart);
	}

	private static Class<?> packetclass = NMSUtils.getNMSClass("PacketStatusInStart");
}
