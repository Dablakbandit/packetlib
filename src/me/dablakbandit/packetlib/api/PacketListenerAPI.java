package me.dablakbandit.packetlib.api;

import java.util.ArrayList;
import java.util.List;

import me.dablakbandit.packetlib.listener.PacketListener;
import me.dablakbandit.packetlib.listener.PlayerPacketReceive;
import me.dablakbandit.packetlib.listener.PlayerPacketSend;
import me.dablakbandit.packetlib.listener.ServerPacketReceive;
import me.dablakbandit.packetlib.listener.ServerPacketSend;

public class PacketListenerAPI {

	public static PacketListenerAPI main = new PacketListenerAPI();
	private List<PacketListener> listeners = new ArrayList<PacketListener>();
	
	private PacketListenerAPI(){}
	
	public static PacketListenerAPI getInstance(){
		return main;
	}
	
	public void addListener(PacketListener packetlistener){
		listeners.add(packetlistener);
	}
	
	public void removeListener(PacketListener packetlistener){
		listeners.remove(packetlistener);
	}
	
	public void onPlayerPacketSend(PlayerPacketSend pps){
		for(PacketListener packetlistener : listeners){
			packetlistener.onPlayerPacketSend(pps);
		}
	}
	
	public void onPlayerPacketReceive(PlayerPacketReceive ppr){
		for(PacketListener packetlistener : listeners){
			packetlistener.onPlayerPacketReceive(ppr);
		}
	}
	
	public void onServerPacketSend(ServerPacketSend sps){
		for(PacketListener packetlistener : listeners){
			packetlistener.onServerPacketSend(sps);
		}
	}
	
	public void onServerPacketReceive(ServerPacketReceive spr){
		for(PacketListener packetlistener : listeners){
			packetlistener.onServerPacketReceive(spr);
		}
	}
	
}
